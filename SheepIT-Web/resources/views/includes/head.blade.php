<meta charset="utf-8">
<meta name="description" content="">

<title>{{Route::current()->getName()}}</title>

<!-- load bootstrap from a cdn
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
-->

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="csrf-token" content="{{ csrf_token() }}">


<link rel="stylesheet" href="{{URL::to('/vendor/bootstrap/dist/css/bootstrap.min.css')}}">

<link rel="stylesheet" href="{{URL::to('/vendor/font-awesome/css/font-awesome.css')}}">


<link rel="stylesheet" href="{{URL::to('css/app.css')}}">
<link rel="stylesheet" href="{{URL::to('css/custom.css')}}">


<script src="{{ URL::to('vendor/jquery/dist/jquery.js') }}"></script>
<script src="{{ URL::to('vendor/jquery/dist/bootstrap.js') }}"></script>

<script src="{{URL::to('vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{ URL::to('vendor/jquery.browser/dist/jquery.browser.js') }}"></script>
<script src="{{ URL::to('vendor/jquery-validation/dist/jquery.validate.js') }}"></script>
<script src="{{ URL::to('vendor/jquery-validation/dist/additional-methods.js') }}"></script>

<script src="{{ URL::to('js/app.js') }}"></script>
<script src="{{ URL::to('js/Chart.js') }}"></script>



<style media="screen" type="text/css">
    /*div{border:1px solid red;}*/
</style>
