<!-- top navbar-->


<header id="topbar">
  <div class="row">

    <!-- menu icon for mobile -->
    <div class="col-2 d-lg-none align-middle">
      <a id="sidebar-toggler" href="#"><h3>
          <i class="fa fa-bars" style="color:black;" aria-hidden="true"></i>
        </h3></a>
    </div>

    <!-- current page -->
    <div class="col-5 offset-xs-1 col-sm-4 pl-4">
      <h4>{{Route::current()->getName()}}</h4>
    </div>

    <!-- project name 
    <div class="col-md-3 text-center d-none d-lg-block">
      <h4 style=";">SheepIT</h4>
    </div>
    -->

    <!-- user name, opetions and image -->
    <div class="col-5 col-sm-5 col-md-5 offset-md-1 col-lg-5 offset-lg-3 text-right" id="userInfo">
      <h5>
        {{Auth::user()->username}}
      </h5>

      <h4>
        <a href="#" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-caret-down" style="color: black;"></i></a>
        <div class="dropdown-menu dropdown-menu-right" id="dropdownUser" aria-labelledby="dropdownmenu">
        <a class="dropdown-item" href="#">Perfil</a>
        <a class="dropdown-item" href="{{ url('Logout') }}">Logout</a>
        </div>
      </h4>

      
      @if(file_exists(public_path('img/user/'.Auth::user()->pkuserid.'.jpg'))) 
        <img id="userimg" class="userimg img-fluid img-responsive hidden-xs" src="{{ url('/img/user/'.Auth::user()->pkuserid.'.jpg') }}">
      @else
        <img id="userimg" class="img-fluid img-responsive hidden-xs" src="{{ url('/img/user/default.jpg') }}">
      @endif
    </div>
  </div>

<hr>
</header>

