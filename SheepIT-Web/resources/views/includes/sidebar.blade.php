<!-- sidebar-->
<aside class="sidebar-container">
    <nav class="sidebar-nav">
        <ul>
            <li>
                <h3 class="text-center mt-0 mb-4">SheepIT</h3>
            </li>
            

            <li><a href="{{ url('home') }}" class="ripple"> <i class="fa fa-dashboard"></i> Painel de Controlo</a></li>

            
            <li><a href="{{ url('animal') }}" class="ripple"> <i class ="fa"><img src="{{ url('/img/icons/black-sheep.png') }}" class="icon"></i> Animais</a></li>
            <li><a href="{{ url('herd') }}" class="ripple"> <i class="fa"><img src="{{ url('/img/icons/Group.png') }}" class="icon"></i> Rebanhos</a></li>


            <li><a href="#" class="ripple"><i class="fa fa-microchip"></i> Dispositivos <i class="fa fa-caret-down"></i></a>
                <ul class="sidebar-subnav">
                    <li><a href="{{ url('collar') }}" class="ripple"> <i class="fa"><img src="{{ url('/img/icons/collar.png') }}" class="icon"> </i>Coleiras</a></li>
                    <li><a href="{{ url('beacon') }}" class="ripple"><i class="fa fa-wifi"></i> Farois</a></li>
                </ul>
            </li>
            
            <li><a href="{{ url('property') }}" class="ripple"> <i class="fa fa-map-marker"></i> Propriedades</a></li>

            <li>&nbsp</li>

            <li><a href="{{ url('user') }}" class="ripple"> <i class="fa fa-user"></i> Utilizadores</a></li>
            <li><a href="{{ url('client') }}" class="ripple"> <i class="fa fa-building"></i> Clientes</a></li>
            <li><a href="{{ url('alertas') }}" class="ripple"> <i class="fa fa-bell"></i> Alertas</a></li>
            <li><a href="#" class="ripple"> <i class="fa fa-gear"></i> Sistema</a></li>
            
            <!-- TODO : Eleminar
            <li style="position: relative; display: inline-block; padding-top: 40px; padding-bottom: 20px; left: 20px;">
                <img src="{//{ url('/img/logo_it.png') }}" style="width:180px; height: 50px;">
            </li>
        -->
        </ul>
    </nav>

</aside>

<script type="text/javascript">
      $("li a").click(function(){
        $(this).find($(".fa-caret-down, .fa-caret-right")).toggleClass('fa-caret-down fa-caret-right');
      });
</script>