<!-- top navbar-->


<header>
  <div class="row">

    <!-- menu icon for mobile -->
    <div class="col-2 d-lg-none align-middle">
      <a id="sidebar-toggler" href="#"><h3>
          <i class="fa fa-bars" style="color:black;" aria-hidden="true"></i>
        </h3></a>
    </div>

    <!-- current page -->
    <div class="col-10 col-sm-4 text-center">
      <h4>{{Route::current()->getName()}}</h4>
    </div>

    <!-- project name -->
    <div class="col-md-3 text-center d-none d-lg-block">
      <h4 style=";">SheepIT</h4>
    </div>

    <!-- user name, opetions and image -->
    <div class="col-12 col-sm-5 col-md-5 offset-md-0 col-lg-5 offset-lg-0 text-right" id="userInfo">
      <h4>
        {{Auth::user()->username}}
      </h4>

      <h4>
        <a href="#" id="dropdownmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-caret-down" style="color: black;"></i></a>
        <div class="dropdown-menu dropdown-menu-right" id="dropdownUser" aria-labelledby="dropdownmenu">
        <a class="dropdown-item" href="#">Perfil</a>
        <a class="dropdown-item" href="Logout">Logout</a>
        </div>
      </h4>
        
      <img id="userimg" class="img-fluid img-responsive hidden-xs" src="/img/user/01.jpg">
    </div>
  </div>

<hr>
</header>

