<!doctype html>
<html>
<head>
    <!-- head with links, meta and scripts imports -->
    @include('includes.head')
</head>
<body class="theme-8">
<div class="layout-container">
    
    <!-- side menu with all options -->
    @include('includes.sidebarGuest')
    

    <!-- obfuscator div used for mobile menu-->
    <div class="sidebar-layout-obfuscator"></div>

    <main class="main-container">


        <!-- Page content-->
        <section>
          <div class="container-fluid">

            <!-- content of current page -->
            @yield('content')

          </div>
        </section>

    </main>



</div>
</body>
</html>
