<!doctype html>
<html lang="pt">
<head>
    <!-- head with links, meta and scripts imports -->
    @include('includes.head')
</head>
<body class="theme-8">
<div class="layout-container">
    
    <!-- side menu with all options -->
    @include('includes.sidebar')
    

    <!-- obfuscator div used for mobile menu-->
    <div class="sidebar-layout-obfuscator"></div>

    <main class="main-container">

        <!-- file with current page, project name and user with icon -->
        @include('includes.topnav')

        <!-- Page content-->
        <section>
          <div class="container-fluid">

            <!-- content of current page -->
            @yield('content')

          </div>
        </section>

        
        <!-- Page footer-->
        <footer>
            @include('includes.footer')
        </footer>
    </main>



</div>
</body>
</html>

<script type="text/javascript">
    // TODO : 
    //  Passar isto para um ficheiro geral
    $(document).on('click', '.text-toggle', function(event) {
        //event.preventDefault();
        //$(this).parent().children('.card-toggle').toggleClass("d-none")
        var item = $(this).parents(".card").children('.card-toggle');
        item.toggleClass("d-none");
        if(item.hasClass('d-none')){
            $(this).children('i').addClass('fa-caret-down');
            $(this).children('i').removeClass('fa-caret-up');            
        }
        else{
            $(this).children('i').addClass('fa-caret-up');
            $(this).children('i').removeClass('fa-caret-down');  
        }
        //fa fa-caret-down
    });
</script>