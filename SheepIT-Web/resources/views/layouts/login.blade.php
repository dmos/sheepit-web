<!doctype html>
<html>
<head>
    <!-- head with links, meta and scripts imports -->
    @include('includes.head')

    <style type="text/css">
        @media only screen and (min-width: 0px){
          .sidebar-container {
                -webkit-transform: translate3d(0, 0, 0);
                transform: translate3d(0, 0, 0);
            }
            .main-container {
                margin-left: 240px;
            }  
        }
    </style>
</head>
<body class="theme-8">
<div class="layout-container">
    
    <!-- side menu with all options -->
    <aside class="sidebar-container">
        <nav class="sidebar-nav">
            <ul>
                <li><h2 href="#" class="ripple"></i> &nbsp SheepIT</h2></li>            
            </ul>
        </nav>

        <li style="position: absolute; display: inline-block;bottom: 0; padding-bottom: 20px; left: 20px;">
            <img src="img/logo_it.png" style="width:180px; height: 50px;">
        </li>
    </aside>
    

    <!-- obfuscator div used for mobile menu-->
    <div class="sidebar-layout-obfuscator"></div>

    <main class="main-container">


        <!-- Page content-->
        <section>
          <div class="container-fluid">

            <!-- content of current page -->
            @yield('content')

          </div>
        </section>

    </main>



</div>
</body>
</html>
