@extends('layouts.login')
@section('content')

  <div class="row">

    <!-- basic error treatment
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
-->




      <div class="col-12">

        <h2>Login</h2>

        <form class="" action="{{ URL::to('/login')}}" method="post" id="form">
            {{ csrf_field() }}


          <div class="form-group form-group-md">
            <label for="username">Nome de Utilizador</label>
            <input type="text" class="form-control input-md {{($errors->first('username') ? " invalid" : "")}}" name="username" value="{{ old('username') }}">
            <span class="text-danger error">{{ $errors->first('username') }}</span>
          </div>

          <div class="form-group form-group-md">
            <label for="pass">Password</label>
            <input type="password" class="form-control input-md {{($errors->first('pass') ? " invalid" : "")}}" name="pass" value="{{ old('pass') }}" >
            <span class="text-danger error">{{ $errors->first('pass') }}</span>
          </div>
          <div class="form-group form-group-md">
              <span class="text-danger">{{ $errors->first('Error') }}</span>
          </div>
          <br>
          <div class="form-group form-group-md text-center">
            <input type="submit" class="btn btn-success btn-submit " name="btn" value="Entrar" >
          </div>
        </form>

      </div>

@stop
