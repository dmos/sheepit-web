@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>

<style>
	#herdTableScroll_wrapper{padding: 0px;}
</style>


<a href="{{ URL::to('/herd/new')}}"><button class="btn btn-primary">Novo Rebanho <i class="fa fa-plus"></i></button></a>

<table id="herdTableScroll" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Nome do Rebanho</th>
			<th>Cliente</th>
			<th>Numero de Animais</th>
			<th></th>
		</tr>
	</thead>

	<tbody>

	</tbody>
</table>



<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//datatable 

		/**
		 * $herdSearch contains String that is passed as a parameter to be searched on the List
		 * @type {String}
		 */
		var herdSearch =  '{{ $herdSearch }}' ;

		
		//createDatatable("#collarTable","/collarList",collarSearch);
		//
		//

		createDatatableScroll("#herdTableScroll","{{ URL::to('/herdList')}}",true,herdSearch,400,true,[-1],[0, "asc"]);


	} ); /* document.ready */
	    
</script>

@stop
