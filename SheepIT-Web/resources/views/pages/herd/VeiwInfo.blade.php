@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>


<style type="text/css">

#animalTableScroll_wrapper{padding: 0px;}

.sucesso{
	color: green;
}
.invalid{
	color: red;
}

</style>

<div class="row card-deck">
	<div class="col-md-12 offset-md-0 mb-3">
		<div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="row m-0">
				<div class="col-6 p-0 pt-1 text-toggle">Informações <i class="fa fa-caret-up"></i></div>
				<div class="col-6 pr-0 pl-0 text-right">
					<button id="editing" class="btn btn-sm  mr-1"><span>Editar</span> <i class="fa fa-edit"></i></button>
				</div>
				</div>
			</div>
			<div class="card-body card-toggle" id="infos">
				<form method="POST" id="form" action="{{ URL::to('/herd/view/'.$result[0]->pkherdid.'/update')}}">
					{{ csrf_field() }}
					<span class="text-danger" id="generalError"></span>

					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Nome do Rebanho:</lable>
						<div class="col-sm-6 p0">
							<span class="bold span">{{$result[0]->name}}</span>
							<input type="text" id="herdName" name="herdName" class="editable editable-input d-none bold" value="{{$result[0]->name}}" disabled="disabled"/>
							<span class="text-danger error" id="herdNameerror"></span>
						</div>
					</div>

					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Numero de Animais: </lable>
						<div class="col-sm-6 p0">
							<span class="bold">{{$result[0]->nanimals}}</span>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Cliente: </lable>
						<div class="col-sm-6 p0">
							<a class="col-sm-6 p0 bold span" href="{{URL::to('/client/view/'.$result[0]->pkenterpriseid)}}" id="clientnameSpan">
							{{$result[0]->clientname}}
							</a>
							<select class="editable editable-input d-none bold" id='clientname' name='clientname'>
							</select>
							<span class="text-danger error" id="clientnameerror"></span>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Propriedade: </lable>
						<div class="col-sm-6 p0">
							<a class="col-sm-6 p0 bold span" href="{{URL::to('/property/view/'.$result[0]->pkpropertyid)}}" id="propertydescriptionSpan">
							{{$result[0]->propertydescription}}
							</a>
							<select class="editable editable-input d-none bold" id='propertydescription' name='propertydescription'>
							</select>
							<span class="text-danger error" id="propertydescriptionerror"></span>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div> <!-- end of row -->


<div class="row card-deck">
	<div class="col-md-12 offset-md-0 mb-3">
		<div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="row m-0">
					<div class="col-6 p-0 pt-1 text-toggle">Lista de Animais <i class="fa fa-caret-up"></i></div>
					<div class="col-6 pr-0 pl-0 text-right">
						<button class="btn btn-sm btn-primary" id="getEdit"  data-toggle="modal" data-target="#modal" data-id="">Associar Animal <i class="fa fa-plus"></i></button>
					</div>
				</div>
			</div>
			<div class="card-body card-toggle">
				<table id="animalTableScroll" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID do Animal</th>
							<th>Data de Nascimento</th>
							<th>Peso</th>
							<th>Género</th>
							<th>Raça</th>
							<th>Coleira Associada</th>
							<th></th>
						</tr>
					</thead>

					<tbody>

					</tbody>
				</table>
			</div>
		</div>
	</div>

</div> <!-- end of row -->


<!-- modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header pt-0 pb-0">
				<h4>Associar Animal</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0" id="modal-body">
				<div class="row">
					<div class="col-sm-8 pl3" style="border-right: 1px solid gray">
						<h5>Animal Existente: </h5>
						<p>
							<lable class=" control-label">Lista de Animais: </lable>
							<select id='animalList' name='animalList'>
							</select>
						</p>
						<p>
							<label class=" control-label">Coleira Associada: </label>
							<span id="animalCollar"></span>
						</p>
						<p class="text-center">
							<button class="btn btn-sm btn-primary" id="addAnimal" name="addAnimal" disabled="disabled"> Associar Animal </button>
						</p>
						<p class="text-center">
							<span id="status"></span>
						</p>
					</div>
					<div class="col-sm-4">
						<h5>Novo Animal: </h5>
						<p class="text-center">							
							<a href="{{ URL::to('/animal/new')}}"><button class="btn btn-primary btn-sm">Novo Animal</button></a>
						</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn" data-dismiss="modal">Close</button>
				
			</div>
		</div>
	</div>
</div>


<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#modal').modal("hide");


	//Animal List datatable
	createDatatableScroll("#animalTableScroll","{{ URL::to('herd/view/'.$result[0]->pkherdid.'/animalList')}}",true,null,200,true,[-1],[0, "asc"]);

	//edieting
	$("#editing").click(function(event) {
		if($(this).children('span').html() == "Editar"){
			getSelectsInfos();
			$(".editable").prop("disabled",false);
			$(".editable").toggleClass('editable-input');			
			$(".editable").removeClass('d-none');
			$(".span").addClass('d-none');
			$(".hidden").removeClass('d-none');
			
			$(this).children('span').html("Concluir");
		}
		else{
			
			$.ajax({
				type: "POST",
				url: '{{ URL::to('/herd/view/'.$result[0]->pkherdid.'/update')}}',
				data: $("#form").serialize(),
			})
			.done(function(result) {
				console.log(result);
				$(".text-danger").empty();
				$("#generalError").empty();
				$(".editable").removeClass('invalid');

				if(result.status == "sucesso"){
					//VER! AO ATUALIZAR o redirect
					location.replace("{{ URL::to('/herd/view')}}/"+result.pkherdid);
				}
				if(result.status == "validation"){
					$.each(result.error , function( key, value ) {
						$("#"+key+"error").html("<br>"+value);
						$("#"+key).addClass('invalid');
					});
				}

				if(result.status == "error"){
					$("#generalError").html(result.error+"<br>");
				}
			});			
			//$("#form").submit();
		}
	});

	function getSelectsInfos(){
		var currentValues = {
			client:"{{$result[0]->pkenterpriseid}}",
			property:"{{$result[0]->pkpropertyid}}"}

		$.getJSON("{{ URL::to('/beacon/new/clienteList')}}",function(data) {
			//console.log(data);
			var ClientSelectHTML = "<option value='0'></option>";
			for (var i=0; i< data.length; i++) {
				ClientSelectHTML += "<option value="+data[i].pkenterpriseid+">"+data[i].enterprisename+"</option>"
			}
			$("#clientname").html(ClientSelectHTML);
			if(currentValues.client > 0){
				$("#clientname").val(currentValues.client).change();
			}
		});

		$("#clientname").change(function(){

			if($(this).val() < 1 || $(this).html() == ""){
				$("#propertydescription").prop('disabled', true);
				$("#propertydescription").empty();
				return;
			}
			$("#propertydescription").prop('disabled', false);
			
			$.getJSON("{{ URL::to('/beacon/new/propertyList')}}/"+$(this).val(),function(data) {
				
				var PropertySelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					PropertySelectHTML += "<option value="+data[i].pkpropertyid+">"+data[i].description+"</option>"
				}
				$("#propertydescription").html(PropertySelectHTML);
				if(currentValues.property > 0){
					$("#propertydescription").val(currentValues.property).change();
				}
				
			});
			
		});/* end of ClientSelect.change */
	}
	
	$('#modal').on('shown.bs.modal', function () {
	  	$.getJSON("{{ URL::to('herd/view/'.$result[0]->pkherdid.'/animalListAdd')}}",function(data) {
	  		console.log(data);
			if(data.length>0){
				var AnimalSelectHTML = "";
				for (var i=0; i< data.length; i++) {
					AnimalSelectHTML += "<option value="+data[i].pkanimalid+" data-collar='"+data[i].Asso_Collar+"'>"+data[i].animalidtag+"</option>"
					if(i == 0)
						$("#animalCollar").html(data[i].Asso_Collar);
				}
				$("#animalList").html(AnimalSelectHTML);
				$("#addAnimal").removeAttr('disabled');
				
			}
		});

	  	
	});

	$("#animalList").change(function(event) {
		$("#animalCollar").html($(this).find(':selected').data('collar'));
	});

	$("#addAnimal").click(function(event) {
		$("#status").removeClass();
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
		$.ajax({
			url: '{{ URL::to('herd/view/'.$result[0]->pkherdid.'/addAnimal')}}',
			type: 'POST',
			data: {Animal: $("#animalList").val(),_token: '{{csrf_token()}}'},
		})
		.done(function(data) {
			if(data.status == "sucesso"){
				$("#status").addClass('sucesso');
				$("#status").html(data.status);

				$("#animalList option[value='"+$("#animalList").val()+"']").remove();
				if($('#animalList > option').length == 0){
					$("#addAnimal").attr('disabled', 'disabled');
				}
			}
			else{
				$("#status").addClass('invalid');
				$("#status").html(data.error);
			}
		})		
	});

	$('#modal').on('hidden.bs.modal', function () {
		$("#status").html("");
		$("#animalCollar").html("");
		$('#animalTableScroll').DataTable().ajax.reload()
	  	//createDatatableScroll("#animalTableScroll","{//{ URL::to('herd/view/'.$result[0]->pkherdid.'/animalList')}}",true,null,200,true,[-1],[0, "asc"]);
	});
		

}); /* document.ready */

</script>

@stop
