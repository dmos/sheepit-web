@extends('layouts.sidebar')
@section('content')
  
<?php 
  // {{ dd(get_defined_vars()) }} // debug variables
?>

  <!-- INFORMATION -->
  <div class="row">
    <div class="col-md-4 mb-3">
        <div class="card text-black bg-card">
              <div class="card-header">Animais</div>
              <div class="card-body">
                <p class="card-text">Rebanhos: 
                  <a class="badge badge-secondary" href="#">2</a>
                  <a class="badge badge-success" href="#">1</a>
                </p>
                <p class="card-text">Ovelhas:
                  <a class="badge badge-secondary" href="#">{{ $numeroOvelhas['nAnimalTotal']}}</a>

                  <a class="badge badge-success" href="#">{{ $numeroOvelhas['nAnimalActive']}}</a>
                </p>
              </div>
        </div>
    </div>

    <div class="col-md-4 mb-3">
        <div class="card text-black bg-card">
          <div class="card-header">Comportamento</div>
          <div class="card-body">
            <p class="card-text">Overfence: 
              <a class="badge badge-danger" href="{{ url('alertas/OVERFENCE') }}"> 10 <i class="fa fa-exclamation-triangle"></i></a>
              <a class="badge badge-warning" href="{{ url('alertas/OVERFENCE') }}">{{ $numeroAlertasNaoVistos['OVERFENCE']}}</a>
            </p>
            <p class="card-text">Overposture: 
              <a class="badge badge-danger" href="{{ url('alertas/OVERPOSTURE') }}"> 20 <i class="fa fa-exclamation-triangle"></i></a>
              <a class="badge badge-warning" href="{{ url('alertas/OVERPOSTURE') }}">{{ $numeroAlertasNaoVistos['OVERPOSTURE'] }}</a>    
            </p>
          </div>
        </div>
    </div>

    <div class="col-md-4 mb-3">
        <div class="card text-black bg-card">
              <div class="card-header">Dispositivos</div>
              <div class="card-body">
                <p class="card-text">Coleiras:
                  <a class="badge badge-secondary" href="#">{{$numeroColeiras['nCollarTotal']}}</a>
                  <a class="badge badge-success" href="#">{{$numeroColeiras['nCollarActive']}}</a>
                  <a class="badge badge-danger" href="#">0 <i class="fa fa-exclamation-triangle"></i></a>
                </p>
                <p class="card-text">Faróis:
                  <a class="badge badge-secondary" href="#">{{ $numeroFarois }} </a>
                  <a class="badge badge-success" href="#">{{ $numeroFarois }} </a>
                  <a class="badge badge-danger" href="#">0 <i class="fa fa-exclamation-triangle"></i></a>
                </p>
              </div>
        </div>
    </div>

  </div> <!-- row --> 

  <div class="row">
    <div class="col-sm-12">
      Legenda:
      <p>
        <a class="badge badge-secondary" href="#">Registados</a>
        <a class="badge badge-success" href="#">Activos</a>
        <a class="badge badge-warning" href="#">Avisos</a>
        <a class="badge badge-danger" href="#">Alertas críticos</a>
      </p>
    </div>
  </div>

  <!-- end of INFORMATION -->
  

  <!-- Charts -->
  <div class="row">
    <div class="col-12 col-md-8 offset-md-2 text-center">
      <h4>Alertas</h4>
      <label id="alertasError"></label>
      <canvas id="Alertas"></canvas>
    </div>
  </div> <!-- fim da 'row' -->
  <!-- end of Charts -->




  <script type="text/javascript" src="{{ URL::to('/js/graphMaking.js')}}"></script>
  <script type="text/javascript">
      $(document).ready(function(){
          piechart("{{ URL::to('/alertasPie')}}","Alertas");
      });
  </script>

@stop
