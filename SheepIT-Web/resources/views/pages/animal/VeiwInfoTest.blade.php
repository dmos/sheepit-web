@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>



<style type="text/css">
	span.bold{font-weight: bold;}
	#HistoryTable_wrapper{padding: 0px;}
	#HistoryAlertsTable_wrapper{padding: 0px;}
	
</style>

<?php 
	//var_dump($result);exit;
?>



<div class="row card-deck">
    <div class="col-md-12 mb-3">
        <div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="col-4 p-0 pt-1 text-toggle">infrações de postura <i class="fa fa-caret-up"></i></div>
			</div>
			<div class="card-body card-toggle">
				<h6 class="mt0"> 
					Dia Inical: <input type="date" name="dayStart" id="dayStart">
					&nbsp;
					Dia Final: <input type="date" name="dayEnd" id="dayEnd">
					&nbsp;
					
					Tipo de Grafico: 
					<select id="chartType">
						<option value="1">infrações de postura</option>
						<option value="2">infrações de cerca</option>
						<option value="3">Numero de passos</option>
					</select>
				</h6>
				<canvas id="line" style="max-width: 98%;"></canvas>
			</div>
        </div>
    </div>

</div><!-- end of row -->


<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript" src="{{ URL::to('/js/graphMaking.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
		
	//datatable
	createDatatableScroll("#HistoryTable","{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/datatable')}}",true,null,100,true,[],[1, "desc"]);

	//Chart
	var dia = "{{$lastDay}}";
	dia = dia.split(" ");
	$("#dayStart").val(dia[0]);
	$("#dayEnd").val(dia[0]);

	lineChart("{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/lineChart')}}/"+$("#dayStart").val()+"/"+$("#dayEnd").val()+"/"+$("#chartType").val(),"line");

	$("#dayStart").change(function(event) {
		lineChart("{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/lineChart')}}/"+$("#dayStart").val()+"/"+$("#dayEnd").val()+"/"+$("#chartType").val(),"line");
	});

	$("#dayEnd").change(function(event) {
		lineChart("{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/lineChart')}}/"+$("#dayStart").val()+"/"+$("#dayEnd").val()+"/"+$("#chartType").val(),"line");
	});

	$("#chartType").change(function(){
		lineChart("{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/lineChart')}}/"+$("#dayStart").val()+"/"+$("#dayEnd").val()+"/"+$("#chartType").val(),"line");
	});
	

	
	//edieting
	$("#editing").click(function(event) {
		if($(this).children('span').html() == "Editar"){
			getSelectsInfos();
			$(".editable").prop("disabled",false);
			$(".editable").toggleClass('editable-input');			
			$(".editable").removeClass('d-none');
			$(".span").addClass('d-none');
			$(".hidden").removeClass('d-none');
			
			$(this).children('span').html("Concluir");
		}
		else{
			
			$.ajax({
				type: "POST",
				url: '{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/update')}}',
				data: $("#form").serialize(),
			})
			.done(function(result) {
				console.log(result);
				$(".text-danger").empty();
				$("#generalError").empty();
				$(".editable").removeClass('invalid');

				if(result.status == "sucesso"){
					//VER! AO ATUALIZAR o redirect
					location.replace("{{ URL::to('/animal/view')}}/"+result.animalidtag);
				}
				if(result.status == "validation"){
					$.each(result.error , function( key, value ) {
						$("#"+key+"error").html("<br>"+value);
						$("#"+key).addClass('invalid');
					});
				}

				if(result.status == "error"){
					$("#generalError").html(result.error+"<br>");
				}
			});	
		}
	});

	function getSelectsInfos(){
		var currentValues = {
			client:"{{$result[0]->pkenterpriseid}}",
			property:"{{$result[0]->pkpropertyid}}",
			collar:"{{$result[0]->pkcollarid}}",
			herd:"{{$result[0]->fkherdid}}"}
	
		$.getJSON("{{ URL::to('/animal/new/clienteList')}}",function(data) {
			//console.log(data);
			var ClientSelectHTML = "<option value='0'></option>";
			for (var i=0; i< data.length; i++) {
				ClientSelectHTML += "<option value="+data[i].pkenterpriseid+">"+data[i].enterprisename+"</option>"
			}
			$("#ClientSelect").html(ClientSelectHTML);
			if(currentValues.client > 0){
				$("#ClientSelect").val(currentValues.client).change();
			}
		});

		$("#ClientSelect").change(function(){

			if($(this).val() < 1 || $(this).html() == ""){
				$("#PropertySelect").prop('disabled', true);
				$("#PropertySelect").empty();
				$("#HerdSelect").prop('disabled', true);
				$("#HerdSelect").empty();
				$("#CollarSelect").prop('disabled', true);
				$("#CollarSelect").empty();
				return;
			}
			$("#PropertySelect").prop('disabled', false);
			
			$.getJSON("{{ URL::to('/animal/new/propertyList')}}/"+$(this).val(),function(data) {
				
				var PropertySelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					PropertySelectHTML += "<option value="+data[i].pkpropertyid+">"+data[i].description+"</option>"
				}
				$("#PropertySelect").html(PropertySelectHTML);
				if(currentValues.property > 0){
					$("#PropertySelect").val(currentValues.property).change();
				}
				
			});
			
		});/* end of ClientSelect.change */

		$("#PropertySelect").change(function(){

			if($(this).val() < 1 || $(this).html() == ""){
				$("#HerdSelect").prop('disabled', true);
				$("#CollarSelect").prop('disabled', true);
				$("#HerdSelect").empty();
				return;
			}
			$("#HerdSelect").prop('disabled', false);
			$("#CollarSelect").prop('disabled', false);

			
			$.getJSON("{{ URL::to('/animal/new/herdList')}}/"+$(this).val(),function(data) {
				var HerdSelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					HerdSelectHTML += "<option value="+data[i].pkherdid+">"+data[i].name+"</option>"
				}
				$("#HerdSelect").html(HerdSelectHTML);
				if(currentValues.herd > 0){
					$("#HerdSelect").val(currentValues.herd).change();
				}
				
			});
			$.getJSON("{{ URL::to('/animal/update/collarListUpdate')}}/"+$(this).val(),function(data) {
				var CollarSelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					CollarSelectHTML += "<option value="+data[i].pkcollarid+">"+data[i].serialnumber+"</option>"
				}
				$("#CollarSelect").html(CollarSelectHTML);
				if(currentValues.collar > 0){
					$("#CollarSelect").val(currentValues.collar).change();
				}
				
			});
			
		}); /* end of PropertySelect.change */
	}
		
}); /* document.ready */
	
	    
</script>

@stop
