@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>

<style>
	#animalTableScroll_wrapper{padding: 0px;}
</style>


<a href="{{ URL::to('/animal/new')}}"><button class="btn btn-primary">Novo Animal <i class="fa fa-plus"></i></button></a>

<table id="animalTableScroll" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>ID do Animal</th>
			<th>Data de Nascimento</th>
			<th>Peso</th>
			<th>Género</th>
			<th>Raça</th>
			<th>Coleira Associada</th>
			<th>Rebanho Assoiado</th>
			<th></th>
		</tr>
	</thead>

	<tbody>

	</tbody>
</table>



<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {		
		//datatable 
		/**
		 * $animalSearch contains String that is passed as a parameter to be searched on the List
		 * @type {String}
		 */
		var animalSearch =  '{{ $animalSearch }}' ;

		createDatatableScroll("#animalTableScroll","{{ URL::to('/animalList')}}",true,animalSearch,400,true,[-1],[0, "asc"]);


	} ); /* document.ready */
	    
</script>

@stop
