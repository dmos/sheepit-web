@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>


<style type="text/css">
	span.bold{font-weight: bold;}
	#HistoryTable_wrapper{padding: 0px;}
	#HistoryAlertsTable_wrapper{padding: 0px;}
	
</style>

<?php 
	//var_dump($result);exit;
?>

<div class="row card-deck">
    <div class="col-md-5 mb-3">
        <div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="row m-0">
				<div class="col-4 p-0 pt-1 text-toggle">Informações <i class="fa fa-caret-up"></i></div>
				<div class="col-8 pr-0 pl-0">
					@if($result[0]->Asso_Collar)
						<a href="{{ URL::to('/collar/view/'.$result[0]->Asso_Collar.'/map')}}" class="btn btn-success btn-sm float-right mr-1">Ver no Mapa <i class="fa fa-map"></i></a>
					@endif
					<button id="editing" class="btn btn-sm float-right mr-1"><span>Editar</span> <i class="fa fa-edit"></i></button>
				</div>
				</div>
			</div>
			<div class="card-body card-toggle" id="infos">
				<form method="POST" id="form" action="{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/update')}}">
					{{ csrf_field() }}
					<span class="text-danger" id="generalError"></span>

					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">ID do Animal:</lable>
						<div class="col-sm-6 p0">
							<span class="bold span">{{$result[0]->animalidtag}}</span>
							<input type="text" id="AnimalIDInput" name="AnimalIDInput" class="editable editable-input d-none bold" value="{{$result[0]->animalidtag}}" disabled="disabled"/>
							<span class="text-danger error" id="AnimalIDInputerror"></span>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Data de Nascimento:</lable>
						<div class="col-sm-6 p0">
							<span class="bold span" id="birthdaySpan">{{$birthdata}}</span>
							<input type="date" class="editable editable-input d-none bold" id="BirthDateInput" name="BirthDateInput" value="{{$result[0]->birthdata}}">
							<span class="text-danger error" id="BirthDateInputerror"></span>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Peso:</lable>
						<div class="col-sm-6 p0">
							<span class="bold span">{{$result[0]->weight}} kg</span>
							<input type="text" id="WeigthInput" name="WeigthInput" class="editable editable-input d-none bold" value="{{$result[0]->weight}}" disabled="disabled"/>
							<span class="text-danger error" id="WeigthInputerror"></span>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Género:</lable>
						<div class="col-sm-6 p0">
							<span class="bold span">{{$result[0]->gender}}</span>
							<select class="editable editable-input d-none bold" name="GenderInput" id="GenderInput">
							@if($result[0]->gender == "Feminino")
						   		<option value="F" selected>Feminino</option>
								<option value="M">Masculino</option>
							@else
							    <option value="F">Feminino</option>
								<option value="M" selected>Masculino</option>
							@endif
							</select>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Raça:</lable>
						<div class="col-sm-6 p0">
							<span class="bold span">{{$result[0]->breed}}</span>
							<input type="text" id="BreedInput" name="BreedInput" class="editable editable-input d-none bold" value="{{$result[0]->breed}}" disabled="disabled"/>
							<span class="text-danger error" id="BreedInputerror"></span>
						</div>
					</div>

					<div class="form-group row mb-1 hidden d-none">
						<label class="col-sm-6 pl1 control-label" for="ClientSelect">Cliente Associado:</label>
						<div class="col-sm-6 p0">
							<select class="editable editable-input bold" id="ClientSelect" name="ClientSelect">
							</select>
						</div>
					</div>
					<div class="form-group row mb-1 hidden d-none">
						<label class="col-sm-6 pl1 control-label" for="PropertySelect">Propriedade:</label>
						<div class="col-sm-6 p0">
							<select class="editable editable-input bold" id="PropertySelect" name="PropertySelect" disabled="disabled">
							</select>
						</div>
					</div>

					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Coleira Associada:</lable>
						<div class="col-sm-6 p0">
							<a class="span" href="{{URL::to('/collar/view/'.$result[0]->Asso_Collar)}}"> <span class="bold">{{$result[0]->Asso_Collar}}</span> </a>
							<select class="editable editable-input d-none bold" id="CollarSelect" name="CollarSelect">
							</select>
							<span class="text-danger error" id="CollarSelecterror"></span>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Rebanho Assoiado:</lable>
						<div class="col-sm-6 p0">
							<a href="{{ URL::to('/herd/view/'.$result[0]->fkherdid)}}" class="span"> <span class="bold">{{$result[0]->herdName}}</span></a>
							<select class="editable editable-input d-none bold" id="HerdSelect" name="HerdSelect">
							</select>
							<span class="text-danger error" id="HerdSelecterror"></span>
						</div>
					</div>
				</form>
			</div>
        </div>
    </div>
	

	<div class="col-md-7 mb-3">
        <div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="col-4 p-0 pt-1 text-toggle">Histórico de Atividade <i class="fa fa-caret-up"></i></div>
				
			</div>
			<div class="card-body card-toggle">
				<table id="HistoryTable" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Farol mais próximo</th>
							<th>Data Hora</th>
							<th>Nº Passos</th>
							<th>Nº Infrações</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
        </div>
    </div>

</div> <!-- end of row -->


<div class="row card-deck">
    <div class="col-md-12 mb-3">
        <div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="col-4 p-0 pt-1 text-toggle">Gráficos <i class="fa fa-caret-down"></i></div>
			</div>
			<div class="card-body card-toggle d-none">
				<h6 class="mt0"> 
					Dia Inical: <input type="date" name="dayStart" id="dayStart">
					&nbsp;
					Dia Final: <input type="date" name="dayEnd" id="dayEnd">
					&nbsp;
					
					Tipo de Atividade: 
					<select id="chartType">
						<option value="1">infrações de postura</option>
						<option value="2">infrações de cerca</option>
						<option value="3">Numero de passos</option>
					</select>
				</h6>
				<canvas id="line"></canvas>
			</div>
        </div>
    </div>

</div><!-- end of row -->


<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript" src="{{ URL::to('/js/graphMaking.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
		
	//datatable
	createDatatableScroll("#HistoryTable","{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/datatable')}}",true,null,100,true,[],[1, "desc"]);

	//Chart
	var dia = "{{$lastDay}}";
	dia = dia.split(" ");
	$("#dayStart").val(dia[0]);
	$("#dayEnd").val(dia[0]);

	lineChartTime("{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/lineChart')}}/"+$("#dayStart").val()+"/"+$("#dayEnd").val()+"/"+$("#chartType").val(),"line");

	$("#dayStart").change(function(event) {
		lineChartTime("{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/lineChart')}}/"+$("#dayStart").val()+"/"+$("#dayEnd").val()+"/"+$("#chartType").val(),"line");
	});

	$("#dayEnd").change(function(event) {
		lineChartTime("{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/lineChart')}}/"+$("#dayStart").val()+"/"+$("#dayEnd").val()+"/"+$("#chartType").val(),"line");
	});

	$("#chartType").change(function(){
		lineChartTime("{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/lineChart')}}/"+$("#dayStart").val()+"/"+$("#dayEnd").val()+"/"+$("#chartType").val(),"line");
	});
	

	
	//edieting
	$("#editing").click(function(event) {
		if($(this).children('span').html() == "Editar"){
			getSelectsInfos();
			$(".editable").prop("disabled",false);
			$(".editable").toggleClass('editable-input');			
			$(".editable").removeClass('d-none');
			$(".span").addClass('d-none');
			$(".hidden").removeClass('d-none');
			
			$(this).children('span').html("Concluir");
		}
		else{
			
			$.ajax({
				type: "POST",
				url: '{{ URL::to('/animal/view/'.$result[0]->animalidtag.'/update')}}',
				data: $("#form").serialize(),
			})
			.done(function(result) {
				console.log(result);
				$(".text-danger").empty();
				$("#generalError").empty();
				$(".editable").removeClass('invalid');

				if(result.status == "sucesso"){
					//VER! AO ATUALIZAR o redirect
					location.replace("{{ URL::to('/animal/view')}}/"+result.animalidtag);
				}
				if(result.status == "validation"){
					$.each(result.error , function( key, value ) {
						$("#"+key+"error").html("<br>"+value);
						$("#"+key).addClass('invalid');
					});
				}

				if(result.status == "error"){
					$("#generalError").html(result.error+"<br>");
				}
			});	
		}
	});

	function getSelectsInfos(){
		var currentValues = {
			client:"{{$result[0]->pkenterpriseid}}",
			property:"{{$result[0]->pkpropertyid}}",
			collar:"{{$result[0]->pkcollarid}}",
			herd:"{{$result[0]->fkherdid}}"}
	
		$.getJSON("{{ URL::to('/animal/new/clienteList')}}",function(data) {
			//console.log(data);
			var ClientSelectHTML = "<option value='0'></option>";
			for (var i=0; i< data.length; i++) {
				ClientSelectHTML += "<option value="+data[i].pkenterpriseid+">"+data[i].enterprisename+"</option>"
			}
			$("#ClientSelect").html(ClientSelectHTML);
			if(currentValues.client > 0){
				$("#ClientSelect").val(currentValues.client).change();
			}
		});

		$("#ClientSelect").change(function(){

			if($(this).val() < 1 || $(this).html() == ""){
				$("#PropertySelect").prop('disabled', true);
				$("#PropertySelect").empty();
				$("#HerdSelect").prop('disabled', true);
				$("#HerdSelect").empty();
				$("#CollarSelect").prop('disabled', true);
				$("#CollarSelect").empty();
				return;
			}
			$("#PropertySelect").prop('disabled', false);
			
			$.getJSON("{{ URL::to('/animal/new/propertyList')}}/"+$(this).val(),function(data) {
				
				var PropertySelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					PropertySelectHTML += "<option value="+data[i].pkpropertyid+">"+data[i].description+"</option>"
				}
				$("#PropertySelect").html(PropertySelectHTML);
				if(currentValues.property > 0){
					$("#PropertySelect").val(currentValues.property).change();
				}
				
			});
			
		});/* end of ClientSelect.change */

		$("#PropertySelect").change(function(){

			if($(this).val() < 1 || $(this).html() == ""){
				$("#HerdSelect").prop('disabled', true);
				$("#CollarSelect").prop('disabled', true);
				$("#HerdSelect").empty();
				return;
			}
			$("#HerdSelect").prop('disabled', false);
			$("#CollarSelect").prop('disabled', false);

			
			$.getJSON("{{ URL::to('/animal/new/herdList')}}/"+$(this).val(),function(data) {
				var HerdSelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					HerdSelectHTML += "<option value="+data[i].pkherdid+">"+data[i].name+"</option>"
				}
				$("#HerdSelect").html(HerdSelectHTML);
				if(currentValues.herd > 0){
					$("#HerdSelect").val(currentValues.herd).change();
				}
				
			});
			$.getJSON("{{ URL::to('/animal/update/collarListUpdate')}}/"+$(this).val(),function(data) {
				var CollarSelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					CollarSelectHTML += "<option value="+data[i].pkcollarid+">"+data[i].serialnumber+"</option>"
				}
				$("#CollarSelect").html(CollarSelectHTML);
				if(currentValues.collar > 0){
					$("#CollarSelect").val(currentValues.collar).change();
				}
				
			});
			
		}); /* end of PropertySelect.change */
	}
		
}); /* document.ready */
	
	    
</script>

@stop
