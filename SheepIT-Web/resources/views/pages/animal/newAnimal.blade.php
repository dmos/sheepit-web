@extends('layouts.sidebar')
@section('content')


<div class="row">
    <div class="col-md-6 mb-3 offset-md-3">
		<form  action="{{ URL::to('/animal/new')}}" method="post" id="form">
			{{ csrf_field() }}
			<div class="form-group form-group-md">
            	<span class="text-danger">{{ $errors->first('Error') }}</span>
          	</div>
          	
			<div class="form-group mb-3">
				<label for="AnimalIDInput">ID do Animal:</label>
				<input type="text" class="form-control {{($errors->first('AnimalIDInput') ? " invalid" : "")}}" id="AnimalIDInput" name="AnimalIDInput" placeholder="ex: 200" value="{{ old('AnimalIDInput') }}">
				<span class="text-danger">{{ $errors->first('AnimalIDInput') }}</span>
			</div>
			<div class="form-group mb-3">
				<label for="BirthDateInput">Data de Nascimento:</label>
				<input type="date" class="form-control {{($errors->first('BirthDateInput') ? " invalid" : "")}}" id="BirthDateInput" name="BirthDateInput" value="{{ old('BirthDateInput') }}">
				<span class="text-danger">{{ $errors->first('BirthDateInput') }}</span>
			</div>
			
			<div class="form-group mb-3">
				<label for="WeigthInput">Peso:</label>
				<input type="text" class="form-control {{($errors->first('WeigthInput') ? " invalid" : "")}}" id="WeigthInput" name="WeigthInput" placeholder="ex: 40" value="{{ old('WeigthInput') }}">
				<span class="text-danger">{{ $errors->first('WeigthInput') }}</span>
			</div>

			<div class="form-group mb-3">
				<label for="GenderInput">Genero:</label>
				<select class="form-control {{($errors->first('GenderInput') ? " invalid" : "")}}" id="GenderInput" name="GenderInput">
					<option value="F">Feminino</option>
					<option value="M">Masculino</option>
				</select>
				<span class="text-danger">{{ $errors->first('GenderInput') }}</span>
			</div>

			<div class="form-group mb-3">
				<label for="BreedInput">Raça:</label>
				<input type="text" class="form-control {{($errors->first('BreedInput') ? " invalid" : "")}}" id="BreedInput" name="BreedInput" placeholder="ex: Dorper" value="{{ old('BreedInput') }}">
				<span class="text-danger">{{ $errors->first('BreedInput') }}</span>
			</div>

			<div class="form-group mb-3">
				<label for="ClientSelect">Cliente Associado:</label>
				<select class="form-control" id="ClientSelect" name="ClientSelect">
				</select>
			</div>
			<div class="form-group mb-3">
				<label for="PropertySelect">Propriedade:</label>
				<select class="form-control" id="PropertySelect" name="PropertySelect" disabled="disabled">
				</select>
			</div>
			<div class="form-group mb-3">
				<label for="HerdSelect">Rebanho Associado:</label>
				<select class="form-control" id="HerdSelect" name="HerdSelect" disabled="disabled">
				</select>
			</div>
			<div class="form-group mb-3">
				<label for="CollarSelect">coleira Associado:</label>
				<select class="form-control" id="CollarSelect" name="CollarSelect" disabled="disabled">
				</select>
			</div>
			<br>
			<div class="form-group mb-3">
				<div class="col text-right p-0">
					<input type="submit" class="btn btn-success btn-submit " name="btn" value="Registar" >
					<a href="{{ URL::to('/animal')}}" class="btn btn-danger">Cancelar</a>
				</div>
			</div>
			

		</form>
		
	</div>
</div><!-- end of row -->


<script type="text/javascript">
	$(document).ready(function() {	
		$.getJSON("{{ URL::to('/animal/new/clienteList')}}",function(data) {
			//console.log(data);
			var ClientSelectHTML = "<option value='0'></option>";
			for (var i=0; i< data.length; i++) {
				ClientSelectHTML += "<option value="+data[i].pkenterpriseid+">"+data[i].enterprisename+"</option>"
			}
			$("#ClientSelect").html(ClientSelectHTML);
			var oldClient = "{{old('ClientSelect')}}";
			if(oldClient > 0){
				$("#ClientSelect").val(oldClient).change();
			}
		});

		$("#ClientSelect").change(function(){

			if($(this).val() < 1 || $(this).html() == ""){
				$("#PropertySelect").prop('disabled', true);
				$("#PropertySelect").empty();
				$("#HerdSelect").prop('disabled', true);
				$("#HerdSelect").empty();
				$("#CollarSelect").prop('disabled', true);
				$("#CollarSelect").empty();
				return;
			}
			$("#PropertySelect").prop('disabled', false);
			
			$.getJSON("{{ URL::to('/animal/new/propertyList')}}/"+$(this).val(),function(data) {
				
				var PropertySelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					PropertySelectHTML += "<option value="+data[i].pkpropertyid+">"+data[i].description+"</option>"
				}
				$("#PropertySelect").html(PropertySelectHTML);
				var oldProperty = "{{old('PropertySelect')}}";
				if(oldProperty > 0){
					$("#PropertySelect").val(oldProperty).change();
				}
				
			});
			
		});/* end of ClientSelect.change */

		$("#PropertySelect").change(function(){

			if($(this).val() < 1 || $(this).html() == ""){
				$("#HerdSelect").prop('disabled', true);
				$("#CollarSelect").prop('disabled', true);
				$("#HerdSelect").empty();
				return;
			}
			$("#HerdSelect").prop('disabled', false);
			$("#CollarSelect").prop('disabled', false);

			
			$.getJSON("{{ URL::to('/animal/new/herdList')}}/"+$(this).val(),function(data) {
				var HerdSelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					HerdSelectHTML += "<option value="+data[i].pkherdid+">"+data[i].name+"</option>"
				}
				$("#HerdSelect").html(HerdSelectHTML);
				var oldHerd = "{{old('HerdSelect')}}";
				if(oldHerd > 0){
					$("#HerdSelect").val(oldHerd).change();
				}
				
			});

			$.getJSON("{{ URL::to('/animal/new/collarList')}}/"+$(this).val(),function(data) {
				var CollarSelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					CollarSelectHTML += "<option value="+data[i].pkcollarid+">"+data[i].serialnumber+"</option>"
				}
				$("#CollarSelect").html(CollarSelectHTML);
				var oldCollar = "{{old('CollarSelect')}}";
				if(oldCollar > 0){
					$("#CollarSelect").val(oldCollar).change();
				}
				
			});
			
		}); /* end of PropertySelect.change */

	}); /* document.ready */
	
</script>

@stop
