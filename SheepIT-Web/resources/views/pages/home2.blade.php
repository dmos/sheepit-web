@extends('layouts.sidebar')
@section('content')
  
    <?php 
      // {{ dd(get_defined_vars()) }} // debug variables
    ?>

    <link rel="stylesheet" href="{{URL::to('css/app_green2.css')}}">
    <link rel="stylesheet" href="{{URL::to('css/custom2.css')}}">

    <style type="text/css">
        .theme-8 .layout-container > aside{
            /*1- so este ate gostei
            background-color: #467135;
            */

        }

        #topbar{
            /* 0 -original green */
            background-color: #C3E7AB
            
            /* 1 
            background-color: #E1F3D5
            */
        }
    </style>
  
  <div class="row mb-3">
      <div class="col-sm-12">
        <div class="setting-color" data-sidebar="#66B032" data-topbar="#C3E7AB" style="display: inline-block;">
            <div class="colorpallet" style="background-color:#66B032 ;width: 30px; height: 30px;"></div>
            <div class="colorpallet" style="background-color:#C3E7AB ;width: 30px; height: 30px;"></div>
        </div>
        <div class="setting-color" data-sidebar="#467135" data-topbar="#C3E7AB" style="display: inline-block;">
            <div class="colorpallet" style="background-color:#467135 ;width: 30px; height: 30px;"></div>
            <div class="colorpallet" style="background-color:#C3E7AB ;width: 30px; height: 30px;"></div>
        </div>
        <div class="setting-color" data-sidebar="#467135" data-topbar="#E1F3D5"  style="display: inline-block;">
            <div class="colorpallet" style="background-color:#467135 ;width: 30px; height: 30px;"></div>
            <div class="colorpallet" style="background-color:#E1F3D5 ;width: 30px; height: 30px;"></div>
        </div>
        <div class="setting-color" data-sidebar="#518634" data-topbar="#E1F3D5"  style="display: inline-block;">
            <div class="colorpallet" style="background-color:#518634 ;width: 30px; height: 30px;"></div>
            <div class="colorpallet" style="background-color:#E1F3D5 ;width: 30px; height: 30px;"></div>
        </div>
        <div class="setting-color" data-sidebar="#5B9B33" data-topbar="#E1F3D5"  style="display: inline-block;">
            <div class="colorpallet" style="background-color:#5B9B33 ;width: 30px; height: 30px;"></div>
            <div class="colorpallet" style="background-color:#C3E7AB ;width: 30px; height: 30px;"></div>
        </div>
        
      </div>
  </div>

    <script type="text/javascript">
        $(".setting-color").click(function(event) {
            $(".theme-8 .layout-container > aside").css("background-color", $(this).data('sidebar'));
            $("#topbar").css("background-color", $(this).data('topbar'));
        });
    </script>

  <!-- INFORMATION -->
  <div class="row">
    <div class="col-md-4 mb-3">
        <div class="card text-black bg-card">
              <div class="card-header">Animais</div>
              <div class="card-body">
                <p class="card-text">Rebanhos 
                  <a class="badge badge-secondary" href="#">2</a>
                  <a class="badge badge-success" href="#">1</a>
                </p>
                <p class="card-text">Ovelhas
                  <a class="badge badge-secondary" href="#">{{ $numeroOvelhas }}</a>
                  <a class="badge badge-success" href="#">4000</a>
                </p>
              </div>
        </div>
    </div>

    <div class="col-md-4 mb-3">
        <div class="card text-black bg-card">
          <div class="card-header">Comportamento</div>
          <div class="card-body">
            <p class="card-text">Overfence 
              <a class="badge badge-danger" href="{{ url('alertas/OVERFENCE') }}"> 10 <i class="fa fa-exclamation-triangle"></i></a>
              <a class="badge badge-warning" href="{{ url('alertas/OVERFENCE') }}">{{ $numeroAlertasNaoVistos['OVERFENCE']}}</a>
            </p>
            <p class="card-text">Overposture 
              <a class="badge badge-danger" href="{{ url('alertas/OVERPOSTURE') }}"> 20 <i class="fa fa-exclamation-triangle"></i></a>
              <a class="badge badge-warning" href="{{ url('alertas/OVERPOSTURE') }}">{{ $numeroAlertasNaoVistos['OVERPOSTURE'] }}</a>    
            </p>
          </div>
        </div>
    </div>

    <div class="col-md-4 mb-3">
        <div class="card text-black bg-card">
              <div class="card-header">Dispositivos</div>
              <div class="card-body">
                <p class="card-text">Coleiras:
                  <a class="badge badge-secondary" href="#">{{ $numeroColeiras }}</a>
                  <a class="badge badge-success" href="#">300</a>
                  <a class="badge badge-danger" href="#">20 <i class="fa fa-exclamation-triangle"></i></a>
                </p>
                <p class="card-text">Faróis:
                  <a class="badge badge-secondary" href="#">{{ $numeroFarois }} </a>
                  <a class="badge badge-success" href="#">100</a>
                  <a class="badge badge-danger" href="#">40 <i class="fa fa-exclamation-triangle"></i></a>
                </p>
              </div>
        </div>
    </div>

  </div> <!-- row --> 

  <div class="row">
    <div class="col-sm-4 offset-md-8">
      Legenda:
      <p>
        <a class="badge badge-secondary" href="#">Registados</a>
        <a class="badge badge-success" href="#">Activos</a>
        <a class="badge badge-warning" href="#">Avisos</a>
        <a class="badge badge-danger" href="#">Alertas críticos</a>
      </p>
    </div>
  </div>

  <!-- end of INFORMATION -->
  

  <!-- Charts -->
  <div class="row">
    <div class="col-12 col-md-8 offset-md-2 text-center">
      <h4>Alertas</h4>
      <label id="alertasError"></label>
      <canvas id="Alertas"></canvas>
    </div>
  </div> <!-- fim da 'row' -->
  <!-- end of Charts -->




  <script type="text/javascript" src="{{ URL::to('/js/graphMaking.js')}}"></script>
  <script type="text/javascript">
      $(document).ready(function(){
          piechart("{{ URL::to('/alertasPie')}}","Alertas");
      });
  </script>

@stop
