@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>

<style>
	#userTableScroll_wrapper{padding: 0px;}
</style>

<a href="{{ URL::to('/user/new')}}"><button class="btn btn-primary">Novo Utilizador <i class="fa fa-plus"></i></button></a>

<table id="userTableScroll" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Username</th>
			<th>Nome</th>
			<th>Cliente</th>
			<th></th>
		</tr>
	</thead>

	<tbody>

	</tbody>
</table>



<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {

		
		//datatable 

		/**
		 * $userSearch contains String that is passed as a parameter to be searched on the List
		 * @type {String}
		 */
		var userSearch =  '{{ $userSearch }}' ;

		createDatatableScroll("#userTableScroll","{{ URL::to('/userList')}}",true,userSearch,400,true,[-1],[0, "asc"]);


	} ); /* document.ready */
	    
</script>

@stop
