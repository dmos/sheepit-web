@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>


<style type="text/css">

#HistoryTable_wrapper{padding: 0px;}
#HistoryAlertsTable_wrapper{padding: 0px;}

</style>

<div class="row card-deck">
	<div class="col-md-12 offset-md-0 mb-3">
		<div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="row m-0">
				<div class="col-6 p-0 pt-1 text-toggle">Informações <i class="fa fa-caret-up"></i></div>
				<div class="col-6 pr-0 pl-0 text-right">
					<button id="editing" class="btn btn-sm  mr-1"><span>Editar</span> <i class="fa fa-edit"></i></button>
				</div>
				</div>
			</div>
			<div class="card-body card-toggle" id="infos">
				
				<form method="POST" id="form" action="{{ URL::to('/user/view/'.$result[0]->pkuserid.'/update')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<span class="text-danger" id="generalError"></span>
					<div class="row">
					<div class="col-md-10 mb-1">
						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Primeiro Nome:</lable>
							<div class="col-sm-6 p0">
								<span class="bold span">{{$result[0]->name}}</span>
								<input type="text" id="nameInput" name="nameInput" class="editable editable-input d-none bold" value="{{$result[0]->name}}" disabled="disabled"/>
								<span class="text-danger error" id="nameInputerror"></span>
							</div>
						</div>
						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Último Nome:</lable>
							<div class="col-sm-6 p0">
								<span class="bold span">{{$result[0]->surname}}</span>
								<input type="text" id="surnameInput" name="surnameInput" class="editable editable-input d-none bold" value="{{$result[0]->surname}}" disabled="disabled"/>
								<span class="text-danger error" id="surnameInputerror"></span>
							</div>
						</div>
						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Email:</lable>
							<div class="col-sm-6 p0">
								<span class="bold span">{{$result[0]->email}}</span>
								<input type="email" id="emailInput" name="emailInput" class="editable editable-input d-none bold" value="{{$result[0]->email}}" disabled="disabled"/>
								<span class="text-danger error" id="emailInputerror"></span>
							</div>
						</div>

						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Username:</lable>
							<div class="col-sm-6 p0">
								<span class="bold span">{{$result[0]->username}}</span>
								<input type="text" id="usernameInput" name="usernameInput" class="editable editable-input d-none bold" value="{{$result[0]->username}}" disabled="disabled"/>
								<span class="text-danger error" id="usernameInputerror"></span>
							</div>
						</div>

						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Cliente: </lable>
							<div class="col-sm-6 p0">
								<a class="col-sm-6 p0 bold span" href="{{URL::to('/client/view/'.$result[0]->fkenterpriseid)}}" id="clientnameSpan">
								{{$result[0]->clientname}}
								</a>
								<select class="editable editable-input d-none bold" id='clientname' name='clientname'>
								</select>
								<span class="text-danger error" id="clientnameerror"></span>
							</div>
						</div>
						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Tipo de Utilizador: </lable>
							<div class="col-sm-6 p0">
								<span class="bold span">{{$result[0]->userType}}</span>
								<select class="editable editable-input d-none bold" id='UserType' name='UserType'>
								</select>
								<span class="text-danger error" id="UserTypeerror"></span>
							</div>
						</div>
					</div>
					<div class="col-md-2 text-center">
						<p>	
						@if(file_exists(public_path('img/user/'.$result[0]->pkuserid.'.jpg'))) 
						<img class="userimg img-fluid img-responsive hidden-xs" src="{{ url('/img/user/'.$result[0]->pkuserid.'.jpg') }}">
						@else
							<img class="userimg img-fluid img-responsive hidden-xs" src="{{ url('/img/user/default.jpg') }}">
						@endif
						</p>

						<label class="control-label editable d-none">Alterar Foto: </label>
						<br>
						<button type="button" id="photoInputButton" class="btn btn-sm editable d-none"> Escolher Ficheiro</button>
						<input type="file" class="d-none" id="photoInput" name="photoInput" style="color:transparent; width: 100%;">
						<span id="photoInputText"></span>
					
						<span class="text-danger error" id="photoInputerror"></span>
					</div>

					</div> <!-- end of row -->
				</form>
			</div>
		</div>
	</div>

</div> <!-- end of row -->




<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript" src="{{ URL::to('/js/graphMaking.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	$("#photoInputButton").click(function(event) {
		$('#photoInput').click();
	});

	$(document).on('change','#photoInput' , function(event){
		var nomeFicheiro = $(this).val().split("\\");
		$("#photoInputText").html(nomeFicheiro.pop());
	});

	//edieting
	$("#editing").click(function(event) {
		if($(this).children('span').html() == "Editar"){
			getSelectsInfos();
			$(".editable").prop("disabled",false);
			$(".editable").toggleClass('editable-input');			
			$(".editable").removeClass('d-none');
			$(".span").addClass('d-none');
			$(".hidden").removeClass('d-none');
			
			$(this).children('span').html("Concluir");
		}
		else{
			var formData = new FormData($("#form")[0]);
			$.ajax({
				type: "POST",
				url: '{{ URL::to('/user/view/'.$result[0]->pkuserid.'/update')}}',
				data: formData,
				processData: false,
				contentType: false,
			})
			.done(function(result) {
				$(".text-danger").empty();
				$("#generalError").empty();
				$(".editable").removeClass('invalid');

				if(result.status == "sucesso"){
					location.replace("{{ URL::to('/user/view')}}/"+result.userid);
				}
				if(result.status == "validation"){
					$.each(result.error , function( key, value ) {
						$("#"+key+"error").html("<br>"+value);
						$("#"+key).addClass('invalid');
					});
				}

				if(result.status == "error"){
					$("#generalError").html(result.error+"<br>");
				}
			});			
			//$("#form").submit();
		}
	});

	function getSelectsInfos(){
		var currentValues = {
			client:"{{$result[0]->fkenterpriseid}}",
			userType:"{{$result[0]->fkusertypeid}}"}

		$.getJSON("{{ URL::to('/user/new/clienteList')}}",function(data) {
			//console.log(data);
			var ClientSelectHTML = "";
			for (var i=0; i< data.length; i++) {
				ClientSelectHTML += "<option value="+data[i].pkenterpriseid+">"+data[i].enterprisename+"</option>"
			}
			$("#clientname").html(ClientSelectHTML);
			if(currentValues.client > 0){
				$("#clientname").val(currentValues.client).change();
			}
		});

		$.getJSON("{{ URL::to('/user/new/UserTypeList')}}",function(data) {
			//console.log(data);
			var UserTypeSelectHTML = "";
			for (var i=0; i< data.length; i++) {
				UserTypeSelectHTML += "<option value="+data[i].pkusertypeid+">"+data[i].type+"</option>"
			}
			$("#UserType").html(UserTypeSelectHTML);
			if(currentValues.userType > 0){
				$("#UserType").val(currentValues.userType).change();
			}
		});
	}
	
}); /* document.ready */

</script>

@stop
