@extends('layouts.sidebar')
@section('content')

<div class="row">
    <div class="col-md-6 mb-3 offset-md-3">
		<form  action="{{ URL::to('/user/new')}}" method="post" id="form" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="form-group form-group-md">
            	<span class="text-danger">{{ $errors->first('Error') }}</span>
          	</div>
          	
			<div class="form-group mb-3">
				<label for="nameInput">Primeiro Nome:</label>
				<input type="text" class="form-control {{($errors->first('nameInput') ? " invalid" : "")}}" id="nameInput" name="nameInput" value="{{ old('nameInput') }}">
				<span class="text-danger">{{ $errors->first('nameInput') }}</span>
			</div>
			<div class="form-group mb-3">
				<label for="surnameInput">Último Nome:</label>
				<input type="text" class="form-control {{($errors->first('surnameInput') ? " invalid" : "")}}" id="surnameInput" name="surnameInput" value="{{ old('surnameInput') }}">
				<span class="text-danger">{{ $errors->first('surnameInput') }}</span>
			</div>
			<div class="form-group mb-3">
				<label for="emailInput">Email:</label>
				<input type="email" class="form-control {{($errors->first('emailInput') ? " invalid" : "")}}" id="emailInput" name="emailInput" value="{{ old('emailInput') }}">
				<span class="text-danger">{{ $errors->first('emailInput') }}</span>
			</div>

			<div class="form-group mb-3 mb-3">
				<label for="usernameInput">Username:</label>
				<input type="text" class="form-control {{($errors->first('usernameInput') ? " invalid" : "")}}" id="usernameInput" name="usernameInput" value="{{ old('usernameInput') }}">
				<span class="text-danger">{{ $errors->first('usernameInput') }}</span>
			</div>
			<div class="form-group mb-3">
				<label for="passwordInput">Password:</label>
				<input type="password" class="form-control {{($errors->first('passwordInput') ? " invalid" : "")}}" id="passwordInput" name="passwordInput" value="{{ old('passwordInput') }}">
				<span class="text-danger">{{ $errors->first('passwordInput') }}</span>
			</div>

			<div class="form-group mb-3">
				<label for="UserTypeSelect">Tipo de Utilizador:</label>
				<select class="form-control" id="UserTypeSelect" name="UserTypeSelect">
				</select>
				<span class="text-danger">{{ $errors->first('UserTypeSelect') }}</span>
			</div>

			<div class="form-group mb-3">
				<label for="ClientSelect">Cliente Associado:</label>
				<select class="form-control" id="ClientSelect" name="ClientSelect">
				</select>
				<span class="text-danger">{{ $errors->first('ClientSelect') }}</span>
			</div>

			<div class="form-group mb-3">
				<label for="photoInput">Foto:</label>
				<br>
				<button type="button" id="photoInputButton" class="btn btn-sm "> Escolher Ficheiro</button>
				<input type="file" class="d-none" id="photoInput" name="photoInput" style="color:transparent; width: 100%;">
				<span id="photoInputText"></span>
				<span class="text-danger">{{ $errors->first('photoInput') }}</span>
			</div>
			<br>
			<div class="form-group mb-3">
				<div class="col text-right p-0">
					<input type="submit" class="btn btn-success btn-submit " name="btn" value="Registar" >
					<a href="{{ URL::to('/user')}}" class="btn btn-danger">Cancelar</a>
				</div>
			</div>
			

		</form>
		
	</div>
</div><!-- end of row -->


<script type="text/javascript">
	$(document).ready(function() {	

		$("#photoInputButton").click(function(event) {
			$('#photoInput').click();
		});

		$(document).on('change','#photoInput' , function(event){
			var nomeFicheiro = $(this).val().split("\\");
			$("#photoInputText").html(nomeFicheiro.pop());
		});

		$.getJSON("{{ URL::to('/user/new/clienteList')}}",function(data) {
			//console.log(data);
			var ClientSelectHTML = "";
			for (var i=0; i< data.length; i++) {
				ClientSelectHTML += "<option value="+data[i].pkenterpriseid+">"+data[i].enterprisename+"</option>"
			}
			$("#ClientSelect").html(ClientSelectHTML);
			var oldClient = "{{old('ClientSelect')}}";
			if(oldClient > 0){
				$("#ClientSelect").val(oldClient).change();
			}
		});

		$.getJSON("{{ URL::to('/user/new/UserTypeList')}}",function(data) {
			//console.log(data);
			var UserTypeSelectHTML = "";
			for (var i=0; i< data.length; i++) {
				UserTypeSelectHTML += "<option value="+data[i].pkusertypeid+">"+data[i].type+"</option>"
			}
			$("#UserTypeSelect").html(UserTypeSelectHTML);
			var oldUserType = "{{old('UserTypeSelect')}}";
			if(oldUserType > 0){
				$("#UserTypeSelect").val(oldUserType).change();
			}
		});

	}); /* document.ready */
	
</script>

@stop
