@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>

<style type="text/css">
/*
	table{border-collapse: collapse;}
	td,th{border:1px solid black;}
*/
table.dataTable td {
    padding: 10px;
}
</style>

<h3>{{$serialnumber}} - Debug</h3>


<table id="collarTable" class="display" cellspacing="0" width="100%">
	<thead>
		<th>id</th>
		<th>b_beaconid</th>
		<th>b_b2bseqnumb</th>
		<th>b_bsseqnumb</th>
		<th>b_gateway</th>
		<th>b_latitude</th>
		<th>b_longitude</th>
		<th>b_neighborid's</th>
		<th>b_neighborrssi's</th>
		<th>b_propertyid</th>
		<th>b_timestamp</th>
	</thead>
	<tbody>

	</tbody>
</table>


<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		createDatatableScroll("#collarTable","{{ URL::to('/beacon/view/'.$serialnumber.'/debug/table')}}",false,null,500,true,[],[0, "asc"]);
	}); /* document.ready */
	    
</script>

@stop
