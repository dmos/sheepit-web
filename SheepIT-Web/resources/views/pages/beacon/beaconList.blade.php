@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>

<style>
	#beaconTableScroll_wrapper{padding: 0px;}
</style>


<a href="{{ URL::to('/beacon/new')}}"><button class="btn btn-primary">Novo Farol <i class="fa fa-plus"></i></button></a>

<table id="beaconTableScroll" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Numero de série</th>
			<th>Versão</th>
			<th>Bateria</th>
			<th>tempo activo</th>
			<th></th>
		</tr>
	</thead>

	<tbody>

	</tbody>
</table>



<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//datatable 

		/**
		 * $beaconSearch contains String that is passed as a parameter to be searched on the List
		 * @type {String}
		 */
		var beaconSearch =  '{{ $beaconSearch }}' ;

		
		//createDatatable("#collarTable","/collarList",collarSearch);
		//
		//

		createDatatableScroll("#beaconTableScroll","{{ URL::to('/beaconList')}}",true,beaconSearch,400,true,[-1],[0, "asc"]);


	} ); /* document.ready */
	    
</script>

@stop
