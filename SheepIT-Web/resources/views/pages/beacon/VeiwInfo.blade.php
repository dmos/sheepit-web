@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>


<style type="text/css">

#HistoryTable_wrapper{padding: 0px;}
#HistoryAlertsTable_wrapper{padding: 0px;}

</style>

<?php 
	//var_dump($result);exit;
?>

<div class="row card-deck">
	<div class="col-md-12 offset-md-0 mb-3">
		<div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="row m-0">
				<div class="col-6 p-0 pt-1 text-toggle">Informações <i class="fa fa-caret-up"></i></div>
				<div class="col-6 pr-0 pl-0">
					<button id="editing" class="btn btn-sm  mr-1"><span>Editar</span> <i class="fa fa-edit"></i></button>
					<a href="{{ URL::to('/beacon/view/'.$result[0]->serialnumber.'/debug')}}" class="btn btn-primary btn-sm  mr-1">Informação adicional <i class="fa fa-info-circle"></i></a>
				</div>
				</div>
			</div>
			<div class="card-body card-toggle" id="infos">
				<form method="POST" id="form" action="{{ URL::to('/beacon/view/'.$result[0]->serialnumber.'/update')}}">
					{{ csrf_field() }}
					<span class="text-danger" id="generalError"></span>

					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Número de série:</lable>
						<div class="col-sm-6 p0">
							<span class="bold span">{{$result[0]->serialnumber}}</span>
							<input type="text" id="serialnumber" name="serialnumber" class="editable editable-input d-none bold" value="{{$result[0]->serialnumber}}" disabled="disabled"/>
							<span class="text-danger error" id="serialnumbererror"></span>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Versão:</lable>
						<div class="col-sm-6 p0">
							<span class="bold span">{{$result[0]->version}}</span>
							<input type="text" id="version" name="version" class="editable editable-input d-none bold" value="{{$result[0]->version}}" disabled="disabled"/>
							<span class="text-danger error" id="versionerror"></span>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Tempo activo: </lable>
						<div class="col-sm-6 p0">
							<span class="bold col-sm-6 p0">{{$result[0]->uptime}}</span>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Bateria: </lable>
						<div class="col-sm-6 p0">
							<span class="bold">{{$result[0]->battery}}%</span>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Cliente: </lable>
						<div class="col-sm-6 p0">
							<a class="col-sm-6 p0 bold span" href="{{URL::to('/client/view/'.$result[0]->pkenterpriseid)}}" id="clientnameSpan">
							{{$result[0]->clientname}}
							</a>
							<select class="editable editable-input d-none bold" id='clientname' name='clientname'>
							</select>
							<span class="text-danger error" id="clientnameerror"></span>
						</div>
					</div>
					<div class="form-group row mb-1">
						<lable class="col-sm-6 pl1 control-label">Propriedade: </lable>
						<div class="col-sm-6 p0">
							<a class="col-sm-6 p0 bold span" href="{{URL::to('/property/view/'.$result[0]->pkpropertyid)}}" id="propertydescriptionSpan">
							{{$result[0]->propertydescription}}
							</a>
							<select class="editable editable-input d-none bold" id='propertydescription' name='propertydescription'>
							</select>
							<span class="text-danger error" id="propertydescriptionerror"></span>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div> <!-- end of row -->


<div class="row card-deck">

	<div class="col-md-12 mb-3">
		<div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="col-12 p-0 pt-1 text-toggle">Percentagem da bateria <i class="fa fa-caret-down"></i></div>
			</div>
			<div class="card-body card-toggle d-none">
				<h6 class="mt0">
					Dia Inical: <input type="date" name="dayStart" id="dayStart">
					&nbsp;
					Dia Final: <input type="date" name="dayEnd" id="dayEnd">
				</h6>
				<canvas id="line"></canvas>
			</div>
		</div>
	</div>

</div><!-- end of row -->


<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript" src="{{ URL::to('/js/graphMaking.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//createDatatableScroll("#HistoryTable","{/{ URL::to('/collar/view/'.$result[0]->pkanimalid.'/datatable')}}",false,null,120,true,[],[0, "asc"]);

	//lineChart

	// TODO :  dá error 404 se não tiver animal associado, mas não ha problema! resolvido?
	//lineChart("{//{ URL::to('/collar/view/'.$result[0]->pkanimalid.'/lineChart')}}","line");
	
	var dia = "{{$lastDay}}";
	dia = dia.split(" ");
	$("#dayStart").val(dia[0]);
	$("#dayEnd").val(dia[0]);

	lineChartTime("{{ URL::to('/beacon/view/'.$result[0]->serialnumber.'/lineChart')}}/"+$("#dayStart").val()+"/"+$("#dayEnd").val(),"line");

	$("#dayStart").change(function(event) {
		lineChartTime("{{ URL::to('/beacon/view/'.$result[0]->serialnumber.'/lineChart')}}/"+$("#dayStart").val()+"/"+$("#dayEnd").val(),"line");
	});

	$("#dayEnd").change(function(event) {
		lineChartTime("{{ URL::to('/beacon/view/'.$result[0]->serialnumber.'/lineChart')}}/"+$("#dayStart").val()+"/"+$("#dayEnd").val(),"line");
	});


	//edieting
	$("#editing").click(function(event) {
		if($(this).children('span').html() == "Editar"){
			getSelectsInfos();
			$(".editable").prop("disabled",false);
			$(".editable").toggleClass('editable-input');			
			$(".editable").removeClass('d-none');
			$(".span").addClass('d-none');
			$(".hidden").removeClass('d-none');
			
			$(this).children('span').html("Concluir");
		}
		else{
			
			$.ajax({
				type: "POST",
				url: '{{ URL::to('/beacon/view/'.$result[0]->serialnumber.'/update')}}',
				data: $("#form").serialize(),
			})
			.done(function(result) {
				console.log(result);
				$(".text-danger").empty();
				$("#generalError").empty();
				$(".editable").removeClass('invalid');

				if(result.status == "sucesso"){
					//VER! AO ATUALIZAR o redirect
					location.replace("{{ URL::to('/beacon/view')}}/"+result.serialnumber);
				}
				if(result.status == "validation"){
					$.each(result.error , function( key, value ) {
						$("#"+key+"error").html("<br>"+value);
						$("#"+key).addClass('invalid');
					});
				}

				if(result.status == "error"){
					$("#generalError").html(result.error+"<br>");
				}
			});			
			//$("#form").submit();
		}
	});

	function getSelectsInfos(){
		var currentValues = {
			client:"{{$result[0]->pkenterpriseid}}",
			property:"{{$result[0]->pkpropertyid}}"}

		$.getJSON("{{ URL::to('/beacon/new/clienteList')}}",function(data) {
			//console.log(data);
			var ClientSelectHTML = "<option value='0'></option>";
			for (var i=0; i< data.length; i++) {
				ClientSelectHTML += "<option value="+data[i].pkenterpriseid+">"+data[i].enterprisename+"</option>"
			}
			$("#clientname").html(ClientSelectHTML);
			if(currentValues.client > 0){
				$("#clientname").val(currentValues.client).change();
			}
		});

		$("#clientname").change(function(){

			if($(this).val() < 1 || $(this).html() == ""){
				$("#propertydescription").prop('disabled', true);
				$("#propertydescription").empty();
				return;
			}
			$("#propertydescription").prop('disabled', false);
			
			$.getJSON("{{ URL::to('/beacon/new/propertyList')}}/"+$(this).val(),function(data) {
				
				var PropertySelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					PropertySelectHTML += "<option value="+data[i].pkpropertyid+">"+data[i].description+"</option>"
				}
				$("#propertydescription").html(PropertySelectHTML);
				if(currentValues.property > 0){
					$("#propertydescription").val(currentValues.property).change();
				}
				
			});
			
		});/* end of ClientSelect.change */
	}
	
}); /* document.ready */

</script>

@stop
