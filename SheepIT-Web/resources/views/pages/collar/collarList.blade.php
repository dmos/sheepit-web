@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>

<style>
	#collarTableScroll_wrapper{padding: 0px;}
</style>

<a href="{{ URL::to('/collar/new')}}"><button class="btn btn-primary">Nova coleira <i class="fa fa-plus"></i></button></a>

<table id="collarTableScroll" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Numero de série</th>
			<th>Versão</th>
			<th>Bateria</th>
			<th>tempo activo</th>
			<th>ID de Rede</th>
			<th>Animal Associado</th>
			<th>Alertas</th>
			<th></th>
		</tr>
	</thead>

	<tbody>

	</tbody>
</table>



<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {

		
		//datatable 

		/**
		 * $collarSearch contains String that is passed as a parameter to be searched on the List
		 * @type {String}
		 */
		var collarSearch =  '{{ $collarSearch }}' ;

		
		//createDatatable("#collarTable","/collarList",collarSearch);
		//
		//

		createDatatableScroll("#collarTableScroll","{{ URL::to('/collarList')}}",true,collarSearch,400,true,[-1],[0, "asc"]);


	} ); /* document.ready */
	    
</script>

@stop
