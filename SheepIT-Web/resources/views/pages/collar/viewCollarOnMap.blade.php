@extends('layouts.sidebar')
@section('content')

@include('includes.leaflet')

<div class="row">
	<div class="col mb-3">
		<div class="card text-black bg-card">
			<div class="card-header">
				Mapa
			</div>
			<div class="card-body" id="infoMapa">
				<div id="mapid" style="height: 450px; width: 100%; margin: auto;"></div>

				<div class="row">
				    <div class="col-sm-12 pt-2">
				    	Legenda:
				    	<br>
			      		<span class="badge" style="background-color:#3388ff">Área da propriedade</span>
			        	<span class="badge" style="background-color:red">Farois</span>     
				      
				    </div>
			 	</div>
			</div>
        </div>
	</div>
</div><!-- end of row -->


<script type="text/javascript">

	var beaconArray = [];
	var collarMapMarker;

	$(document).ready(function() {
		mymap = L.map('mapid').setView([0,0],18);
			L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
				attribution: '',
		maxZoom: 19,
		minZoom: 5,
		noWrap: true
		}).addTo(mymap);

		/**
		* draw layer
		*/

		var editableLayers = new L.FeatureGroup();
		mymap.addLayer(editableLayers);
		
		/*
		var MyCustomMarker = L.Icon.extend({
			options: {
				shadowUrl: null,
				iconAnchor: new L.Point(12, 12),
				iconSize: new L.Point(24, 24),
				iconUrl: 'js/leaflet/images/marker-icon.png'//bug
			}
		});
		*/

		
		var options = {
			position: 'topright',
			draw: {
				polyline: false,
				polygon: false,
				circle: false, // Turns off this drawing tool
				rectangle: false,
				marker: false,

				circlemarker : false,
			},
			edit: false
		};
		
		var drawControl = new L.Control.Draw(options);
		mymap.addControl(drawControl);
		
		mymap.on(L.Draw.Event.CREATED, function (e) {
			var type = e.layerType,
				layer = e.layer;
		
			if (type === 'marker') {
				layer.bindPopup('A popup!');
			}
		
			editableLayers.addLayer(layer);
			//logChanges(); // log on create
			
		});

		mymap.on('draw:edited', function (e) {
			var layers = e.layers;
			layers.eachLayer(function (layer) {
			//do whatever you want; most likely save back to db
			//logChanges(); // log on edit
			});
		});

		/**
		* end of draw layer
		*/
			
		$.getJSON("{{ URL::to('/collar/view/'.$collarSerial.'/map/get')}}", function(json) {
			//console.log(JSON.stringify(json));
        	console.log(json);    


			function onEachFeature(feature, layer) {
			// does this feature have a property named popupContent?
				if (feature.properties && feature.properties.popupContent) {
					layer.bindPopup(feature.properties.popupContent);
				}
			}
			var addedJson = L.geoJson(json,{
				onEachFeature: onEachFeature
			});

			addedJson.eachLayer(
				function(l){
					
					if(l.feature.properties.radius){
						//console.log(l.feature.properties.radius);
						var circle = new L.Circle(l.feature.geometry.coordinates.reverse(), l.feature.properties.radius,{
							color: "red",
							fillColor: "red",
                			fillOpacity: 0.5
						}).bindPopup(l.feature.properties.popupContent);
						editableLayers.addLayer(circle);
						beaconArray[l.feature.properties.beaconID] = circle;
					}
					else{
						if(l.feature.geometry.type == "Polygon"){
							editableLayers.addLayer(l)
						}
						else{
							var marker2 = new L.Icon({
								// iconSize: [27, 27],
								// iconAnchor: [13, 27],
								// popupAnchor:  [1, -24],

								iconSize: [30, 40],
								iconAnchor: [15, 40],
								popupAnchor:  [1, -24],
								iconUrl: "{{URL::to('/vendor/leaflet/images/marker2.png')}}"
							});

							var c = L.marker(l.feature.geometry.coordinates.reverse(), {icon: marker2})
							.bindPopup(l.feature.properties.popupContent);
							editableLayers.addLayer(c);
							collarMapMarker = c;
						}				
					}
					
			});
			
			//fit map to all points
			mymap.fitBounds(addedJson.getBounds(),{
				padding: [20,20]
			});

    	}); /**end of $.get */



    	setInterval(function(){
    	 	$.getJSON("{{ URL::to('/collar/view/'.$collarSerial.'/map/update')}}", function(json){
    	 		//console.log(json);
    	 		for(i=0;i<json.length;i++){ 			
    	 			if(json[i].type == 'beacon'){
    	 				beaconArray[json[i].id].setLatLng([json[i].lat,json[i].lng]);
    	 				beaconArray[json[i].id]._popup.setContent(json[i].popup);
    	 				continue;
    	 			}
    	 			collarMapMarker.setLatLng([json[i].lat,json[i].lng]);
    	 			collarMapMarker._popup.setContent(json[i].popup);
    	 		}

    		});
    	}, 3000);


	} ); /* document.ready */
	    
</script>

@stop
