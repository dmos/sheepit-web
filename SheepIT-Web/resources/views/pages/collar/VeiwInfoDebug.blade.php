@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>

<style type="text/css">
/*
	table{border-collapse: collapse;}
	td,th{border:1px solid black;}
*/
table.dataTable td {
    padding: 10px;
}
</style>

<h3>{{$collarSerial}} - Debug</h3>


<table id="collarTable" class="display" cellspacing="0" width="100%">
	<thead>
		<th>id</th>
		<th>c_collarid</th>
		<th>c_timestamp</th>
		<th>c_battery</th>
		<th>c_anglesdec (x, y, z)</th>
		<th>c_accelerometer (x, y, z)</th>
		<th>c_d_accelerometer (x, y, z)</th>
		<th>c_magnetometer (x, y, z)</th>
		<th>c_rcv_rssi</th>
		<th>c_rcv_rssi_id's</th>
		<th>c_ultrasoundsdistance</th>
		<th>c_steps</th>
		<th>c_postureinfraction</th>
		<th>c_fenceinfraction</th>
		<th>c_propertyid</th>
		<th>c_reportingbeacon</th>

	</thead>
	<tbody>

	</tbody>
</table>


<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		createDatatableScroll("#collarTable","{{ URL::to('/collar/view/'.$collarSerial.'/debug/table')}}",false,null,500,true,[],[0, "asc"]);
	}); /* document.ready */
	    
</script>

@stop
