@extends('layouts.sidebar')
@section('content')


<div class="row">
    <div class="col-md-6 mb-3 offset-md-3">
		<form  action="{{ URL::to('/collar/new')}}" method="post" id="form">
			{{ csrf_field() }}
			<div class="form-group form-group-md">
            	<span class="text-danger">{{ $errors->first('Error') }}</span>
          	</div>
          	
			<div class="form-group mb-3">
				<label for="serialNumberInput">Numero de série:</label>
				<input type="text" class="form-control {{($errors->first('serialNumberInput') ? " invalid" : "")}}" id="serialNumberInput" name="serialNumberInput" placeholder="ex: CX123A4" value="{{ old('serialNumberInput') }}">
				<span class="text-danger error">{{ $errors->first('serialNumberInput') }}</span>
			</div>
			<div class="form-group mb-3">
				<label for="VersionInput">Versão:</label>
				<input type="text" class="form-control {{($errors->first('VersionInput') ? " invalid" : "")}}" id="VersionInput" name="VersionInput" placeholder="ex: 1.1" value="{{ old('VersionInput') }}" pattern="[0-9.]{3,}" title="Introduza a versão como por exemplo: 1.1">
				<span class="text-danger error">{{ $errors->first('VersionInput') }}</span>
			</div>
			<div class="form-group mb-3">
				<label for="ClientSelect">Cliente Associado:</label>
				<select class="form-control" id="ClientSelect" name="ClientSelect">
				</select>
			</div>
			<div class="form-group mb-3">
				<label for="PropertySelect">Propriedade:</label>
				<select class="form-control" id="PropertySelect" name="PropertySelect" disabled="disabled">
				</select>
			</div>
			<div class="form-group mb-3">
				<label for="AnimalSelect">Animal Associado:</label>
				<select class="form-control" id="AnimalSelect" name="AnimalSelect" disabled="disabled">
				</select>
			</div>
			<br>
			
			<div class="form-group mb-3">
				<div class="col text-right p-0">
					<input type="submit" class="btn btn-success btn-submit " name="btn" value="Registar" >
					<a href="{{ URL::to('/collar')}}" class="btn btn-danger">Cancelar</a>
				</div>
			</div>
			

		</form>
		
	</div>
</div><!-- end of row -->


<script type="text/javascript">
	$(document).ready(function() {	
		$.getJSON("{{ URL::to('/collar/new/clienteList')}}",function(data) {
			//console.log(data);
			var ClientSelectHTML = "<option value='0'></option>";
			for (var i=0; i< data.length; i++) {
				ClientSelectHTML += "<option value="+data[i].pkenterpriseid+">"+data[i].enterprisename+"</option>"
			}
			$("#ClientSelect").html(ClientSelectHTML);
			var oldClient = "{{old('ClientSelect')}}";
			if(oldClient > 0){
				$("#ClientSelect").val(oldClient).change();
			}
		});

		$("#ClientSelect").change(function(){

			if($(this).val() < 1 || $(this).html() == ""){
				$("#PropertySelect").prop('disabled', true);
				$("#PropertySelect").empty();
				$("#AnimalSelect").prop('disabled', true);
				$("#AnimalSelect").empty();
				return;
			}
			$("#PropertySelect").prop('disabled', false);
			
			$.getJSON("{{ URL::to('/collar/new/propertyList')}}/"+$(this).val(),function(data) {
				
				var PropertySelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					PropertySelectHTML += "<option value="+data[i].pkpropertyid+">"+data[i].description+"</option>"
				}
				$("#PropertySelect").html(PropertySelectHTML);
				var oldProperty = "{{old('PropertySelect')}}";
				if(oldProperty > 0){
					$("#PropertySelect").val(oldProperty).change();
				}
				
			});
			
		});/* end of ClientSelect.change */

		$("#PropertySelect").change(function(){

			if($(this).val() < 1 || $(this).html() == ""){
				$("#AnimalSelect").prop('disabled', true);
				$("#AnimalSelect").empty();
				return;
			}
			$("#AnimalSelect").prop('disabled', false);
			
			$.getJSON("{{ URL::to('/collar/new/animalList')}}/"+$(this).val(),function(data) {
				var AnimalSelectHTML = "<option value='0'></option>";
				for (var i=0; i< data.length; i++) {
					AnimalSelectHTML += "<option value="+data[i].pkanimalid+">"+data[i].animalidtag+"</option>"
				}
				$("#AnimalSelect").html(AnimalSelectHTML);
				var oldAnimal = "{{old('AnimalSelect')}}";
				if(oldAnimal > 0){
					$("#AnimalSelect").val(oldAnimal).change();
				}
				
			});
			
		}); /* end of PropertySelect.change */

	}); /* document.ready */
	
</script>

@stop
