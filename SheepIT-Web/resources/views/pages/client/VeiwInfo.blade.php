@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>


<style type="text/css">

#userTableScroll_wrapper{padding: 0px;}

#propertyTableScroll_wrapper{padding: 0px;}


.editableAddress{
	width: 100%;
}

</style>

<!-- General Info -->
<div class="row card-deck">
	<div class="col-md-12 offset-md-0 mb-3">
		<div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="row m-0">
				<div class="col-6 p-0 pt-1 text-toggle">Informações <i class="fa fa-caret-up"></i></div>
				<div class="col-6 pr-0 pl-0 text-right">
					<button id="editing" class="btn btn-sm  mr-1"><span>Editar</span> <i class="fa fa-edit"></i></button>
				</div>
				</div>
			</div>
			<div class="card-body card-toggle" id="infos">
				
				<form method="POST" id="form" action="{{ URL::to('/client/view/'.$result[0]->pkenterpriseid.'/update')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<span class="text-danger" id="generalError"></span>

					<div class="row">
					<div class="col-md-10 mb-1">
						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Nome do cliente:</lable>
							<div class="col-sm-6 p0">
								<span class="bold span">{{$result[0]->enterprisename}}</span>
								<input type="text" id="enterprisenameInput" name="enterprisenameInput" class="editable editable-input d-none bold" value="{{$result[0]->enterprisename}}" disabled="disabled"/>
								<span class="text-danger error" id="enterprisenameInputerror"></span>
							</div>
						</div>
						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Nome do responsável:</lable>
							<div class="col-sm-6 p0">
								<span class="bold span">{{$result[0]->nameresponsible . " " .$result[0]->surnameresponsible}}</span>
								<input type="text" id="nameresponsibleInput" name="nameresponsibleInput" class="editable editable-input d-none bold" value="{{$result[0]->nameresponsible}}" disabled="disabled"/>
								<span class="text-danger error" id="nameresponsibleInputerror"></span>
								<input type="text" id="surnameresponsibleInput" name="surnameresponsibleInput" class="editable editable-input d-none bold" value="{{$result[0]->surnameresponsible}}" disabled="disabled"/>
								<span class="text-danger error" id="surnameresponsibleInputerror"></span>
							</div>
						</div>
						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Email:</lable>
							<div class="col-sm-6 p0">
								<span class="bold span">{{$result[0]->email}}</span>
								<input type="email" id="emailInput" name="emailInput" class="editable editable-input d-none bold" value="{{$result[0]->email}}" disabled="disabled"/>
								<span class="text-danger error" id="emailInputerror"></span>
							</div>
						</div>
						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Nºtelefone:</lable>
							<div class="col-sm-6 p0">
								<span class="bold span">{{$result[0]->cellphone}}</span>
								<input type="text" id="cellphoneInput" name="cellphoneInput" class="editable editable-input d-none bold" value="{{$result[0]->cellphone}}" disabled="disabled" title="Introduza o número de telefone como por exemplo: 222333444"/>
								<span class="text-danger error" id="cellphoneInputerror"></span>
							</div>
						</div>
						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Nºfiscal:</lable>
							<div class="col-sm-6 p0">
								<span class="bold span">{{$result[0]->taxnumber}}</span>
								<input type="text" id="taxnumberInput" name="taxnumberInput" class="editable editable-input d-none bold" value="{{$result[0]->taxnumber}}" disabled="disabled"  title="Introduza o número fiscal como por exemplo: 222333444"/>
								<span class="text-danger error" id="taxnumberInputerror"></span>
							</div>
						</div>
						<div class="form-group row mb-1">
							<lable class="col-sm-6 pl1 control-label">Tipo de cliente: </lable>
							<div class="col-sm-6 p0">
								<a class="col-sm-6 p0 bold span" href="#" id="UserTypeSpan">
								{{$result[0]->type}}
								</a>
								<select class="editable editable-input d-none bold" id='clientType' name='clientType'>
								</select>
								<span class="text-danger error" id="clientTypeerror"></span>
							</div>
						</div>
					</div>

					<div class="col-md-2 text-center">
						<p>	
						@if(file_exists(public_path('img/enterprise/'.$result[0]->pkenterpriseid.'.jpg'))) 
						<img class="userimg img-fluid img-responsive hidden-xs" src="{{ url('/img/enterprise/'.$result[0]->pkenterpriseid.'.jpg') }}">
						@else
							<img class="userimg img-fluid img-responsive hidden-xs" src="{{ url('/img/enterprise/default.jpg') }}">
						@endif
						</p>

						<label class="control-label editable d-none">Alterar Foto: </label>
						<br>
						<button type="button" id="photoInputButton" class="btn btn-sm editable d-none"> Escolher Ficheiro</button>
						<input type="file" class="d-none" id="photoInput" name="photoInput" style="color:transparent; width: 100%;">
						<span id="photoInputText"></span>
						
						<span class="text-danger error" id="photoInputerror"></span>
					</div>

					</div> <!-- end of row -->
				</form>
			</div>
		</div>
	</div>

</div> <!-- end of row -->

<!-- Location info -->
<div class="row card-deck">
	<div class="col-md-12 offset-md-0 mb-3">
		<div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="row m-0">
				<div class="col-4 p-0 pt-1 text-toggle">Localização  <i class="fa fa-caret-up"></i></div>
				<div class="col-8 pr-0 pl-0">
					<button id="editingAddress" class="btn btn-sm float-right mr-1"><span>Editar</span> <i class="fa fa-edit"></i></button>
				</div>
				</div>
			</div>
			<div class="card-body card-toggle" id="infoLocalizacao">
				<form method="POST" id="formAddress" action="{{ URL::to('/client/view/'.$result[0]->pkenterpriseid.'/updateAddress')}}">
					{{ csrf_field() }}

				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Pais: </lable>
					<div class="col-sm-6 p0">
						<span class="bold spanAddress">{{$result[0]->country}}</span>
						<input type="text" id="countryInput" name="countryInput" class="editableAddress editable-input d-none bold" value="{{$result[0]->country}}" disabled="disabled"/>
						<span class="text-danger error" id="countryInputerror"></span>
					</div>
				</div>
				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Distrito: </lable>
					<div class="col-sm-6 p0">
						<span class="bold spanAddress">{{$result[0]->district}}</span>
						<input type="text" id="districtInput" name="districtInput" class="editableAddress editable-input d-none bold" value="{{$result[0]->district}}" disabled="disabled"/>
						<span class="text-danger error" id="districtInputerror"></span>
					</div>
				</div>
				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Localização: </lable>
					<div class="col-sm-6 p0">
						<span class="bold spanAddress">{{$result[0]->locality}}</span>
						<input type="text" id="localityInput" name="localityInput" class="editableAddress editable-input d-none bold" value="{{$result[0]->locality}}" disabled="disabled"/>
						<span class="text-danger error" id="localityInputerror"></span>
					</div>
				</div>
				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Rua: </lable>
					<div class="col-sm-6 p0">
						<span class="bold spanAddress">{{$result[0]->street}}</span>
						<input type="text" id="streetInput" name="streetInput" class="editableAddress editable-input d-none bold" value="{{$result[0]->street}}" disabled="disabled"/>
						<span class="text-danger error" id="streetInputerror"></span>
					</div>
				</div>
				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Codigo Postal: </lable>
					<div class="col-sm-6 p0">
						<span class="bold spanAddress">{{$result[0]->zipcode}}</span>
						<input type="text" id="zipcodeInput" name="zipcodeInput" class="editableAddress editable-input d-none bold" value="{{$result[0]->zipcode}}" disabled="disabled" pattern="[0-9-]{8,}"  title="Introduza o código postal como por exemplo: 1234-567"/>
						<span class="text-danger error" id="zipcodeInputerror"></span>
					</div>
				</div>
				</form>
			</div>
        </div>
	</div>
</div> <!-- end of row -->


<!-- List of users -->
<div class="row card-deck">
	<div class="col-md-12 offset-md-0 mb-3">
		<div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="row m-0">
					<div class="col-6 p-0 pt-1 text-toggle listuser">Lista de Utilizadores <i class="fa fa-caret-down"></i></div>
					<div class="col-6 pr-0 pl-0 text-right">
						<a href="{{ URL::to('/user/new')}}" class="btn btn-primary btn-sm">Novo Utilizador <i class="fa fa-plus"></i></a>
					</div>
				</div>
			</div>
			<div class="card-body card-toggle d-none">
				<table id="userTableScroll" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Username</th>
							<th>Nome</th>
							<th>Email</th>
							<th></th>
						</tr>
					</thead>

					<tbody>

					</tbody>
				</table>
			</div>
		</div>
	</div>

</div> <!-- end of row -->

<!-- List of Properties -->
<div class="row card-deck">
	<div class="col-md-12 offset-md-0 mb-3">
		<div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="row m-0">
					<div class="col-6 p-0 pt-1 text-toggle listproperty">Lista de Propriedades <i class="fa fa-caret-down"></i></div>
					<div class="col-6 pr-0 pl-0 text-right">
						<a href="{{ URL::to('/property/new')}}" class="btn btn-primary btn-sm">Nova Propriedade <i class="fa fa-plus"></i></a>
					</div>
				</div>
			</div>
			<div class="card-body card-toggle d-none">
				<table id="propertyTableScroll" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Descrição</th>
							<th>Tipo</th>
							<th>Morada</th>
							<th></th>
						</tr>
					</thead>

					<tbody>

					</tbody>
				</table>
			</div>
		</div>
	</div>

</div> <!-- end of row -->
<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript" src="{{ URL::to('/js/graphMaking.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	//datatables
	//in this spcifi case, the content is onlu loaded with the click function, otherwise the header of the tables bugs
	
	$(".listuser").one('click',function(event) {
		createDatatableScroll("#userTableScroll","{{ URL::to('/client/view/'.$result[0]->pkenterpriseid.'/userList')}}",true,null,200,true,[-1],[0, "asc"]);
	});
	$(".listproperty").one('click',function(event) {
		createDatatableScroll("#propertyTableScroll","{{ URL::to('/client/view/'.$result[0]->pkenterpriseid.'/propertyList')}}",true,null,200,true,[-1],[0, "asc"]);
	});
	

	$("#photoInputButton").click(function(event) {
		$('#photoInput').click();
	});

	$(document).on('change','#photoInput' , function(event){
		var nomeFicheiro = $(this).val().split("\\");
		$("#photoInputText").html(nomeFicheiro.pop());
	});

	//edieting
	$("#editing").click(function(event) {
		if($(this).children('span').html() == "Editar"){
			getSelectsInfos();
			$(".editable").prop("disabled",false);
			$(".editable").toggleClass('editable-input');			
			$(".editable").removeClass('d-none');
			$(".span").addClass('d-none');
			$(".hidden").removeClass('d-none');
			
			$(this).children('span').html("Concluir");
		}
		else{
			var formData = new FormData($("#form")[0]);
			$.ajax({
				type: "POST",
				url: '{{ URL::to('/client/view/'.$result[0]->pkenterpriseid.'/update')}}',
				data: formData,
				processData: false,
				contentType: false,
			})
			.done(function(result) {
				$(".text-danger").empty();
				$("#generalError").empty();
				$(".editable").removeClass('invalid');

				if(result.status == "sucesso"){
					location.replace("{{ URL::to('/client/view')}}/"+result.clientID);
				}
				if(result.status == "validation"){
					$.each(result.error , function( key, value ) {
						$("#"+key+"error").html("<br>"+value);
						$("#"+key).addClass('invalid');
					});
				}

				if(result.status == "error"){
					$("#generalError").html(result.error+"<br>");
				}
			});			
			//$("#form").submit();
		}
	});

	function getSelectsInfos(){
		var currentValues = {clientType:"{{$result[0]->fkenterprisetypeid}}"}

		$.getJSON("{{ URL::to('/client/new/clientTypeList')}}",function(data) {
			//console.log(data);
			var clientTypeSelectHTML = "";
			for (var i=0; i< data.length; i++) {
				clientTypeSelectHTML += "<option value="+data[i].pkenterprisetypeid+">"+data[i].type+"</option>"
			}
			$("#clientType").html(clientTypeSelectHTML);
			if(currentValues.clientType > 0){
				$("#clientType").val(currentValues.clientType).change();
			}
		});
	}
	
	// editing address
	$("#editingAddress").click(function(event) {
		if($(this).children('span').html() == "Editar"){
			$(".editableAddress").prop("disabled",false);
			$(".editableAddress").toggleClass('editable-input');			
			$(".editableAddress").removeClass('d-none');
			$(".spanAddress").addClass('d-none');
			$(".hiddenAddress").removeClass('d-none');
			
			$(this).children('span').html("Concluir");
		}
		else{
			
			$.ajax({
				type: "POST",
				url: '{{ URL::to('/client/view/'.$result[0]->pkenterpriseid.'/updateAddress')}}',
				data: $("#formAddress").serialize(),
			})
			.done(function(result) {
				$(".text-danger").empty();
				$("#generalError").empty();
				$(".editableAddress").removeClass('invalid');

				if(result.status == "sucesso"){
					location.replace("{{ URL::to('/client/view')}}/"+result.clientID);
				}
				if(result.status == "validation"){
					$.each(result.error , function( key, value ) {
						$("#"+key+"error").html("<br>"+value);
						$("#"+key).addClass('invalid');
					});
				}

				if(result.status == "error"){
					$("#generalError").html(result.error+"<br>");
				}
			});			
		}
	});


}); /* document.ready */

</script>

@stop
