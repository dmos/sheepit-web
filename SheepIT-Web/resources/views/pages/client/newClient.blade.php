@extends('layouts.sidebar')
@section('content')

<div class="row">
    <div class="col-md-4 mb-3">
	<form  action="{{ URL::to('/client/new')}}" method="post" id="form" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="form-group form-group-md">
        	<span class="text-danger">{{ $errors->first('Error') }}</span>
      	</div>

      	<div class="form-group mb-3">
			<label for="enterprisenameInput">Nome do Cliente:</label>
			<input type="text" class="form-control {{($errors->first('enterprisenameInput') ? " invalid" : "")}}" id="enterprisenameInput" name="enterprisenameInput" value="{{ old('enterprisenameInput') }}">
			<span class="text-danger">{{ $errors->first('enterprisenameInput') }}</span>
		</div>

		<div class="form-group mb-3">
			<label for="nameresponsibleInput">Primeiro Nome do Responsável:</label>
			<input type="text" class="form-control {{($errors->first('nameresponsibleInput') ? " invalid" : "")}}" id="nameresponsibleInput" name="nameresponsibleInput" value="{{ old('nameresponsibleInput') }}">
			<span class="text-danger">{{ $errors->first('nameresponsibleInput') }}</span>
		</div>
		<div class="form-group mb-3">
			<label for="surnameresponsibleInput">Último Nome do Responsável:</label>
			<input type="text" class="form-control {{($errors->first('surnameresponsibleInput') ? " invalid" : "")}}" id="surnameresponsibleInput" name="surnameresponsibleInput" value="{{ old('surnameresponsibleInput') }}">
			<span class="text-danger">{{ $errors->first('surnameresponsibleInput') }}</span>
		</div>
	</div>

	<div class="col-md-4 mb-3">
		<div class="form-group mb-3">
			<label for="cellphoneInput">Nºtelefone:</label>
			<input type="text" class="form-control {{($errors->first('cellphoneInput') ? " invalid" : "")}}" id="cellphoneInput" name="cellphoneInput" value="{{ old('cellphoneInput') }}" pattern="[0-9]{9,12}"  title="Introduza o numero de telefone como por exemplo: 222333444">
			<span class="text-danger">{{ $errors->first('cellphoneInput') }}</span>
		</div>
		<div class="form-group mb-3">
			<label for="emailInput">Email:</label>
			<input type="email" class="form-control {{($errors->first('emailInput') ? " invalid" : "")}}" id="emailInput" name="emailInput" value="{{ old('emailInput') }}">
			<span class="text-danger">{{ $errors->first('emailInput') }}</span>
		</div>
		
		<div class="form-group mb-3">
			<label for="taxnumberInput">Nºfiscal:</label>
			<input type="text" class="form-control {{($errors->first('taxnumberInput') ? " invalid" : "")}}" id="taxnumberInput" name="taxnumberInput" value="{{ old('taxnumberInput') }}" pattern="[0-9]{9,12}"  title="Introduza o número fiscal como por exemplo: 222333444">
			<span class="text-danger">{{ $errors->first('taxnumberInput') }}</span>
		</div>


		<div class="form-group mb-3">
			<label for="ClientTypeSelect">Tipo de Cliente:</label>
			<select class="form-control" id="ClientTypeSelect" name="ClientTypeSelect">
			</select>
			<span class="text-danger">{{ $errors->first('ClientTypeSelect') }}</span>
		</div>

		<div class="form-group mb-3">
			<label for="photoInput">Foto:</label>
			<br>
			<button type="button" id="photoInputButton" class="btn btn-sm "> Escolher Ficheiro</button>
			<input type="file" class="d-none" id="photoInput" name="photoInput" style="color:transparent; width: 100%;">
			<span id="photoInputText"></span>
			<span class="text-danger">{{ $errors->first('photoInput') }}</span>
		</div>

	</div>
	
	<!-- address div -->
	<div class="col-md-4 mb-3">
		<div class="form-group mb-3 row">
			<div class="col-md-12">
				<label for="streetInput">Rua:</label>
				<input type="text" class="form-control {{($errors->first('streetInput') ? " invalid" : "")}}" id="streetInput" name="streetInput" value="{{ old('streetInput') }}">
				<span class="text-danger">{{ $errors->first('streetInput') }}</span>
			</div>
		</div>

		<div class="form-group mb-3 row">
			<div class="col-md-12">
				<label for="countryInput">País:</label>
				<input type="text" class="form-control {{($errors->first('countryInput') ? " invalid" : "")}}" id="countryInput" name="countryInput" value="{{ old('countryInput') }}">
				<span class="text-danger">{{ $errors->first('countryInput') }}</span>
			</div>
		</div>
		<div class="form-group mb-3 row">
			<div class="col-md-12">
				<label for="districtInput">Distrito:</label>
				<input type="text" class="form-control {{($errors->first('districtInput') ? " invalid" : "")}}" id="districtInput" name="districtInput" value="{{ old('districtInput') }}">
				<span class="text-danger">{{ $errors->first('districtInput') }}</span>
			</div>
		</div>
		<div class="form-group mb-3 row">
			<div class="col-md-12">
				<label for="localityInput">Localidade:</label>
				<input type="text" class="form-control {{($errors->first('localityInput') ? " invalid" : "")}}" id="localityInput" name="localityInput" value="{{ old('localityInput') }}">
				<span class="text-danger">{{ $errors->first('localityInput') }}</span>
			</div>
			<div class="col-md-12">
				<label for="zipcodeInput">Código Postal:</label>
				<input type="text" class="form-control {{($errors->first('zipcodeInput') ? " invalid" : "")}}" id="zipcodeInput" name="zipcodeInput" value="{{ old('zipcodeInput') }}" pattern="[0-9-]{8,}"  title="Introduza o código postal como por exemplo: 1234-567" placeholder="ex: 1234-567">
				<span class="text-danger">{{ $errors->first('zipcodeInput') }}</span>
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="form-group mb-3 text-right row">
			<div class="col-md-12">
				<input type="submit" class="btn btn-success btn-submit " name="btn" value="Registar" >
				<a href="{{ URL::to('/client')}}" class="btn btn-danger">Cancelar</a>
			</div>
		</div>
	</div>

	</form>	
	
</div><!-- end of row -->


<script type="text/javascript">
	$(document).ready(function() {	

		$("#photoInputButton").click(function(event) {
			$('#photoInput').click();
		});

		$(document).on('change','#photoInput' , function(event){
			var nomeFicheiro = $(this).val().split("\\");
			$("#photoInputText").html(nomeFicheiro.pop());
		});


		$.getJSON("{{ URL::to('/client/new/clientTypeList')}}",function(data) {
			//console.log(data);
			var clientTypeSelectHTML = "";
			for (var i=0; i< data.length; i++) {
				clientTypeSelectHTML += "<option value="+data[i].pkenterprisetypeid+">"+data[i].type+"</option>"
			}
			$("#ClientTypeSelect").html(clientTypeSelectHTML);
			var oldClientType = "{{old('ClientTypeSelect')}}";
			if(oldClientType > 0){
				$("#ClientTypeSelect").val(oldClientType).change();
			}
		});

	}); /* document.ready */
	
</script>

@stop
