@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>

<style>
	#userTableScroll_wrapper{padding: 0px;}
</style>

<a href="{{ URL::to('/client/new')}}"><button class="btn btn-primary">Novo Cliente <i class="fa fa-plus"></i></button></a>

<table id="userTableScroll" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Nome do cliente</th>
			<th>Nome do responsável</th>
			<th>Distrito</th>
			<th>Tipo de Cliente</th>
			<th></th>
		</tr>
	</thead>

	<tbody>

	</tbody>
</table>



<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {

		
		//datatable 

		/**
		 * $clientSearch contains String that is passed as a parameter to be searched on the List
		 * @type {String}
		 */
		var clientSearch =  '{{ $clientSearch }}' ;

		createDatatableScroll("#userTableScroll","{{ URL::to('/clientList')}}",true,clientSearch,400,true,[-1],[0, "asc"]);


	} ); /* document.ready */
	    
</script>

@stop
