@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>



@include('includes.leaflet')

<style type="text/css">
	span.bold{font-weight: bold;}	
</style>


<div class="row">
    	<div class="col-md-6 mb-3">
		<form  action="{{ URL::to('/property/new')}}" method="post" id="form">
			{{ csrf_field() }}
			<div class="form-group form-group-md">
            	<span class="text-danger">{{ $errors->first('Error') }}</span>
          	</div>
          	
			<div class="form-group mb-3 row">
				<div class="col-md-7">
					<label for="descriptionInput">Descrição:</label>
					<input type="text" class="form-control {{($errors->first('descriptionInput') ? " invalid" : "")}}" id="descriptionInput" name="descriptionInput" value="{{ old('descriptionInput') }}">
					<span class="text-danger">{{ $errors->first('descriptionInput') }}</span>
				</div>
				<div class="col-md-5">
					<label for="typeInput">tipo:</label>
					<input type="text" class="form-control {{($errors->first('typeInput') ? " invalid" : "")}}" id="typeInput" name="typeInput" value="{{ old('typeInput') }}" placeholder="ex: Vinhas">
					<span class="text-danger">{{ $errors->first('typeInput') }}</span>
				</div>
			</div>

			<div class="form-group mb-3 row">
				<div class="col-md-12">
					<label for="ClientSelect">Cliente Associado:</label>
					<select class="form-control" id="ClientSelect" name="ClientSelect">
					</select>
				</div>
			</div>

			<h4>Morada</h4>
			
			<div class="form-group mb-3 row">
				<div class="col-md-12">
					<label for="streetInput">Rua:</label>
					<input type="text" class="form-control {{($errors->first('streetInput') ? " invalid" : "")}}" id="streetInput" name="streetInput" value="{{ old('streetInput') }}">
					<span class="text-danger">{{ $errors->first('streetInput') }}</span>
				</div>
			</div>

			<div class="form-group mb-3 row">
				<div class="col-md-6">
					<label for="countryInput">País:</label>
					<input type="text" class="form-control {{($errors->first('countryInput') ? " invalid" : "")}}" id="countryInput" name="countryInput" value="{{ old('countryInput') }}">
					<span class="text-danger">{{ $errors->first('countryInput') }}</span>
				</div>
				<div class="col-md-6">
					<label for="districtInput">Distrito:</label>
					<input type="text" class="form-control {{($errors->first('districtInput') ? " invalid" : "")}}" id="districtInput" name="districtInput" value="{{ old('districtInput') }}">
					<span class="text-danger">{{ $errors->first('districtInput') }}</span>
				</div>
			</div>
			<div class="form-group mb-3 row">
				<div class="col-md-7">
					<label for="localityInput">Localidade:</label>
					<input type="text" class="form-control {{($errors->first('localityInput') ? " invalid" : "")}}" id="localityInput" name="localityInput" value="{{ old('localityInput') }}">
					<span class="text-danger">{{ $errors->first('localityInput') }}</span>
				</div>
				<div class="col-md-5">
					<label for="zipcodeInput">Código Postal:</label>
					<input type="text" class="form-control {{($errors->first('zipcodeInput') ? " invalid" : "")}}" id="zipcodeInput" name="zipcodeInput" value="{{ old('zipcodeInput') }}" pattern="[0-9-]{8,}"  title="Introduza o código postal como por exemplo: 1234-567" placeholder="ex: 1234-567">
					<span class="text-danger">{{ $errors->first('zipcodeInput') }}</span>
				</div>
			</div>
		</div>

		
		<div class="col-md-6 mb-3">
			<label>Delimitações da propriadede:
			<br><small>(Utilize o mapa para delimitar a área da propriedade)</small>
			</label>
			<div id="mapid" style="height: 350px; width: 100%; margin: auto;"></div>
			<input type="hidden" name="coordentsInput" id="coordentsInput" readonly value="{{ old('coordentsInput')}}">
			<span class="text-danger">{{ $errors->first('coordentsInput') }}</span>
		</div>
		

		<div class="col-md-6">
			<div class="form-group mb-3 text-right row">
				<div class="col-md-12">
					<input type="submit" class="btn btn-success btn-submit" name="btn" value="Registar" >
					<a href="{{ URL::to('/property')}}" class="btn btn-danger">Cancelar</a>
				</div>
			</div>
		</div>

	</form>
		
</div><!-- end of row -->




<script type="text/javascript">
	$(document).ready(function() {		
		$.getJSON("{{ URL::to('/animal/new/clienteList')}}",function(data) {
			//console.log(data);
			var ClientSelectHTML = "";
			for (var i=0; i< data.length; i++) {
				ClientSelectHTML += "<option value="+data[i].pkenterpriseid+">"+data[i].enterprisename+"</option>"
			}
			$("#ClientSelect").html(ClientSelectHTML);
			var oldClient = "{{old('ClientSelect')}}";
			if(oldClient > 0){
				$("#ClientSelect").val(oldClient).change();
			}
		});
	}); /* document.ready */
	    
</script>


<!-- MAP -->
<script>
	var mymap;

	var areaArray;
	var old = "{{ old('coordentsInput')}}";
	if(old != ""){
		areaArray= JSON.parse(old);
	}
	
	var drawControl;
	var drawControl2;
	var optionsDraw;
	var optionsDelete;
$(document).ready(function() {
	/**
	* base layer
	*/
	mymap = L.map('mapid').setView([40.195659093364654,-7.503662109375001],5);
			
		L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
			attribution: '',
			maxZoom: 19,
			minZoom: 3,
			noWrap: true
		}).addTo(mymap);

		/**
		* draw layer
		*/

		var editableLayers = new L.FeatureGroup();
		mymap.addLayer(editableLayers);

		
		optionsDraw = {
			position: 'topright',
			draw: {
				polyline: false,
				polygon: {
					allowIntersection: false, // Restricts shapes to simple polygons
					drawError: {
						color: 'red', // Color the shape will turn when intersects
						message: '<strong>Não pode fazer Isso!<strong>' // Message that will show when intersect
					},
				},
				circle: false, 
				rectangle: {
					shapeOptions: {
						clickable: false
					}
				},
				marker: false,

				circlemarker : false,
			},

			edit: false
		};

		optionsDelete = {
			position: 'topright',
			draw: {
				polyline: false,
				polygon: false,
				circle: false,
				rectangle:false,
				marker: false,

				circlemarker : false,
			},

			edit: {
				featureGroup: editableLayers, //REQUIRED!!
				remove: true
			}
		};
		
		drawControl = new L.Control.Draw(optionsDraw);
		drawControl2 = new L.Control.Draw(optionsDelete)

		mymap.addControl(drawControl);
		//console.log(L);


		mymap.on(L.Draw.Event.CREATED, function (e) {
			var layer = e.layer;
			editableLayers.addLayer(layer);

			areaArray = editableLayers.toGeoJSON().features[0].geometry.coordinates[0];
			arrayChange();
			mymap.removeControl(drawControl);
			mymap.addControl(drawControl2);
		});

		mymap.on(L.Draw.Event.EDITED, function (e) {
			areaArray = editableLayers.toGeoJSON().features[0].geometry.coordinates[0];
			arrayChange();
		});

		mymap.on(L.Draw.Event.DELETED, function (e) {
			mymap.removeControl(drawControl2);
			mymap.addControl(drawControl);
			areaArray = [];
			arrayChange();
		});

		
		/**
		* end of draw layer
		*/
		if(areaArray != undefined){
			//temp geojson 
			var json = {"type": "FeatureCollection","features": [{"type": "Feature","properties": {},"geometry": {"type": "Polygon","coordinates":[areaArray]}}]};

			var addedJson = L.geoJson(json);

			addedJson.eachLayer(function(l){
				editableLayers.addLayer(l);								
			});

			mymap.removeControl(drawControl);
			mymap.addControl(drawControl2);
		}

    	function arrayChange(){
    		$("#coordentsInput").val(JSON.stringify(areaArray));
    	}

});// end of document.ready
</script>	



@stop
