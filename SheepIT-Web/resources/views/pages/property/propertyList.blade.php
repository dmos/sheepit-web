@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>

<style>
	#propertyTableScroll_wrapper{padding: 0px;}
</style>


<a href="{{ URL::to('/property/new')}}"><button class="btn btn-primary">Nova propeiadade <i class="fa fa-plus"></i></button></a>

<table id="propertyTableScroll" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>ID</th>
			<th>Descrição</th>
			<th>Dono</th>
			<th>Tipo</th>
			<th>Morada</th>
			<th></th>
		</tr>
	</thead>

	<tbody>
	<!--
		<tr>
			<td>1</td>
			<td>Ramos Pinto Portugal</td>
			<td>Pedro Gonçalves</td>
			<td>Vinhas</td>
			<td>Vila nova de Gaia - Porto</td>
			<td><a href="property/view/1" class="btn btn-primary btn-xs"><i class="fa fa-eye">&nbsp;</i>Ver</a></td>
		</tr>
	-->
	</tbody>
</table>



<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {

		
		//datatable 

		/**
		 * $propertySearch contains String that is passed as a parameter to be searched on the List
		 * @type {String}
		 */
		var propertySearch =  '{{ $propertySearch }}' ;

		createDatatableScroll("#propertyTableScroll","{{ URL::to('/propertyList')}}",true,propertySearch,400,true,[-1],[0, "asc"]);
		/*
		$("#propertyTableScroll").DataTable({
			searchDelay: 350,
	        scrollY: 400,
	        "scrollX": true,
    		scroller: true,
	        "bDeferRender": true,
			"deferRender": true,
			"columnDefs": [ {
			"targets": [-1],
			"orderable": false
			} ],
	        "searching": true,
			"search" : propertySearch
		});
		*/


	} ); /* document.ready */
	    
</script>

@stop
