@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>



@include('includes.leaflet')

<style type="text/css">
	span.bold{font-weight: bold;}	
</style>

<div class="row card-deck">
	<span class="text-danger" id="generalError"></span>
    <div class="col-md-6 mb-3">
        <div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="row m-0">
				<div class="col-4 p-0 pt-1 text-toggle">Informações <i class="fa fa-caret-up"></i></div>
				<div class="col-8 pr-0 pl-0">
					<button id="editing" class="btn btn-sm float-right mr-1"><span>Editar</span> <i class="fa fa-edit"></i></button>
				</div>
				</div>
			</div>
			<div class="card-body card-toggle" id="infos">
				<form method="POST" id="form" action="{{ URL::to('/property/view/'.$result[0]->pkpropertyid.'/updateInfo')}}">
					{{ csrf_field() }}
				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Cliente: </lable>
					<div class="col-sm-6 p0">
						<a class="col-sm-6 p0 bold span" href="{{URL::to('/client/view/'.$result[0]->pkenterpriseid)}}" id="clientnameSpan">
						{{$result[0]->enterprisename}}
						</a>
						<select class="editable editable-input d-none bold" id='clientname' name='clientname'>
						</select>
						<span class="text-danger error" id="clientnameerror"></span>
					</div>
				</div>

				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Descrição: </lable>
					<div class="col-sm-6 p0">
						<span class="bold span">{{$result[0]->description}}</span>
						<input type="text" id="descriptionInput" name="descriptionInput" class="editable editable-input d-none bold" value="{{$result[0]->description}}" disabled="disabled"/>
						<span class="text-danger error" id="descriptionInputerror"></span>
					</div>
				</div>
				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Tipo: </lable>
					<div class="col-sm-6 p0">
						<span class="bold span">{{$result[0]->type}}</span>
						<input type="text" id="typeInput" name="typeInput" class="editable editable-input d-none bold" value="{{$result[0]->type}}" disabled="disabled"/>
						<span class="text-danger error" id="typeInputerror"></span>
					</div>
				</div>
				</form>
			</div>
        </div>
    </div>
	

	<div class="col-md-6 mb-3">
        <div class="card text-black bg-card h-100">
			<div class="card-header">
				<div class="row m-0">
				<div class="col-4 p-0 pt-1 text-toggle">Localização  <i class="fa fa-caret-up"></i></div>
				<div class="col-8 pr-0 pl-0">
					<button id="editingAddress" class="btn btn-sm float-right mr-1"><span>Editar</span> <i class="fa fa-edit"></i></button>
				</div>
				</div>
			</div>
			<div class="card-body card-toggle" id="infoLocalizacao">
				<form method="POST" id="formAddress" action="{{ URL::to('/property/view/'.$result[0]->pkpropertyid.'/updateAddress')}}">
					{{ csrf_field() }}

				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Pais: </lable>
					<div class="col-sm-6 p0">
						<span class="bold spanAddress">{{$result[0]->country}}</span>
						<input type="text" id="countryInput" name="countryInput" class="editableAddress editable-input d-none bold" value="{{$result[0]->country}}" disabled="disabled"/>
						<span class="text-danger error" id="countryInputerror"></span>
					</div>
				</div>
				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Distrito: </lable>
					<div class="col-sm-6 p0">
						<span class="bold spanAddress">{{$result[0]->district}}</span>
						<input type="text" id="districtInput" name="districtInput" class="editableAddress editable-input d-none bold" value="{{$result[0]->district}}" disabled="disabled"/>
						<span class="text-danger error" id="districtInputerror"></span>
					</div>
				</div>
				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Localização: </lable>
					<div class="col-sm-6 p0">
						<span class="bold spanAddress">{{$result[0]->locality}}</span>
						<input type="text" id="localityInput" name="localityInput" class="editableAddress editable-input d-none bold" value="{{$result[0]->locality}}" disabled="disabled"/>
						<span class="text-danger error" id="localityInputerror"></span>
					</div>
				</div>
				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Rua: </lable>
					<div class="col-sm-6 p0">
						<span class="bold spanAddress">{{$result[0]->street}}</span>
						<input type="text" id="streetInput" name="streetInput" class="editableAddress editable-input d-none bold" value="{{$result[0]->street}}" disabled="disabled"/>
						<span class="text-danger error" id="streetInputerror"></span>
					</div>
				</div>
				<div class="form-group row mb-1">
					<lable class="col-sm-6 pl1 control-label">Codigo Postal: </lable>
					<div class="col-sm-6 p0">
						<span class="bold spanAddress">{{$result[0]->zipcode}}</span>
						<input type="text" id="zipcodeInput" name="zipcodeInput" class="editableAddress editable-input d-none bold" value="{{$result[0]->zipcode}}" disabled="disabled" pattern="[0-9-]{8,}"  title="Introduza o código postal como por exemplo: 1234-567"/>
						<span class="text-danger error" id="zipcodeInputerror"></span>
					</div>
				</div>
				</form>
			</div>
        </div>
    </div>

</div> <!-- end of row -->

<div class="row">
	<div class="col mb-3">
		<div class="card text-black bg-card">
			<div class="card-header">
				<div class="col-4 p-0 pt-1 text-toggle">Mapa  <i class="fa fa-caret-up"></i></div>
			</div>
			<div class="card-body card-toggle" id="infoMapa">
				<div id="mapid" style="height: 450px; width: 100%; margin: auto;"></div>

				<div class="row">
				    <div class="col-sm-12 pt-2">
				    	Legenda:
				    	<br>
			      		<span class="badge" style="background-color:#3388ff">Área da propriedade</span>   
				    </div>
			 	</div>
			</div>
        </div>
	</div>
</div><!-- end of row -->




<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {		
		$("#editing").click(function(event) {
			if($(this).children('span').html() == "Editar"){
				getSelectsInfos();
				$(".editable").prop("disabled",false);
				$(".editable").toggleClass('editable-input');			
				$(".editable").removeClass('d-none');
				$(".span").addClass('d-none');
				// TODO : possivelmente apagar estes hidden
				$(".hidden").removeClass('d-none');
				
				$(this).children('span').html("Concluir");
			}
			else{
				
				$.ajax({
					type: "POST",
					url: '{{ URL::to('/property/view/'.$result[0]->pkpropertyid.'/updateInfo')}}',
					data: $("#form").serialize(),
				})
				.done(function(result) {
					$(".text-danger").empty();
					$("#generalError").empty();
					$(".editable").removeClass('invalid');

					if(result.status == "sucesso"){
						location.replace("{{ URL::to('/property/view')}}/"+result.pkpropertyid);
					}
					if(result.status == "validation"){
						$.each(result.error , function( key, value ) {
							$("#"+key+"error").html("<br>"+value);
							$("#"+key).addClass('invalid');
						});
					}

					if(result.status == "error"){
						$("#generalError").html(result.error+"<br>");
					}
				});			
			}
		});

		function getSelectsInfos(){
			var currentValues = {client:"{{$result[0]->pkenterpriseid}}"}

			$.getJSON("{{ URL::to('/beacon/new/clienteList')}}",function(data) {
				//console.log(data);
				var ClientSelectHTML = "";
				for (var i=0; i< data.length; i++) {
					ClientSelectHTML += "<option value="+data[i].pkenterpriseid+">"+data[i].enterprisename+"</option>"
				}
				$("#clientname").html(ClientSelectHTML);
				if(currentValues.client > 0){
					$("#clientname").val(currentValues.client).change();
				}
			});
		}

		
		$("#editingAddress").click(function(event) {
			if($(this).children('span').html() == "Editar"){
				$(".editableAddress").prop("disabled",false);
				$(".editableAddress").toggleClass('editable-input');			
				$(".editableAddress").removeClass('d-none');
				$(".spanAddress").addClass('d-none');
				$(".hiddenAddress").removeClass('d-none');
				
				$(this).children('span').html("Concluir");
			}
			else{
				
				$.ajax({
					type: "POST",
					url: '{{ URL::to('/property/view/'.$result[0]->pkpropertyid.'/updateAddress')}}',
					data: $("#formAddress").serialize(),
				})
				.done(function(result) {
					$(".text-danger").empty();
					$("#generalError").empty();
					$(".editableAddress").removeClass('invalid');

					if(result.status == "sucesso"){
						location.replace("{{ URL::to('/property/view')}}/"+result.pkpropertyid);
					}
					if(result.status == "validation"){
						$.each(result.error , function( key, value ) {
							$("#"+key+"error").html("<br>"+value);
							$("#"+key).addClass('invalid');
						});
					}

					if(result.status == "error"){
						$("#generalError").html(result.error+"<br>");
					}
				});			
			}
		});
	}); /* document.ready */	    
</script>

<!-- MAPA -->
<script>
	var mymap;

	var markerArray=[];

	$(document).ready(function() {
		/**
		* base layer
		*/
		mymap = L.map('mapid').setView([0,0],18);
				
			L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
				attribution: '',
				maxZoom: 19,
				minZoom: 5,
				noWrap: true
			}).addTo(mymap);

			/**
			* draw layer
			*/

			var editableLayers = new L.FeatureGroup();
			mymap.addLayer(editableLayers);
			
			/*
			var MyCustomMarker = L.Icon.extend({
				options: {
					shadowUrl: null,
					iconAnchor: new L.Point(12, 12),
					iconSize: new L.Point(24, 24),
					iconUrl: 'js/leaflet/images/marker-icon.png'//bug
				}
			});
			*/

			
			var options = {
				position: 'topright',
				draw: {
					polyline: false,
					/*
					polygon: {
						allowIntersection: false, // Restricts shapes to simple polygons
						drawError: {
							color: '#e1e100', // Color the shape will turn when intersects
							message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
						},
						shapeOptions: {
							color: '#bada55'
						}
					},
					*/
					polygon: false,
					circle: false, // Turns off this drawing tool
					rectangle: {
						shapeOptions: {
							clickable: false
						}
					},
					marker: false,
					rectangle: false,
					circlemarker : false,
				},

				edit: {
					featureGroup: editableLayers, //REQUIRED!!
					remove: false
				}
			};
			
			var drawControl = new L.Control.Draw(options);
			mymap.addControl(drawControl);
			//console.log(L);
			mymap.on(L.Draw.Event.CREATED, function (e) {
				var type = e.layerType,
					layer = e.layer;
			
				editableLayers.addLayer(layer);
				//logChanges(); // log on create
				
			});

			//TODO : can be optimized
			//http://leaflet.github.io/Leaflet.Editable/doc/api.html
			mymap.on('draw:edited', function (e) {
				var layers = e.layers;
				layers.eachLayer(function (layer) {
					features = editableLayers.toGeoJSON().features;

					features.forEach(element => {                    
	                    if(element.geometry.type == "Polygon"){
	                    	$.ajaxSetup({
							    headers: {
							        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							    }
							});
	                        $.ajax({
	                        	url: '{{ URL::to('/property/view/'.$result[0]->pkpropertyid.'/updateCoord')}}',
	                        	type: 'post',
	                        	data: {coord: element.geometry.coordinates[0], _token: '{{csrf_token()}}'},
	                        })
	                    }
	                });
				});
			});

			/**
			* end of draw layer
			*/

			$.getJSON("{{ URL::to('/property/view/'.$result[0]->pkpropertyid.'/propertyViewMap')}}", function(json) {
				//L.mapbox.featureLayer().setGeoJSON(json).addTo(mapGeo);
				//console.log(JSON.stringify(json));
	        	//console.log(json);    


				function onEachFeature(feature, layer) {
					if (feature.properties && feature.properties.popupContent) {
						layer.bindPopup(feature.properties.popupContent);
					}
				}
				var addedJson = L.geoJson(json,{
					onEachFeature: onEachFeature
				});

				addedJson.eachLayer(
					function(l){
						editableLayers.addLayer(l);			
						if(l.feature.geometry.type == "Point"){
							//console.log(l);
							l.editing.disable();
							markerArray[l.feature.properties.beaconID] = l;
						}
						
				});
				
				//fit map to all points
				mymap.fitBounds(addedJson.getBounds(),{
					padding: [20,20]
				});
				
				/*
				mymap.eachLayer(function (layer) {
					console.log(layer);
				});*/

	    	}); /**end of $.get */


	    	setInterval(function(){
	    	 	$.getJSON("{{ URL::to('/property/view/'.$result[0]->pkpropertyid.'/propertyViewMap/mapBeaconInfoUpdate')}}", function(json){
	    	 		//console.log(json);
	    	 		for(i=0;i<json.length;i++){
	    	 			if(markerArray.indexOf(json[i].id) != -1) { 			
		    	 			markerArray[json[i].id].setLatLng([json[i].lat,json[i].lng]);
		    	 			markerArray[json[i].id]._popup.setContent(json[i].popup);
		    	 			//markerArray[json[i].id].dragging._draggable.disable()
	    	 			}
	    	 		}

	    		});
	    	}, 3000);

	    	// on edit click
		    $(".leaflet-draw-edit-edit").click(function() {
		    	//make all markers not editable. when marker is updated with setLatLng its made editable again.
		    	markerArray.forEach(function(element) {
				  element.editing.disable();
				});	    	
		    });
	});// end of document.ready
</script>	



@stop
