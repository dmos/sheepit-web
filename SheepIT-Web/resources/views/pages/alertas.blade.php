@extends('layouts.sidebar')
@section('content')

<link rel="stylesheet" href="{{URL::to('/vendor/datatable/datatables.min.css')}}">
<script src="{{URL::to('/vendor/datatable/datatables.min.js')}}"></script>

<h3>{{ $alertType }}</h3>

<!-- modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Alert Info</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<span class="pull-right">
					<button type="button" class="btn btn-primary">
						Mark as checked
					</button>
				</span>
			</div>
		</div>
	</div>
</div>


<table id="alertTable" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>id</th>
			<th>alertvalue</th>
			<th>checked</th>
			<th>type</th>
			<th>Action</th>
		</tr>
	</thead>

	<tbody>

	</tbody>
</table>



<script type="text/javascript" src="{{ URL::to('/js/datatable_custom.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('#modal').modal("hide");

		/**
		 * When the modal is opening, it gets the information from the database about the specific alert id
		 *
		 * TODO : 
		 * - rever qual informação importante
		 * - adicionar ação ao buttão de marcar com visto o alerta
		 * - possivelmente adicionar um buttão para ir para a overlha/rebanho/etc.
		 * - talvez passar a modal para o layout para ser reutilizavel
		 */
		$('#modal').on('show.bs.modal', function(e) {
			var htmlcontent = "";
			var id = $(e.relatedTarget).data('id');

		    $.get("{{ URL::to('/alertasInfo/')}}"+id, function(data) {

		    	htmlcontent += "<p>Alert ID : "+data.info[0].id+"</p>";
		    	htmlcontent += "<p>Alert Value : "+data.info[0].alertvalue+"</p>";
		    	htmlcontent += "<p>Checked : "+data.info[0].checked+"</p>";
		    	htmlcontent += "<p>Type : "+data.info[0].type+"</p>";
		    	htmlcontent += "<p>Log Time : "+data.info[0].logtime+"</p>";
		    	htmlcontent += "<p>Object : "+data.info[0].object+"</p>";
		    	htmlcontent += "<p>Percentage : "+data.info[0].percentage+"</p>";
		    	htmlcontent += "<p>Percentual : "+data.info[0].percentual+"</p>";
		    	htmlcontent += "<p>Steps : "+data.info[0].steps+"</p>";

		    	$("#modal-body").html(htmlcontent);
		    });

		    
			
		});


		//datatable 

		/**
		 * $alertaType contains type of alert that is passed as a parameter to be searched on
		 * @type {String}
		 */
		var alertType =  '{{ $alertType }}' ;

		
		createDatatable("#alertTable","{{ URL::to('/alertasData')}}",alertType);

	} ); /* document.ready */
	    
</script>

@stop
