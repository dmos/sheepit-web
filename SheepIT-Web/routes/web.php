<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// TODO : https://stackoverflow.com/questions/39555865/laravel-routegroup-for-admins

  Route::group(['middleware' => ['auth']], function () {

    // TODO : '/' perdeu-se e passou a 'home' ?? ha uma opeção para alterar isto
    /* 
    ----------------------------------------------------------------------
      DASHBOARD 
    ----------------------------------------------------------------------
    */
      Route::get('/home', ['as'=>'Painel de Controlo','uses'=>'DashboardController@index']);
    // TODO : Testing
      Route::get('/home2', ['as'=>'Painel de Controlo','uses'=>'DashboardController@index2']);

      /* dashboard charts */
      Route::get('/alertasPie', ['as'=>'alertasPie','uses'=>'DashboardController@numeroAlertasTotal']);
      
    /* end of DASHBOARD */

    /*
    ----------------------------------------------------------------------
      ANIMAL
    ----------------------------------------------------------------------
    */
   
    /* Animal List */
    Route::get('/animal', ['as'=>'Lista de Animais','uses'=>'AnimalController@index']);
    Route::get('/animal/search/{animalSearch?}', ['as'=>'Lista de Animais','uses'=>'AnimalController@index']);
    Route::get('/animalList', ['as'=>'Lista de Animais','uses'=>'AnimalController@animalList']);

    /* new Animal */
    Route::post('/animal/new', ['as'=>'Novo animal','uses'=>'AnimalController@registerAnimal']);
    Route::get('/animal/new', ['as'=>'Novo animal','uses'=>'AnimalController@newAnimal']);

    Route::get('/animal/new/clienteList', ['as'=>'Lista de clientes','uses'=>'AnimalController@clienteList']);
    Route::get('/animal/new/propertyList/{clientID}', ['as'=>'Lista de propriedades','uses'=>'AnimalController@propertyList']);
    Route::get('/animal/new/herdList/{propertyID}', ['as'=>'Lista de rebanhos','uses'=>'AnimalController@herdList']);
    Route::get('/animal/new/collarList/{propertyID}', ['as'=>'Lista de coleiras','uses'=>'AnimalController@collarList']);

    /* View Animal Info*/
    Route::get('/animal/view/{animalID}',['as'=>'Ver Animal','uses'=>'AnimalController@animalVeiwInfo']);
    Route::get('/animal/view/{animalID}/datatable',['as'=>'Ver Animal','uses'=>'AnimalController@animalVeiwInfoDatatable']);
    Route::get('/animal/view/{animalID}/lineChart/{dayStart}/{dayEnd}/{chartType}',['as'=>'Ver animal - LineChart','uses'=>'AnimalController@animalVeiwInfoLineChart']);

    /* Update Animal Info */
    Route::post('/animal/view/{animalID}/update',['as'=>'Update Animal','uses'=>'AnimalController@animalVeiwInfoUpdate']);
    Route::get('/animal/update/collarListUpdate/{propertyID}',['as'=>'Lista de coleiras - update','uses'=>'AnimalController@collarListUpdate']);

    // TODO : eleminar quando ja não for preciso
    Route::get('/animal/viewtest/{animalID}',['as'=>'Ver Animal','uses'=>'AnimalController@viewtest']);
    /* end of ANIMAL */



    /* 
    ----------------------------------------------------------------------
      HERD 
    ----------------------------------------------------------------------
    */
   /* Herd List */
    Route::get('/herd', ['as'=>'Lista de Rebanhos','uses'=>'HerdController@index']);
    Route::get('/herd/search/{herdSearch?}', ['as'=>'Lista de Faróis','uses'=>'HerdController@index']);
    Route::get('/herdList', ['as'=>'Lista de Rebanhos','uses'=>'HerdController@herdList']);

    /* New Herd */
    Route::post('/herd/new', ['as'=>'Novo Rebanho','uses'=>'HerdController@registerHerd']);
    Route::get('/herd/new', ['as'=>'Novo Rebanho','uses'=>'HerdController@newHerd']);

    Route::get('/herd/new/clienteList', ['as'=>'Lista de clientes','uses'=>'HerdController@clienteList']);
    Route::get('/herd/new/propertyList/{clientID}', ['as'=>'Lista de propriedades','uses'=>'HerdController@propertyList']);

     /* View Herd Info*/
    Route::get('/herd/view/{herdID}',['as'=>'Ver Rebanho','uses'=>'HerdController@herdVeiwInfo']);
    Route::get('/herd/view/{herdID}/animalList',['as'=>'Lista de Animais','uses'=>'HerdController@animalList']);

    Route::get('/herd/view/{herdID}/animalListAdd',['as'=>'Lista de Animais para adicionar','uses'=>'HerdController@animalListAdd']);
    Route::post('/herd/view/{herdID}/addAnimal',['as'=>'Adicionar Animal','uses'=>'HerdController@addAnimal']);

    /* Update Herd*/
    Route::post('/herd/view/{herdID}/update',['as'=>'Update Rebanho','uses'=>'HerdController@herdVeiwInfoUpdate']);



    /* end of HERD */


    /* 
    ----------------------------------------------------------------------
      COLLAR 
    ----------------------------------------------------------------------
    */

    /* Collar List */
    Route::get('/collar', ['as'=>'Lista de Coleiras','uses'=>'CollarController@index']);
    Route::get('/collar/search/{collarSearch?}', ['as'=>'Lista de Coleiras','uses'=>'CollarController@index']);
    Route::get('/collarList', ['as'=>'Lista de Coleiras','uses'=>'CollarController@collarList']);

    /* New Collar */
    Route::post('/collar/new', ['as'=>'Nova Coleira','uses'=>'CollarController@registerCollar']);
    Route::get('/collar/new', ['as'=>'Nova Coleira','uses'=>'CollarController@newCollar']);

    Route::get('/collar/new/clienteList', ['as'=>'Lista de clientes','uses'=>'CollarController@clienteList']);
    Route::get('/collar/new/propertyList/{clientID}', ['as'=>'Lista de propriedades','uses'=>'CollarController@propertyList']);
    Route::get('/collar/new/animalList/{propertyID}', ['as'=>'Lista de Animais','uses'=>'CollarController@animalList']);


    /* View Collar Info*/
    Route::get('/collar/view/{collarSerial}',['as'=>'Ver Coleira','uses'=>'CollarController@collarVeiwInfo']);
    Route::get('/collar/view/{animalID}/datatable',['as'=>'Ver Coleira','uses'=>'CollarController@collarVeiwInfoDatatable']);
    Route::get('/collar/view/{collarID}/lineChart/{dayStart}/{dayEnd}',['as'=>'Ver Coleira - LineChart','uses'=>'CollarController@collarVeiwInfoLineChart']);

    Route::get('/collar/update/animalListUpdate/{propertyID}',['as'=>'Ver Coleira - LineChart','uses'=>'CollarController@animalListUpdate']);
    Route::post('/collar/view/{collarSerial}/update',['as'=>'Update Coleiras','uses'=>'CollarController@collarVeiwInfoUpdate']);

    Route::get('/collar/view2/{collarSerial}',['as'=>'Ver Coleira','uses'=>'CollarController@collarVeiwInfo2']);

    /* View Collar Info Debug */
    Route::get('/collar/view/{collarSerial}/debug',['as'=>'Ver Coleira','uses'=>'CollarController@collarVeiwInfoDebug']);
    Route::get('/collar/view/{collarSerial}/debug/table',['as'=>'Ver Coleira','uses'=>'CollarController@collarVeiwInfoDebugTable']);

    /* View Collar on Map */
    Route::get('/collar/view/{collarSerial}/map',['as'=>'Ver Coleira no Mapa','uses'=>'CollarController@collarVeiwInfoMap']);
    Route::get('/collar/view/{collarSerial}/map/get',['as'=>'Ver Coleira no Mapa','uses'=>'CollarController@collarVeiwInfoMapGet']);
    Route::get('/collar/view/{collarSerial}/map/update',['as'=>'Ver Coleira no Mapa','uses'=>'CollarController@collarVeiwInfoMapUpdate']);

    /* end of Collar */


    /* 
    ----------------------------------------------------------------------
      BEACON 
    ----------------------------------------------------------------------
    */
    /* BEACON LIST */
    Route::get('/beacon', ['as'=>'Lista de faróis','uses'=>'BeaconController@index']);
    Route::get('/beacon/search/{collarSearch?}', ['as'=>'Lista de Faróis','uses'=>'BeaconController@index']);
    Route::get('/beaconList', ['as'=>'Lista de faróis','uses'=>'BeaconController@beaconList']);


    /* New Beacon */
    Route::post('/beacon/new', ['as'=>'Novo farol','uses'=>'BeaconController@registerBeacon']);
    Route::get('/beacon/new', ['as'=>'Novo farol','uses'=>'BeaconController@newBeacon']);

    Route::get('/beacon/new/clienteList', ['as'=>'Lista de clientes','uses'=>'BeaconController@clienteList']);
    Route::get('/beacon/new/propertyList/{clientID}', ['as'=>'Lista de propriedades','uses'=>'BeaconController@propertyList']);


    /* veiw beacon info */
    Route::get('/beacon/view/{serialnumber}',['as'=>'Ver farol','uses'=>'BeaconController@beaconVeiwInfo']);
    Route::get('/beacon/view/{serialnumber}/lineChart/{dayStart}/{dayEnd}',['as'=>'Ver farol - LineChart','uses'=>'BeaconController@beaconVeiwInfoLineChart']);
    Route::post('/beacon/view/{serialnumber}/update',['as'=>'Update Coleiras','uses'=>'BeaconController@beaconVeiwInfoUpdate']);


    /* View beacon Info Debug */
    Route::get('/beacon/view/{serialnumber}/debug',['as'=>'Ver farol','uses'=>'BeaconController@beaconVeiwInfoDebug']);
    Route::get('/beacon/view/{serialnumber}/debug/table',['as'=>'Ver farol','uses'=>'BeaconController@beaconVeiwInfoDebugTable']);

    /*  end of Beacon */


    /* 
    ----------------------------------------------------------------------
      PROPERTY 
    ----------------------------------------------------------------------
    */
    /* property list */
    Route::get('/property', ['as'=>'Lista de Propriedade','uses'=>'PropertyController@index']);
    Route::get('/property/search/{propertySearch?}', ['as'=>'Lista de Propriedade','uses'=>'PropertyController@index']);
    Route::get('/propertyList', ['as'=>'Lista de Propriedade','uses'=>'PropertyController@propertyList']);


    /* New Beacon */
    Route::post('/property/new', ['as'=>'Nova Propriedade','uses'=>'PropertyController@registerProperty']);
    Route::get('/property/new', ['as'=>'Nova Propriedade','uses'=>'PropertyController@newProperty']);

    Route::get('/property/new/clienteList', ['as'=>'Lista de clientes','uses'=>'PropertyController@clienteList']);
    

    /* View property Info*/
    Route::get('/property/view/{propertyID}',['as'=>'Ver Propriedade', 'uses'=>'PropertyController@propertyVeiwInfo']);
    Route::get('/property/view/{propertyID}/propertyViewMap',['as'=>'Ver Propriedade', 'uses'=>'PropertyController@propertyViewMap']);
    Route::get('/property/view/{propertyID}/propertyViewMap/mapBeaconInfoUpdate',['as'=>'Ver Propriedade', 'uses'=>'PropertyController@mapBeaconInfoUpdate']);
    
    /* Update Property */
    Route::post('/property/view/{propertyID}/updateInfo',['as'=>'Atualizar Propriedade','uses'=>'PropertyController@propertyVeiwInfoUpdate']);
    Route::post('/property/view/{propertyID}/updateAddress',['as'=>'Atualizar Morada da Propriedade','uses'=>'PropertyController@propertyVeiwInfoUpdateAddress']);
    Route::post('/property/view/{propertyID}/updateCoord',['as'=>'Atualizar coordenadas', 'uses'=>'PropertyController@updateCoord']);

    /* end of property */


    /*
    ----------------------------------------------------------------------
      USER
    ----------------------------------------------------------------------
    */
   
    Route::get('/user', ['as'=>'Lista de Utilizadores','uses'=>'UserController@index']);
    Route::get('/user/search/{userSearch?}', ['as'=>'Lista de Utilizadores','uses'=>'UserController@index']);
    Route::get('/userList', ['as'=>'Lista de Utilizadores','uses'=>'UserController@userList']);

    /* New USER */
    Route::post('/user/new', ['as'=>'Novo Utilizador','uses'=>'UserController@registerUser']);
    Route::get('/user/new', ['as'=>'Novo Utilizador','uses'=>'UserController@newUser']);

    Route::get('/user/new/clienteList', ['as'=>'Lista de clientes','uses'=>'UserController@clienteList']);
    Route::get('/user/new/UserTypeList', ['as'=>'Lista de clientes','uses'=>'UserController@UserTypeList']);

    /* View USER Info */
    Route::get('/user/view/{userid}',['as'=>'Ver Utilizador','uses'=>'UserController@userVeiwInfo']);
    Route::post('/user/view/{userid}/update',['as'=>'Update Utilizador','uses'=>'UserController@userVeiwInfoUpdate']);


    /* end of USER */


    /*
    ----------------------------------------------------------------------
      Client
    ----------------------------------------------------------------------
    */

    Route::get('/client', ['as'=>'Lista de Clientes','uses'=>'ClientController@index']);
    Route::get('/client/search/{clientSearch?}', ['as'=>'Lista de Clientes','uses'=>'ClientController@index']);
    Route::get('/clientList', ['as'=>'Lista de Clientes','uses'=>'ClientController@clientList']);

    /* new CLIENT */
    Route::post('/client/new', ['as'=>'Novo Cliente','uses'=>'ClientController@registerClient']);
    Route::get('/client/new', ['as'=>'Novo Cliente','uses'=>'ClientController@newClient']);
    Route::get('/client/new/clientTypeList', ['as'=>'Lista de Tipos de Cliente','uses'=>'ClientController@clientTypeList']);
    

    /* View Client Info */
    Route::get('/client/view/{clientID}',['as'=>'Ver Cliente','uses'=>'ClientController@clientVeiwInfo']);
    Route::get('/client/view/{clientID}/userList',['as'=>'Lista de Utilizadores','uses'=>'ClientController@userList']);
    Route::get('/client/view/{clientID}/propertyList',['as'=>'Lista de Utilizadores','uses'=>'ClientController@propertyList']);
    
    /* update Client */
    Route::post('/client/view/{clientID}/update',['as'=>'Update Cliente','uses'=>'ClientController@clientVeiwInfoUpdate']);
    Route::post('/client/view/{clientID}/updateAddress',['as'=>'Update Cliente','uses'=>'ClientController@clientVeiwInfoUpdateAddress']);

    /* end of CLIENTE */




    /* 
    ----------------------------------------------------------------------
      ALERTS 
    ----------------------------------------------------------------------
    */
    //alertType is parameter and ? means optional
    // TODO : passar para ingles as rotas
    Route::get('/alertas/{alertType?}', ['as'=>'Alertas','uses'=>'AlertsController@index']);
    Route::get('/alertasData', ['as'=>'AlertasData','uses'=>'AlertsController@alertsData']);
    Route::get('/alertasInfo/{id}', ['as'=>'AlertaInfo','uses'=>'AlertsController@getAlertInfo']);

    /* end of ALERTS */




    /* logout route */
      Route::get('Logout', 'AuthController@Logout');


  });//end of login routes
  


  /* ROUTES for Non-Logedin Useres */
  Route::group(['middleware' => ['guest']], function () {
    /* 
    ----------------------------------------------------------------------
      LOGIN
    ---------------------------------------------------------------------- 
    */
    Route::post('/login', ['as'=>'login','uses'=>'AuthController@doLogin']);
    Route::get('/login', ['as'=>'login','uses'=>'AuthController@index']);
    Route::get('/', ['as'=>'login','uses'=>'AuthController@index']);
    /* end of LOGIN */

  });
