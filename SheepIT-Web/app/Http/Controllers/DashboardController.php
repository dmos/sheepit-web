<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use View;


class DashboardController extends Controller{
  
    public function index(){
    	/**
    	 * creates dashboard page with sheepNumber, collerNumber, beaconNumber and array/collection with alerts that haven't been seen yet
    	 */
        
    	return View::make('pages.home')
            ->with('numeroOvelhas', $this->numeroOvelhas())

    		->with('numeroAlertasNaoVistos', $this->numeroAlertasNaoVistos())
            
            ->with('numeroColeiras', $this->numeroColeiras())
            ->with('numeroFarois', $this->numeroFarois())
            ;
    }

    // TODO : TEMP (verds)
    public function index2(){
        return View::make('pages.home2')->
            with(
                ['numeroOvelhas' => $this->numeroOvelhas(),
                'numeroColeiras' => $this->numeroColeiras(),
                'numeroFarois' => $this->numeroFarois()])
            ->with('numeroAlertasNaoVistos', $this->numeroAlertasNaoVistos() );
    }


    private function numeroOvelhas(){
    	/**
    	 * function that gets the number of sheep's that exists
    	 * $dados variable with number of sheep's
    	 * @var int
    	 */
        
        /* TODO : remover com o temp
    	$dados = DB::table('animal')->count();
    	return $dados;
        */
       
        try{
            $nAnimalTotal = DB::table('animal')->count();

            if($nAnimalTotal <= 0){
                $nAnimalTotal = 0;
            }

            $nAnimalActive = DB::table('animal')
                ->Join('animalcollar','animal.pkanimalid','=','animalcollar.fkanimalid')
                ->select('animalidtag')
                ->distinct('animalidtag')
                ->where('animalcollar.status','=','Colocada')
                ->count();

            if($nAnimalActive <= 0){
                $nAnimalActive = 0;
            }

            $dados['dados'] =array('nAnimalTotal'=>$nAnimalTotal,'nAnimalActive'=>$nAnimalActive);
        }   
        catch (Exception $e) {
            $dados['dados'] =array('nAnimalTotal'=>0,'nAnimalActive'=>0);
        }

        return $dados['dados'];

    }

    private function numeroColeiras(){
    	/**
    	 * function that gets the number of collar's that exists
    	 * $dados variable with number of collar's
    	 * @var int
    	 */
        
        /* TODO : remover com o temp
    	$dados = DB::table('collar')->count();
    	return $dados;
        */
       

        try{
            $nCollarTotal = DB::table('collar')->count();

            if($nCollarTotal <= 0){
                $nCollarTotal = 0;
            }

            $nCollarActive = DB::table('collar')
                ->Join('animalcollar','collar.pkcollarid','=','animalcollar.fkcollarid')
                ->select('pkcollarid')
                ->distinct('pkcollarid')
                ->where('animalcollar.status','=','Colocada')
                ->count();

            if($nCollarActive <= 0){
                $nCollarActive = 0;
            }

            $dados['dados'] =array('nCollarTotal'=>$nCollarTotal,'nCollarActive'=>$nCollarActive);
        }   
        catch (Exception $e) {
            $dados['dados'] =array('nCollarTotal'=>0,'nCollarActive'=>0);
        }

        return $dados['dados'];
    }

    private function numeroFarois(){
    	/**
    	 * function that gets the number of beacons that exists
    	 * $dados variable with number of beacons
    	 * @var int
    	 */
    	$dados = DB::table('beacon')->count();
    	return $dados;
    }


    private function numeroAlertasNaoVistos(){
    	/**
    	 * function that gets the number o alerts that haven't been seen yet of every type of alert
    	 * $dados variable that has all the alerts that haven't been seen yet
    	 * @var array
    	 */
        try{
            $dados = DB::table('alert')
            ->select(DB::raw("count(id) as total,type as label"))
            ->where('checked', '=', 'false')
            ->groupBy('type')
            ->pluck('total', 'label');
            if($dados->isEmpty()){
                $dados =array('OVERFENCE'=>'0','OVERPOSTURE'=>'0','UNDERSTEPS' =>'0','OVERSTEPS'=>'0');
            }
        }	
        catch (Exception $e) {
            $dados['dados'] =array('OVERFENCE'=>'0','OVERPOSTURE'=>'0','UNDERSTEPS' =>'0','OVERSTEPS'=>'0');
        }

    	return $dados;
    }


    /**
     * function that gets the number o alerts of every type of alert(seen or not) 
     * is used for the pie chart
     * @return JSON returns a json that contains the alerts and respective numbers
     */
    public function numeroAlertasTotal(){
    	try{
            $dados['dados'] = DB::table('alert')
            ->select(DB::raw("count(id) as total,type as label"))
            ->groupBy('type')
            ->get();
            
            if($dados['dados']->isEmpty()){
                $dados['dados'] =array('OVERFENCE'=>'0','OVERPOSTURE'=>'0','UNDERSTEPS' =>'0','OVERSTEPS'=>'0');
            }
        }
        catch (Exception $e) {
            $dados['dados'] =array('OVERFENCE'=>'0','OVERPOSTURE'=>'0','UNDERSTEPS' =>'0','OVERSTEPS'=>'0');
        }

    	return json_encode($dados);
    }


    /*
        TODO : 
        get a way to determine what is active, inactive and with warnings.
     */
}
