<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use DB;
use Validator;

class AnimalController extends Controller
{
    public function index($animalSearch = null){
    	
    	return View::make('pages.animal.animalList')->with(['animalSearch' => $animalSearch]);
    }

    public function animalList(){
    	$request=$_REQUEST;
    		    
	    $col =array(
	        0   =>  'animalidtag',
	        1   =>  'birthdata',
	        2   =>  'weight',
	        3   =>  'gender',
	        4	=> 	'breed',
	        5	=>	'Asso_Collar',
	        6   =>  'fkherdid'
		);  //create column like table in database //create filter animal get animal

		$result = DB::table('animal')
			->leftJoin('animalcollar','animal.pkanimalid','=','animalcollar.fkanimalid')
			->leftJoin('collar','animalcollar.fkcollarid','=','collar.pkcollarid')
			->leftJoin('device','collar.fkdeviceid','=','device.pkdeviceid')
		->select('animalidtag', 'birthdata','weight','gender','breed', 'device.serialnumber as Asso_Collar','fkherdid')
		->distinct('animalidtag');
		

    	//search
    	
		if(!empty($request['search']['value'])){
            $result = $result->whereRaw("(animalidtag::varchar ilike '".$request['search']['value']."%' 
            							OR birthdata::varchar ilike '".$request['search']['value']."%'
            							OR weight::varchar ilike '".$request['search']['value']."%' 
            							OR breed::varchar ilike '".$request['search']['value']."%'  
            							OR animalcollar.fkcollarid::varchar ilike '".$request['search']['value']."%'  
            							OR fkherdid::varchar ilike '".$request['search']['value']."%')");

            // var_dump($result->toSql());
            // exit;
		}

		$totalData = $result->count("animalidtag");
    	$totalFilter =$totalData;

		//order
		$result = $result
		         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
		         	->offset($request['start'])
		         	->limit($request['length']);
		         	
		$result = $result->get();

		$data = array();

		foreach($result as $r){		
			$temp = array();
		    $temp[] = $r->animalidtag;
	        $temp[] = $r->birthdata;
	        $temp[] = $r->weight;
	        $temp[] = $r->gender;
	        $temp[] = $r->breed;
	        $temp[] = $r->Asso_Collar;
	        $temp[] = $r->fkherdid;

	        $temp[] = '<a href="'.url('/animal/view').'/'.$r->animalidtag.'" class="btn btn-primary btn-xs">Ver <i class="fa fa-eye">&nbsp;</i></a>';
	        $data[] = $temp;
		}
		// echo "<br><br>";
		// var_dump($sql);
		// exit;
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }

    /* Create Animal */
    public function newAnimal(){
		return View::make('pages.animal.newAnimal');
	}

	public function registerAnimal(Request $request){
		//dd($_POST);exit;
		$rules = [
			'AnimalIDInput' => 'required|unique:animal,animalidtag',
			'BirthDateInput' => 'required|date_format:"Y-m-d"',
			'WeigthInput' => 'required|integer',
			'BreedInput' => 'required|max:50'
	    ];

	    $customMessages = [
	    	'AnimalIDInput.required' => 'O ID do Animal é obrigatorio!',
	    	'AnimalIDInput.unique' => 'O ID do Animal já existe!',

	    	'BirthDateInput.required'=> 'A Data de Nascimento é obrigatoria',
	    	'BirthDateInput.date_format'=> 'A Data de Nascimento está com o formato errado',

	    	'WeigthInput.required'=> 'O Peso é obrigatoria',
	    	'WeigthInput.integer'=> 'O Peso tem de ser valor exacto',

	    	'BreedInput.required'=> 'A Raça é obrigatoria',
	    	'BreedInput.max'=> 'A Raça Apenas tem um limit de :max caracteres',
	   ];

	    $this->validate($request, $rules, $customMessages);
	    
	    $HerdSelect = (isset($_POST['HerdSelect']) && $_POST['HerdSelect'] != 0  ) ? $_POST['HerdSelect'] : null;
	    $CollarSelect = (isset($_POST['CollarSelect']) && $_POST['CollarSelect'] != 0  ) ? $_POST['CollarSelect'] : null;

	    try{
			DB::beginTransaction();
			$pkanimalid = DB::table('animal')->insertGetId(
			    ['animalidtag' => $_POST['AnimalIDInput'],
			     'birthdata' => $_POST['BirthDateInput'],
			     'weight' => $_POST['WeigthInput'],
			     'gender' => $_POST['GenderInput'],
			     'breed' => $_POST['BreedInput'],
			     'fkherdid' => $HerdSelect,
			 	], 
			    'pkanimalid'//table primarykey
			);


			if($CollarSelect != null){
				$timestamp = date("Y/m/d") . " " . date("H:i:s");
				DB::table('animalcollar')->insert([
					['timestamp' => $timestamp,'status' =>'Colocada', 'fkcollarid' => $CollarSelect, 'fkanimalid' => $pkanimalid]
				]);
			}

			DB::commit();
			return redirect('/animal/view/'.$_POST['AnimalIDInput']);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return redirect('/animal/new')->withErrors(['Error'=>"Occoreu um erro! Tente novamente"])->withInput();
		}	
	}

	public function clienteList(){
		$result = DB::table("enterprise")
			->select('pkenterpriseid','enterprisename')
			->get();
		echo json_encode($result);
	}

	public function propertyList($clientID){
		$result = DB::table("property")
			->select('pkpropertyid', 'description')
			->where("property.fkenterpriseid", "=",$clientID)
			->get();
		echo json_encode($result);
	}

	public function herdList($propertyID){
		$result = DB::table("herd")
			->select('pkherdid', 'name')
			->where("herd.fkpropertyid", "=",$propertyID)
			->get();
		echo json_encode($result);
	}

	public function collarList($propertyID){	
		$result = DB::table("device")
			->join('collar', 'device.pkdeviceid', '=', 'collar.fkdeviceid')
			->leftJoin('animalcollar', 'collar.pkcollarid', '=', 'animalcollar.fkcollarid')
			->select('collar.pkcollarid', 'device.serialnumber')
			->where("device.fkpropertyid", "=",$propertyID)
			->whereRaw("collar.pkcollarid not in (select fkcollarid from animalcollar where fkcollarid is not null)")
			->get();
		echo json_encode($result);
	}


    /* VIEW PAGE */
    public function animalVeiwInfo($animalID){
    	$result = DB::table('animal')
			->leftJoin('animalcollar','animal.pkanimalid','=','animalcollar.fkanimalid')
			->leftJoin('herd','animal.fkherdid','=','herd.pkherdid')
			->leftJoin('collar','animalcollar.fkcollarid','=','collar.pkcollarid')
			->leftJoin('device','collar.fkdeviceid','=','device.pkdeviceid')
			->leftJoin('property','device.fkpropertyid','=','property.pkpropertyid')
			->leftJoin('enterprise','property.fkenterpriseid','=','pkenterpriseid')
			->select('animalidtag', 'birthdata','weight','gender','breed', 'pkcollarid', 'device.pkdeviceid', 'device.serialnumber as Asso_Collar','fkherdid','herd.name as herdName','enterprise.pkenterpriseid', 'property.pkpropertyid')
			->distinct('animalidtag')
			->where('animal.animalidtag', '=', $animalID)
			->limit(1)
			->get()
			->toArray();
		 	// ->toSql();
		
		$birthdata = date("d/m/Y",strtotime($result[0]->birthdata));

		$animalID = $this->getPkAnimalID($animalID);
		$lastDay = DB::table("animalrecordcache")
			->select("timestamp")
			->where("fkanimalid","=",$animalID)
			->whereNotNull("timestamp")
			->limit(1)
			->get();

		$lastDay = $lastDay->isEmpty() ? date("Y-m-d"): $lastDay[0]->timestamp;
		
		$result[0]->gender = $result[0]->gender == 'F' ? 'Feminino' : 'Masculino';
    	return View::make('pages.animal.VeiwInfo')->with(['result' => $result,'lastDay' =>$lastDay,'birthdata'=>$birthdata]);
    }

    public function animalVeiwInfoDatatable($animalID){
    	$animalID = $this->getPkAnimalID($animalID);

		// datatable
		$request=$_REQUEST;

		//dd($request);

    	$col =array(
	        0   =>  'nearestbeacon',
	        1   =>  'timestamp',
	        2   =>  'numbersteps',
	        3   =>  'postureinfraction',
		);

    	$result = DB::table('animalrecordhistory')
    		->select("animalrecordhistory.nearestbeacon","animalrecordhistory.timestamp","animalrecordhistory.numbersteps","animalrecordhistory.postureinfraction")
			->where('animalrecordhistory.fkanimalid', '=', $animalID)
			->whereNotNull('animalrecordhistory.timestamp');
			//->limit(100);
			//->get();
		
    	//search
    	
		if(!empty($request['search']['value'])){
            $result = $result->whereRaw("(nearestbeacon::varchar ilike '".$request['search']['value']."%' 
            							OR timestamp::varchar ilike '".$request['search']['value']."%' 
            							OR numbersteps::varchar ilike '".$request['search']['value']."%'  
            							OR postureinfraction::varchar ilike '".$request['search']['value']."%')");

            // var_dump($result->toSql());
            // exit;

	        
		}

		$totalData=$result->count("nearestbeacon");
		$totalFilter= $totalData;

		//order
		$result = $result
	         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
	         	->offset($request['start'])
	         	->limit($request['length']);	

	    $result = $result->get();

		$data = array();
		foreach($result as $r){
			$temp = array();
		    $temp[] = $r->nearestbeacon;
	        $temp[] = $r->timestamp;
	        $temp[] = $r->numbersteps;
	        $temp[] = $r->postureinfraction;

	        $data[] = $temp;
		}
		// echo "<br><br>";
		// var_dump($sql);
		// exit;
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }

    public function animalVeiwInfoLineChart($animalID,$dayStart,$dayEnd,$chartType){
    	$animalID = $this->getPkAnimalID($animalID);
				
		$chartType = $this->getChartTypeInfo($chartType);

		$dayEnd = date('Y-m-d',strtotime($dayEnd."+1 days")) ;//offset

		/*
		$result = DB::table('animalrecordhistory')//timestamp_hour
			->select(DB::raw("to_timestamp(floor((extract('epoch' from timestamp) / 1800 )) * 1800) AT TIME ZONE 'UTC' as timestamp_hour, SUM(animalrecordhistory.".$chartType[0].") as counter"))
			->where('animalrecordhistory.fkanimalid', '=', $animalID)
			->whereRaw("animalrecordhistory.timestamp >= '".$dayStart."'  And animalrecordhistory.timestamp < '".$dayEnd."'")
			->whereNotNull('animalrecordhistory.timestamp')
			->groupBy('timestamp_hour')
			->orderBy('timestamp_hour', 'asc')
			->get()
			->toArray();
		*/	

		// TODO : subquery optimization
		$result = DB::table('animalrecordhistory')//timestamp_hour
			->select(DB::raw("to_timestamp(ceil((extract('epoch' from timestamp) / (30* 60) )) * (30* 60)) AT TIME ZONE 'UTC' as timestamp_hour, animalrecordhistory.".$chartType[0]." as counter"))
			->where('animalrecordhistory.fkanimalid', '=', $animalID)
			->whereRaw("animalrecordhistory.timestamp >= '".$dayStart."'  And animalrecordhistory.timestamp < '".$dayEnd."'")
			->whereNotNull('animalrecordhistory.timestamp')
			->whereRaw("animalrecordhistory.timestamp in (
							select max(timestamp) as maxtime
							from animalrecordhistory
							where animalrecordhistory.fkanimalid = ".$animalID." 
								and animalrecordhistory.timestamp is not null 
								and animalrecordhistory.timestamp >= '".$dayStart."'  
								And animalrecordhistory.timestamp < '".$dayEnd."' 
							group by to_timestamp(ceil((extract('epoch' from timestamp) / (30* 60) )) * (30* 60))
							order by maxtime desc
						)")
			->orderBy('timestamp_hour', 'asc')
			->get()
			->toArray();

		
		$data = array();
		foreach($result as $r){
			//echo substr($r->timestamp, 0, 10) . '<br>';		
			$temp = array();
		    //$temp[] = substr($r->timestamp_hour, 10, 20);
		    //$temp[] = array(substr($r->timestamp_hour, 10, 20), substr($r->timestamp_hour, 0, 10));
		    $temp[] = $r->timestamp_hour;
	        $temp[] = $r->counter;
	        $data[] = $temp;
		}

		$graph = array(
			"label"=> $chartType[1],
		    "color"=> $chartType[2],
		    "data"=> $data
		);
		
		echo json_encode($graph);
    }

    

    /* UPDATE INFO OF ANIMAL */
    public function animalVeiwInfoUpdate($animalID,Request $request){
    	$pkanimalid = $this->getPkAnimalID($animalID);
    	$pkanimalcollarid = $this->getPKAnimalCollarID($pkanimalid);	    
		$rules = [
			'AnimalIDInput' => 'required|unique:animal,animalidtag,'.$pkanimalid.',pkanimalid',
			'BirthDateInput' => 'required|date_format:"Y-m-d"',
			'WeigthInput' => 'required|integer',
			'BreedInput' => 'required|max:50',
			'CollarSelect' => 'unique:animalcollar,fkcollarid,'.$pkanimalcollarid.',pkanimalcollarid',
	    ];
		
	    $customMessages = [
	    	'AnimalIDInput.required' => 'O ID do Animal é obrigatorio!',
	    	'AnimalIDInput.unique' => 'O ID do Animal já existe!',

	    	'BirthDateInput.required'=> 'A Data de Nascimento é obrigatoria',
	    	'BirthDateInput.date_format'=> 'A Data de Nascimento está com o formato errado',

	    	'WeigthInput.required'=> 'O Peso é obrigatoria',
	    	'WeigthInput.integer'=> 'O Peso tem de ser valor exacto',

	    	'BreedInput.required'=> 'A Raça é obrigatoria',
	    	'BreedInput.max'=> 'A Raça Apenas tem um limit de :max caracteres',

	    	'CollarSelect.unique' => 'A Colleira Selecionada já esta em uso',
	   	];

	   	$validator = Validator::make( $request->all(), $rules, $customMessages );
	    if ($validator->fails()) {    
            return response()->json(['status'=>'validation','error'=>$validator->errors()]);
        }
        
        // depois pegar no novo codigo e alterar as outras paginas de update a nivel visual
	    $HerdSelect = (isset($_POST['HerdSelect']) && $_POST['HerdSelect'] != 0  ) ? $_POST['HerdSelect'] : null;
	    $CollarSelect = (isset($_POST['CollarSelect']) && $_POST['CollarSelect'] != 0  ) ? $_POST['CollarSelect'] : null;    
	    
	    try{
			DB::beginTransaction();

			DB::table('animal')
            	->where('pkanimalid', $pkanimalid)
            	->update([
            		'animalidtag' => $_POST['AnimalIDInput'],
				    'birthdata' => $_POST['BirthDateInput'],
				    'weight' => $_POST['WeigthInput'],
				    'gender' => $_POST['GenderInput'],
				    'breed' => $_POST['BreedInput'],
				    'fkherdid' => $HerdSelect,
				 ]);          
            
            DB::table('animalcollar')
            	->where('fkanimalid', $pkanimalid)
            	->update(['fkcollarid' => $CollarSelect]);

			DB::commit();

			return response()->json([
			    'status' => 'sucesso',
			    'animalidtag' => $_POST['AnimalIDInput']
			]);
			

		} catch(\Exception $e){
			DB::rollback();
			echo  "ERROR: <br>".$e->getMessage();
			//return response()->json(['status'=>'error','error'=>'Occoreu um erro! Tente novamente']);
		}	
    }

    public function collarListUpdate($propertyID){
		$result = DB::table("device")
			->join('collar', 'device.pkdeviceid', '=', 'collar.fkdeviceid')
			->select('collar.pkcollarid', 'device.serialnumber')
			->where("device.fkpropertyid", "=",$propertyID)
			->get();
		echo json_encode($result);
	}



    /* private controller functions */

    /**
     * [getPkAnimalID description]
     * @param  [string] $animalPublicID [animal public id (animalidtag)]
     * @return [int] [returns the primarykey of the animal]
     */
    private function getPkAnimalID($animalPublicID){
    	$temp = DB::table('animal')
			->select('pkanimalid')
			->where('animal.animalidtag', '=', $animalPublicID)
			->get();
    	return $temp[0]->pkanimalid;
    }

    private function getPKAnimalCollarID($pkanimalid){
    	$temp = DB::table('animalcollar')
			->select('pkanimalcollarid')
			->where('fkanimalid', '=', $pkanimalid)
			->get();
    	return $temp[0]->pkanimalcollarid;
    }

    private function getChartTypeInfo($chartType){
    	if($chartType == 2)
    		return array("fenceinfraction","Numero de infrações de cerca","#ffc107");
    	if($chartType == 3)
    		return array("numbersteps","Numero de Passos","#20c997");
    	return array("postureinfraction","Numero de infrações de postura","#00BCD4");
    }


    // TODO : eleminar quando não for preciso
    public function viewtest($animalID){
    	$result = DB::table('animal')
			->leftJoin('animalcollar','animal.pkanimalid','=','animalcollar.fkanimalid')
			->leftJoin('herd','animal.fkherdid','=','herd.pkherdid')
			->leftJoin('collar','animalcollar.fkcollarid','=','collar.pkcollarid')
			->leftJoin('device','collar.fkdeviceid','=','device.pkdeviceid')
			->leftJoin('property','device.fkpropertyid','=','property.pkpropertyid')
			->leftJoin('enterprise','property.fkenterpriseid','=','pkenterpriseid')
			->select('animalidtag', 'birthdata','weight','gender','breed', 'pkcollarid', 'device.pkdeviceid', 'device.serialnumber as Asso_Collar','fkherdid','herd.name as herdName','enterprise.pkenterpriseid', 'property.pkpropertyid')
			->distinct('animalidtag')
			->where('animal.animalidtag', '=', $animalID)
			->limit(1)
			->get()
			->toArray();
		 	// ->toSql();
		
		$birthdata = date("d/m/Y",strtotime($result[0]->birthdata));

		$animalID = $this->getPkAnimalID($animalID);

		$lastDay = date("Y-m-d",strtotime("2017/06/12"));
		
		$result[0]->gender = $result[0]->gender == 'F' ? 'Feminino' : 'Masculino';
    	return View::make('pages.animal.VeiwInfoTest')->with(['result' => $result,'lastDay' =>$lastDay,'birthdata'=>$birthdata]);
    }
    public function animalVeiwInfoLineChart_old($animalID,$dayStart,$dayEnd,$chartType){
    	$animalID = $this->getPkAnimalID($animalID);
		/* 1 day only
		$result = DB::table('animalrecordhistory')//timestamp_hour
			->select(DB::raw("to_timestamp(floor((extract('epoch' from timestamp) / 1800 )) * 1800) AT TIME ZONE 'UTC' as timestamp_hour, SUM(animalrecordhistory.postureinfraction) as postureinfraction"))
			->where('animalrecordhistory.fkanimalid', '=', $animalID)
			->whereRaw("animalrecordhistory.timestamp::text LIKE '".$day."%' ")
			->whereNotNull('animalrecordhistory.timestamp')
			->groupBy('timestamp_hour')
			->orderBy('timestamp_hour', 'asc')
			->limit(100)
			->get()
			->toArray();
		*/
		
		$chartType = $this->getChartTypeInfo($chartType);

		$dayEnd = date('Y-m-d',strtotime($dayEnd."+1 days")) ;//offset

		$result = DB::table('animalrecordhistory')//timestamp_hour
			->select(DB::raw("to_timestamp(floor((extract('epoch' from timestamp) / 1800 )) * 1800) AT TIME ZONE 'UTC' as timestamp_hour, SUM(animalrecordhistory.".$chartType[0].") as counter"))
			->where('animalrecordhistory.fkanimalid', '=', $animalID)
			->whereRaw("animalrecordhistory.timestamp >= '".$dayStart."'  And animalrecordhistory.timestamp < '".$dayEnd."'")
			->whereNotNull('animalrecordhistory.timestamp')
			->groupBy('timestamp_hour')
			->orderBy('timestamp_hour', 'asc')
			->get()
			->toArray();
	

		$data = array();
		//$result = array_reverse($result); //para aparecer os dados do grafico de forma correta
		foreach($result as $r){
			//echo substr($r->timestamp, 0, 10) . '<br>';		
			$temp = array();
		    //$temp[] = substr($r->timestamp_hour, 10, 20);
		    $temp[] = $r->timestamp_hour;
	        $temp[] = $r->counter;
	        $data[] = $temp;
		}
		
		$graph = array(
			"label"=> $chartType[1],
		    "color"=> $chartType[2],
		    "data"=> $data
		);
		
		echo json_encode($graph);
    }
    
}
