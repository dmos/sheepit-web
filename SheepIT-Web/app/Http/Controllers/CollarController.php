<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use DB;
use Validator;

class CollarController extends Controller
{
    /**
	 * Creates the view of the collar list page.
	 * @param  [String] $collarSearch [Containes the search value to filter the collares. if none is given, then the default value is null]
	 * @return [View] [returns the view of the collar list page]
	 */
    public function index($collarSearch = null){
    	
    	return View::make('pages.collar.collarList')->with(['collarSearch' => $collarSearch]);
    }


    /**
     * function that gets the list of collares for the datatable. this function also does the search option of the datatable
     * @return [JSON]  [returns a json specifically formated for the datatable that contains the data of the table]
     */
    public function collarList(){
    	/**
    	 * TODO :
    	 * feito mas tem de se rever o que há aqui para fazer! como comentar
    	 * falta a tabela de Alertas.
    	 * falta informação dos mockups
    	 */

    	$request=$_REQUEST;
    		    
	    $col =array(
	        0   =>  'serialnumber',
	        1   =>  'version',
	        2   =>  'battery',
	        3   =>  'uptime',
	        4	=> 	'collaridnetwork',
	        5	=>	'Assocs_Animal',
	        6   =>  'EventsTable'
		);  //create column like table in database //create filter animal get animal

		$result = DB::table('device')
			->join('collar', 'device.pkdeviceid', '=', 'collar.fkdeviceid')
			->leftJoin('devicerecordcache','device.pkdeviceid','=','devicerecordcache.fkdeviceid')
			->leftJoin('animalcollar','collar.pkcollarid','=','animalcollar.fkcollarid')
			->leftJoin('animal','animalcollar.fkanimalid','=','animal.pkanimalid')
		->select('serialnumber', 'version','pkdeviceid','collaridnetwork','uptime', 'battery','animalidtag as Assocs_Animal')
		->distinct('serialnumber');
		
    	


    	//search
    	
		if(!empty($request['search']['value'])){
            $result = $result->whereRaw("(serialnumber::varchar ilike '".$request['search']['value']."%' OR version::varchar ilike '".$request['search']['value']."%' OR collaridnetwork::varchar ilike '".$request['search']['value']."%')");

            // var_dump($result->toSql());
            // exit;

	     
		}
		$totalData = $result->count("serialnumber");
    	$totalFilter =$totalData;


		//order
		$result = $result
		         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
		         	->offset($request['start'])
		         	->limit($request['length']);
		$result = $result->get();



		$data = array();

		foreach($result as $r){		
			$temp = array();
		    $temp[] = $r->serialnumber;
	        $temp[] = $r->version;
	        $temp[] = $r->battery;
	        $temp[] = $r->uptime;
	        $temp[] = $r->collaridnetwork;//id de rede
	        $temp[] = $r->Assocs_Animal;//Animal associado
	        $temp[] = "0";//Alertas

	        $temp[] = '<a href="'.url('/collar/view//').'/'.$r->serialnumber.'" class="btn btn-primary btn-xs">Ver <i class="fa fa-eye">&nbsp;</i></a>';
	        $data[] = $temp;
		}
		// echo "<br><br>";
		// var_dump($sql);
		// exit;
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }


	/* NEW COLLAR PAGE */

	public function newCollar(){
		return View::make('pages.collar.newCollar');
	}

	public function registerCollar(Request $request){
		//dd($request);exit;


		$rules = [
			'serialNumberInput' => 'required|unique:device,serialnumber',
			'VersionInput' => 'required',
	    ];

	    $customMessages = [
	    	'serialNumberInput.required' => 'Numero de Série é obrigatorio!',
	    	'serialNumberInput.unique' => 'Numero de Série já existe!',
	    	'VersionInput.required'=> 'A versão é obrigatoria'
	   ];

	    $this->validate($request, $rules, $customMessages);
	    
	    $PropertySelect = (isset($_POST['PropertySelect']) && $_POST['PropertySelect'] != 0  ) ? $_POST['PropertySelect'] : null;
	    $AnimalSelect = (isset($_POST['AnimalSelect'])  && $_POST['AnimalSelect'] != 0 ) ? $_POST['AnimalSelect'] : null;
	    
	    
	    try{
			DB::beginTransaction();
			$id = DB::table('device')->insertGetId(
			    ['version' => $_POST['VersionInput'] , 'serialnumber' => $_POST['serialNumberInput'],'fkpropertyid'=> $PropertySelect] , 
			    'pkdeviceid'
			);

			$collarid = DB::table('collar')->insertGetId(
			    ['fkdeviceid' => $id],
			    'pkcollarid'
			);

			if($AnimalSelect != null){
				$timestamp = date("Y/m/d") . " " . date("H:i:s");
				DB::table('animalcollar')->insert([
					['timestamp' => $timestamp,'status' =>'Colocada', 'fkcollarid' => $collarid, 'fkanimalid' => $AnimalSelect]
				]);
			}

			DB::commit();
			return redirect('/collar/view/'.$_POST['serialNumberInput']);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return redirect('/collar/new')->withErrors(['Error'=>"Occoreu um erro! Tente novamente"])->withInput();
		}	
	}


	public function clienteList(){
		$result = DB::table("enterprise")
			->select('pkenterpriseid','enterprisename')
			->get();
		echo json_encode($result);
	}

	public function propertyList($clientID){
		$result = DB::table("property")
			->select('pkpropertyid', 'description')
			->where("property.fkenterpriseid", "=",$clientID)
			->get();
		echo json_encode($result);
	}

	public function animalList($propertyID){
		$result = DB::table("animal")
			->join('herd', 'animal.fkherdid', '=', 'herd.pkherdid')
			->select('animal.pkanimalid', 'animal.animalidtag')
			->where("herd.fkpropertyid", "=",$propertyID)
			->whereRaw("animal.pkanimalid not in (select distinct fkanimalid from animalcollar where fkcollarid is not null)")
			->get();
		echo json_encode($result);
	}




	/* COLLAR INFO PAGE */

    /**
     * [Creates the view of the collar]
     * @param [String] $collarSerial [contains the serial number of the collar]
     * @return [view] [returns the view of the collar info page]
     */
    public function collarVeiwInfo($collarSerial){
    	//dd($collarSerial);exit;
    	$result = DB::table('device')
			->join('collar', 'device.pkdeviceid', '=', 'collar.fkdeviceid')
			->leftJoin('devicerecordcache', 'device.pkdeviceid', '=', 'devicerecordcache.fkdeviceid')
			->leftJoin('animalcollar', 'collar.pkcollarid', '=', 'animalcollar.fkcollarid')
			->leftJoin('animal', 'animal.pkanimalid', '=', 'animalcollar.fkanimalid')
			->leftJoin('property', 'property.pkpropertyid', '=', 'device.fkpropertyid')
			->leftJoin('enterprise', 'enterprise.pkenterpriseid', '=', 'property.fkenterpriseid')
			->select('device.serialnumber','device.version','devicerecordcache.battery','devicerecordcache.uptime','devicerecordcache.timestamp','collar.pkcollarid','collar.collaridnetwork','animal.animalidtag as animalpublicid','pkanimalid','enterprise.pkenterpriseid','enterprise.enterprisename as clientname','property.description as propertydescription', 'property.pkpropertyid')
			->distinct('device.serialnumber')
			->where('device.serialnumber', '=', $collarSerial)
			->limit(1)
			->get()
			->toArray();
		 	// ->toSql();
		 //dd($result); exit;
		 
		$deviceID = $this->getPkDeviceID($collarSerial);
		$lastDay = DB::table('devicerecordcache')
		 	->select("timestamp")
		 	->where('fkdeviceid', '=', $deviceID)
		 	->get();

		$lastDay = $lastDay->isEmpty() ? date("Y-m-d"): $lastDay[0]->timestamp;

    	return View::make('pages.collar.VeiwInfo')->with(['result' => $result,'lastDay'=>$lastDay]);
    }
    public function collarVeiwInfo2($collarSerial){
    	//dd($collarSerial);exit;
    	$result = DB::table('device')
			->join('collar', 'device.pkdeviceid', '=', 'collar.fkdeviceid')
			->leftJoin('devicerecordcache', 'device.pkdeviceid', '=', 'devicerecordcache.fkdeviceid')
			->leftJoin('animalcollar', 'collar.pkcollarid', '=', 'animalcollar.fkcollarid')
			->leftJoin('animal', 'animal.pkanimalid', '=', 'animalcollar.fkanimalid')
			->leftJoin('property', 'property.pkpropertyid', '=', 'device.fkpropertyid')
			->leftJoin('enterprise', 'enterprise.pkenterpriseid', '=', 'property.fkenterpriseid')
			->select('device.serialnumber','device.version','devicerecordcache.battery','devicerecordcache.uptime','devicerecordcache.timestamp','collar.pkcollarid','collar.collaridnetwork','animal.animalidtag as animalpublicid','pkanimalid','enterprise.pkenterpriseid','enterprise.enterprisename as clientname','property.description as propertydescription', 'property.pkpropertyid')
			->distinct('device.serialnumber')
			->where('device.serialnumber', '=', $collarSerial)
			->limit(1)
			->get()
			->toArray();
		 	// ->toSql();
		 //dd($result); exit;
		 
		$deviceID = $this->getPkDeviceID($collarSerial);
		$lastDay = DB::table('devicerecordcache')
		 	->select("timestamp")
		 	->where('fkdeviceid', '=', $deviceID)
		 	->get();

		$lastDay = $lastDay->isEmpty() ? date("Y-m-d"): $lastDay[0]->timestamp;

    	return View::make('pages.collar.VeiwInfo2')->with(['result' => $result,'lastDay'=>$lastDay]);
    }


    public function collarVeiwInfoDatatable($animalID){

    	$result = DB::table('animalrecordhistory')
    		->select("animalrecordhistory.nearestbeacon","animalrecordhistory.timestamp","animalrecordhistory.numbersteps","animalrecordhistory.postureinfraction")
			->where('animalrecordhistory.fkanimalid', '=', $animalID)
			->whereNotNull('animalrecordhistory.timestamp')
			->orderBy('animalrecordhistory.timestamp', 'desc')
			->limit(30);
			//->get();
		$count = $result->count();
		$result = $result->get();

		// datatable
		$request=$_REQUEST;

		$totalData = $count;
    	$totalFilter =$totalData;

		$data = array();

		foreach($result as $r){
			$temp = array();
		    $temp[] = $r->nearestbeacon;
	        $temp[] = $r->timestamp;
	        $temp[] = $r->numbersteps;
	        $temp[] = $r->postureinfraction;

	        $data[] = $temp;
		}
		// echo "<br><br>";
		// var_dump($sql);
		// exit;
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }

    /* Collar Graph */
    public function collarVeiwInfoLineChart($collarID,$dayStart,$dayEnd){
    	$collarID = $this->getPkDeviceID($collarID);
    	/* 1 day
    	$result = DB::table('devicerecordhistory')
    		->select("timestamp","battery")
			->where('fkdeviceid', '=', $collarID)
			->whereRaw("devicerecordhistory.timestamp::text LIKE '".$day."%' ")
			->whereNotNull('timestamp')
			->orderBy('timestamp', 'desc')
			->limit(100)
			->get()
			->toArray();

		$result = array_reverse($result); //para aparecer os dados do grafico de forma correta
		*/
		$dayEnd = date('Y-m-d',strtotime($dayEnd."+1 days")) ;//offset
		$result = DB::table('devicerecordhistory')//timestamp_hour
			->select(DB::raw("to_timestamp(floor((extract('epoch' from timestamp) / 1800 )) * 1800) AT TIME ZONE 'UTC' as timestamp_hour, AVG(devicerecordhistory.battery) as battery"))
			->where('devicerecordhistory.fkdeviceid', '=', $collarID)
			->whereRaw("devicerecordhistory.timestamp >= '".$dayStart."'  And devicerecordhistory.timestamp < '".$dayEnd."'")
			->whereNotNull('devicerecordhistory.timestamp')
			->groupBy('timestamp_hour')
			->orderBy('timestamp_hour', 'asc')
			->get()
			->toArray();


		$data = array();
		foreach($result as $r){
			//echo substr($r->timestamp, 0, 10) . '<br>';		
			$temp = array();
		    //$temp[] = substr($r->timestamp_hour, 10, 20);
		    $temp[] = $r->timestamp_hour;
	        $temp[] = $r->battery;
	        $data[] = $temp;
		}
		//var_dump(json_encode($data));
		
		$graph = array(
			"label"=> "Percentagem da bateria",
		    "color"=> "#00BCD4",
		    "data"=> $data
		);
		//var_dump($graph);
		
		echo json_encode($graph);
    }


    /* UPDATE INFO*/
    public function animalListUpdate($propertyID){
		$result = DB::table("animal")
			->join('herd', 'animal.fkherdid', '=', 'herd.pkherdid')
			->select('animal.pkanimalid', 'animal.animalidtag')
			->where("herd.fkpropertyid", "=",$propertyID)
			->get();
		echo json_encode($result);
	}
    /* UPDATE INFO OF COLLAR FUNTCION */
    public function collarVeiwInfoUpdate($collarSerial,Request $request){
    	$pkdeviceid = $this->getPkDeviceID($collarSerial);
    	$pkcollarid = $this->getPkcollarID($collarSerial);
    	//dd($_POST);

    	// TODO : versão como double - fazer validação
    	$rules = [
			'serialnumber' => 'required|unique:device,serialnumber,'.$pkdeviceid.',pkdeviceid',
			'version' => 'required',
			//'collaridnetwork' => 'required|unique:collar,collaridnetwork,'.$pkcollarid.',pkcollarid',
			'collaridnetwork' => 'unique:collar,collaridnetwork,'.$pkcollarid.',pkcollarid',
			'animalpublicid' => 'unique:animalcollar,fkanimalid,'.$pkcollarid.',fkcollarid',
	    ];

	    

	    $customMessages = [
	    	'serialnumber.required' => 'Numero de Série é obrigatorio!',
	    	'serialnumber.unique' => 'Numero de Série já existe!',

	    	'version.required' => 'A versão é obrigatoria',

	    	//'collaridnetwork.required'=> 'O ID de Rede é obrigatoria',
	    	'collaridnetwork.unique' => 'O ID de Rede já existe!',

	    	'animalpublicid.unique' => 'O Animal já tem uma coleira',
	   ];

	    //$this->validate($request, $rules, $customMessages);
	    
	   	$validator = Validator::make( $request->all(), $rules, $customMessages );
	    if ($validator->fails()) {
            /*return redirect('/collar/view/'.$collarSerial)->withErrors(['Error'=>"Occoreu um erro! Tente novamente"])
            	->withErrors($validator)
                ->withInput();*/       
            return response()->json(['status'=>'validation','error'=>$validator->errors()]);
        }

	    $_POST['propertydescription'] = (isset($_POST['propertydescription']) && $_POST['propertydescription'] != 0  ) ? $_POST['propertydescription'] : null;
	    $_POST['animalpublicid'] = (isset($_POST['animalpublicid'])  && $_POST['animalpublicid'] != 0 ) ? $_POST['animalpublicid'] : null;	    
	    
	    try{
			DB::beginTransaction();

			DB::table('collar')
            	->where('pkcollarid', $pkcollarid)
            	->update(['collaridnetwork' => $_POST['collaridnetwork']]);

            DB::table('device')
            	->where('pkdeviceid', $pkdeviceid)
            	->update(['serialnumber' => $_POST['serialnumber'] , 'fkpropertyid' => $_POST['propertydescription'], 'version' => $_POST['version']]);           
            
            DB::table('animalcollar')
            	->where('fkcollarid', $pkcollarid)
            	->update(['fkanimalid' => $_POST['animalpublicid']]);
            
            
			DB::commit();

			return response()->json([
			    'status' => 'sucesso',
			    'collarSerial' => $_POST['serialnumber']
			]);
			//return redirect('/collar/view/'.$_POST['serialnumber']);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			//return redirect('/collar/view/'.$collarSerial)->withErrors(['Error'=>"Occoreu um erro! Tente novamente"])->withInput();
			return response()->json(['status'=>'error','error'=>'Occoreu um erro! Tente novamente']);
		}	
    }



    /* DEBUG PAGE COLLAR */


    /**
     * [Creates the view of the collar in debug mode]
     * @param  [String] $collarSerial [Serial number of the collar]
     * @return [View] [returns the view of the collar page debug mode]
     */
    public function collarVeiwInfoDebug($collarSerial){
    	
    	return View::make('pages.collar.VeiwInfoDebug')->with(['collarSerial'=>$collarSerial]);
    }

    /**
     * [Functions loaded by ajax to get information for the debug datatable]
     * @param  [String] $collarSerial [Serial number of the collar]
     * @return [JSON] [returns a json with the information for the datatable]
     */
    public function collarVeiwInfoDebugTable($collarSerial){
    	
    	$collarID = DB::table('collar')
    		->select('collaridnetwork')
    		->join('device', 'device.pkdeviceid', '=', 'collar.fkdeviceid')
    		->where('device.serialnumber', '=',$collarSerial)
    		->get()
    		->toArray();

    	$collarID = $collarID[0]->collaridnetwork;

    	// datatable
    	$request=$_REQUEST;

    	$col =array(
	        0   =>  'id',
		);

    	$result = DB::table('rawdatacollar')
			->where('rawdatacollar.c_collarid', '=',$collarID)
			->orderBy('id','desc');
			//->limit(100);
			//->get();

		$totalData = $result->count("id");
    	$totalFilter =$totalData;

		$result = $result
	         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
	         	->offset($request['start'])
	         	->limit($request['length']);
		$result = $result->get();

		// datatable


		$data = array();

		foreach($result as $r){
			$temp = array();
		    $temp[] = $r->id;
	        $temp[] = $r->c_collarid;
	        $temp[] = $r->c_timestamp;
	        $temp[] = $r->c_battery;
	        $temp[] = $r->c_anglesdecx . ", ".$r->c_anglesdecy .", ".$r->c_anglesdecz;
	        $temp[] = $r->c_accelerometerx . ", ".$r->c_accelerometery .", ".$r->c_accelerometerz;
	        $temp[] = $r->c_d_accelerometery . ", ".$r->c_d_accelerometery .", ".$r->c_d_accelerometerz;
	        $temp[] = $r->c_magnetometerx . ", ".$r->c_magnetometery .", ".$r->c_magnetometerz;
	        $temp[] = $r->c_rcv_rssi_1 .", ".$r->c_rcv_rssi_2 .", ".$r->c_rcv_rssi_3 .", ".$r->c_rcv_rssi_4;
	        $temp[] = $r->c_rcv_rssi_id1 .", ".$r->c_rcv_rssi_id2 .", ".$r->c_rcv_rssi_id3 .", ".$r->c_rcv_rssi_id4;
	        $temp[] = $r->c_ultrasoundsdistance;
	        $temp[] = $r->c_steps;
	        $temp[] = $r->c_postureinfraction;
	        $temp[] = $r->c_fenceinfraction;
	        $temp[] = $r->c_propertyid;
	        $temp[] = $r->c_reportingbeacon;

	        $data[] = $temp;
		}
		// echo "<br><br>";
		// var_dump($sql);
		// exit;
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }





    /* COLlAR ON MAP */

    public function collarVeiwInfoMap($collarSerial){
    	return View::make('pages.collar.viewCollarOnMap')->with(['collarSerial'=>$collarSerial]);
    }


    /* TODO: otimizações - codigo repetido que pode-se colocar numa função! */

    public function collarVeiwInfoMapGet($collarSerial){  	

    	$propertyID = DB::table('device')
    		->select('fkpropertyid','pkcollarid')
    		->join('collar', 'device.pkdeviceid', '=', 'collar.fkdeviceid')
    		->where('serialnumber',$collarSerial)
    		->get();

    	$pkCollarID = $propertyID[0]->pkcollarid;
    	$propertyID = $propertyID[0]->fkpropertyid;


    	$collarNearestBeacon = DB::table('animalrecordcache')
	    	->select('nearestbeacon','animal.animalidtag')
	    	->join('animal', 'animalrecordcache.fkanimalid', '=', 'animal.pkanimalid')
	    	->join('animalcollar', 'animal.pkanimalid', '=', 'animalcollar.fkanimalid')
	    	->where('animalcollar.fkcollarid',$pkCollarID)
	    	->get();
	    
	    $animalidtag = $collarNearestBeacon[0]->animalidtag;

	    if($collarNearestBeacon->isEmpty()){
	    	$collarNearestBeacon= 0;
	    }
	    else{
	    	$collarNearestBeacon = (int) $collarNearestBeacon[0]->nearestbeacon; //casting to int because its a varchar in database
	    }
	    
	    $tempCoord=[];


		$result = DB::table('beacon')
			->select(DB::raw('pkbeaconid,gps[0] as lng , gps[1] as lat,serialnumber'))
			->join('device', 'beacon.fkdeviceid', '=', 'device.pkdeviceid')
			->where('device.fkpropertyid', '=', $propertyID)
			->orderBy('pkbeaconid','asc')
			->get()
			->toArray();


		$geojson = array();

		//property Points
    	
    	$area = array();

		$propertyQuery = DB::table('polygonpoint')
			->join('property', 'polygonpoint.fkpropertyid', '=', 'property.pkpropertyid')
			->select(DB::raw('pkpolygonpointid,gps[0] as lng , gps[1] as lat,property.description'))
			->where('polygonpoint.fkpropertyid', '=', $propertyID)
			->orderBy('pkpolygonpointid','asc')
			->get()
			->toArray();

		foreach($propertyQuery as $r){		
	        array_push($area, array($r->lng,$r->lat));
		}

		$propaty_area= array(
		    'type' => 'Feature',
		    'properties' => array(
		        'popupContent'=> $propertyQuery[0]->description
		    ),
		    'geometry' => array(
		        'type' => 'Polygon',
		        'coordinates' => array($area)
		    )
		);

		array_push($geojson, $propaty_area);


		
		

		foreach($result as $r){		
			$beaconNAnimal = DB::table('animalrecordcache')
				->where('nearestbeacon','=',$r->pkbeaconid)
				->count('fkanimalid');

	        $feature = array(
			        'type' => 'Feature',
			        'properties' => array(
						'popupContent'	=>	"<b>N. Beacon: ".$r->pkbeaconid."</b><br> N. Serie: ".$r->serialnumber."<br>"."Long: ".$r->lng." Lat: ". $r->lat."<br>"."N. Ovelhas:".$beaconNAnimal,
						'radius' => 11,
						'beaconID' =>$r->pkbeaconid
			        ), 
			        'geometry' => array(
			            'type' 			=>	'Point',
			            'coordinates' 	=>	array($r->lng,$r->lat),
			        )
			        
			        );
    		array_push($geojson, $feature);

    		if($r->pkbeaconid == $collarNearestBeacon){
    			$tempCoord =  array($r->lng,$r->lat);
    		}
	    }

	    if(empty($tempCoord))
	    	$tempCoord = array(-8.66002410,40.63418413);

	    //collar info
	    $feature = array(
	        'type' => 'Feature',
	        'properties' => array(
				'popupContent'	=>	"<b>NºSérie: ".$collarSerial."</b><br><b>Animal: ".$animalidtag."</b><br>",
				'serialNumber'	=>	$collarSerial
	        ), 
	        'geometry' => array(
	            'type'			=>	'Point',
	            'coordinates'	=>	$tempCoord,
	        )
	        
	        );
    		array_push($geojson, $feature);






	    //dd($geojson);exit;

	    //final
		$total = array(
		    'type' => 'FeatureCollection',
		    'features' => ($geojson)
		);

    	echo json_encode($total, JSON_NUMERIC_CHECK);
	}

	public function collarVeiwInfoMapUpdate($collarSerial){
		$propertyID = DB::table('device')
    		->select('fkpropertyid','pkcollarid')
    		->join('collar', 'device.pkdeviceid', '=', 'collar.fkdeviceid')
    		->where('serialnumber',$collarSerial)
    		->get();
    	$pkCollarID = $propertyID[0]->pkcollarid;
    	$propertyID = $propertyID[0]->fkpropertyid;

    	$json;

    	$result = DB::table('beacon')
			->select(DB::raw('pkbeaconid,gps[0] as lng , gps[1] as lat,serialnumber'))
			->join('device', 'beacon.fkdeviceid', '=', 'device.pkdeviceid')
			->where('device.fkpropertyid', '=', $propertyID)
			->orderBy('pkbeaconid','asc')
			->get()
			->toArray();

		$collarNearestBeacon = DB::table('animalrecordcache')
	    	->select('nearestbeacon','animal.animalidtag')
	    	->join('animal', 'animalrecordcache.fkanimalid', '=', 'animal.pkanimalid')
	    	->join('animalcollar', 'animal.pkanimalid', '=', 'animalcollar.fkanimalid')
	    	->where('animalcollar.fkcollarid',$pkCollarID)
	    	->get();

	    $animalidtag = $collarNearestBeacon[0]->animalidtag;

		if($collarNearestBeacon->isEmpty()){
	    	$collarNearestBeacon= 0;
	    }
	    else{
	    	$collarNearestBeacon = (int) $collarNearestBeacon[0]->nearestbeacon; //casting to int because its a varchar in database
	    }


	    $tempCoord = [];

		foreach($result as $r){		
			$beaconNAnimal = DB::table('animalrecordcache')
				->where('nearestbeacon','=',$r->pkbeaconid)
				->count('fkanimalid');

			$temp = array();
			$temp['id'] = $r->pkbeaconid;
			$temp['lng'] = $r->lng;
			$temp['lat'] = $r->lat;
			//$temp['popup'] = $beaconNAnimal;
			$temp['popup'] ="<b>N. Beacon: ".$r->pkbeaconid."</b><br> N. Serie: ".$r->serialnumber."<br>"."Long: ".$r->lng." Lat: ". $r->lat."<br>"."N. Ovelhas:".$beaconNAnimal;
			$temp['type'] = 'beacon';
			$json[] = $temp;

			if($r->pkbeaconid == $collarNearestBeacon){
    			$tempCoord =  array('lng'=>$r->lng,'lat'=>$r->lat);
    		}
		}

		if(empty($tempCoord))
	    	$tempCoord = array('lng' =>-8.66002410,'lat' =>40.63418413);

		$temp['lng'] = $tempCoord['lng'];
		$temp['lat'] = $tempCoord['lat'];
		//$temp['popup'] = $beaconNAnimal;
		$temp['popup'] ="<b>NºSérie: ".$collarSerial."</b><br><b>Animal: ".$animalidtag."</b><br>";
		$temp['type'] = 'collar';
		$json[] = $temp;



		echo json_encode($json);
	}



	/* PRIVATE FUNCTIONS */

	private function getPkDeviceID($deviceSerialNumber){
    	$temp = DB::table('device')
			->select('pkdeviceid')
			->where('serialnumber', '=', $deviceSerialNumber)
			->get();
    	return $temp[0]->pkdeviceid;
    }

    private function getPkcollarID($deviceSerialNumber){
    	$temp = DB::table('collar')
			->select('pkcollarid')
			->join('device','collar.fkdeviceid','=','device.pkdeviceid')
			->where('device.serialnumber', '=', $deviceSerialNumber)
			->get();
    	return $temp[0]->pkcollarid;
    }
}
