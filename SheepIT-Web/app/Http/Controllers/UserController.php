<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use DB;
use Validator;
use Hash;
use Image;

class UserController extends Controller
{
    public function index($userSearch = null){	
    	return View::make('pages.user.userList')->with(['userSearch' => $userSearch]);
    }

    public function userList(){
    	$request=$_REQUEST;
    		    
	    $col =array(
	        0   =>  'username',
	        1   =>  'name',
	        2   =>  'enterprisename',
		);

		$result = DB::table('users')
		->leftJoin('enterprise','users.fkenterpriseid','=','enterprise.pkenterpriseid')
		->select('pkuserid', 'name','surname','username','enterprisename')
		->distinct('pkuserid');
		
    	


    	//search
    	
		if(!empty($request['search']['value'])){
            $result = $result->whereRaw("(username::varchar ilike '".$request['search']['value']."%' OR name::varchar ilike '".$request['search']['value']."%' OR enterprisename::varchar ilike '".$request['search']['value']."%')");	     
		}
		$totalData = $result->count("pkuserid");
    	$totalFilter =$totalData;


		//order
		$result = $result
		         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
		         	->offset($request['start'])
		         	->limit($request['length']);
		$result = $result->get();



		$data = array();

		foreach($result as $r){		
			$temp = array();
		    $temp[] = $r->username;
	        $temp[] = $r->name ." ". $r->surname;
	        $temp[] = $r->enterprisename;
	        $temp[] = '<a href="'.url('/user/view//').'/'.$r->pkuserid.'" class="btn btn-primary btn-xs">Ver <i class="fa fa-eye">&nbsp;</i></a>';
	        $data[] = $temp;
		}
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }


    /* NEW USER*/
    public function newUser(){
		return View::make('pages.user.newUser');
	}

	public function registerUser(Request $request){
		//dd($request);exit;
		$rules = [
			'nameInput' 	=> 	'required',
			'surnameInput' 	=> 	'required',
			'emailInput' 	=> 	'required|email||unique:users,email',
			'usernameInput' => 	'required|unique:users,username',
			'passwordInput' => 	'required|min: 7',
			'photoInput'	=>	'image|mimes:jpeg,bmp,png|max:5120|dimensions:max_width=100,max_height=100',
	    ];

	    $customMessages = [
	    	'nameInput.required' 	=> 'O Primeiro Nome é obrigatorio!',
	    	'surnameInput.required' => 'O Último Nome é obrigatorio!',

	    	'emailInput.required' 	=> 'O Email é obrigatorio!',
	    	'emailInput.email'		=> 'O Email não esta no formato correto!',
	    	'emailInput.unique'		=> 'O Email já existe!',

	    	'usernameInput.required'=> 'O Nome de Utilizador é obrigatorio!',
	    	'usernameInput.unique'	=> 'O Nome de Utilizador já existe!',

	    	'passwordInput.required'=> 'A Password é obrigatoria',
	    	'passwordInput.min'=> 'A Password temd e ter no mínimo :min caracteres',

	    	'photoInput.image'		=> 'Por Favor selecione uma Foto válida',
	    	'photoInput.mimes'		=> ' A Foto não tem uma extenção válida',
	    	'photoInput.size'		=> 'A Foto é grande de mais',
	    	'photoInput.dimensions'	=> 'A Foto tem de ser no máximo :max_width x :max_height',

	   ];

	    $this->validate($request, $rules, $customMessages);
	    $passwordHash = Hash::make($_POST['passwordInput']);

	    try{
			DB::beginTransaction();

			$id = DB::table('users')->insertGetId(
				[
					'name' => $_POST['nameInput'],
					'surname' => $_POST['surnameInput'],
					'email' => $_POST['emailInput'],
					'username' => $_POST['usernameInput'],
					'password' => $passwordHash,
					'fkusertypeid' => $_POST['UserTypeSelect'],
					'fkenterpriseid' => $_POST['ClientSelect'],
				] ,'pkuserid'
			);

			DB::commit();

		    if ($request->hasFile('photoInput')) {
			    $img = Image::make($request->file('photoInput'))->encode('jpg', 80);
			    $imageName = $id. '.jpg';
			   	$img->save(
			        base_path() . '/public/img/user/'. $imageName
			    );
			}

			return redirect('/user/view/'.$id);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return redirect('/user/new')->withErrors(['Error'=>"Occoreu um erro! Tente novamente"])->withInput();
		}	
	}

    public function clienteList(){
		$result = DB::table("enterprise")
			->select('pkenterpriseid','enterprisename')
			->get();
		echo json_encode($result);
	}
	public function UserTypeList(){
		$result = DB::table("usertype")
			->select('pkusertypeid','type')
			->get();
		echo json_encode($result);
	}


	/* VIEW USER */
	public function userVeiwInfo($userid){
    	//dd($collarSerial);exit;
    	$result = DB::table('users')
    		->leftJoin('enterprise','users.fkenterpriseid','=','enterprise.pkenterpriseid')
    		->leftJoin('usertype','users.fkusertypeid','=','usertype.pkusertypeid')
			->select('users.pkuserid','users.name','users.surname','users.email','users.username','users.fkenterpriseid','enterprisename as clientname','users.fkusertypeid','type as userType')
			->where('users.pkuserid', '=', $userid)
			->limit(1)
			->get()
			->toArray();

    	return View::make('pages.user.VeiwInfo')->with(['result' => $result]);
    }

    /* UPDATE USER */
    public function userVeiwInfoUpdate($userid,Request $request){
    	//dd($_POST);exit;
    	$rules = [
			'nameInput' 	=> 	'required',
			'surnameInput' 	=> 	'required',
			'emailInput' 	=> 	'required|email||unique:users,email,'.$userid.',pkuserid',
			'usernameInput' => 	'required|unique:users,username,'.$userid.',pkuserid',
			'photoInput'	=>	'image|mimes:jpeg,bmp,png|max:5120|dimensions:max_width=100,max_height=100',
	    ];

	    $customMessages = [
	    	'nameInput.required' 	=> 'O Primeiro Nome é obrigatorio!',
	    	'surnameInput.required' => 'O Último Nome é obrigatorio!',

	    	'emailInput.required' 	=> 'O Email é obrigatorio!',
	    	'emailInput.email'		=> 'O Email não esta no formato correto!',
	    	'emailInput.unique'		=> 'O Email já existe!',

	    	'usernameInput.required'=> 'O Nome de Utilizador é obrigatorio!',
	    	'usernameInput.unique'	=> 'O Nome de Utilizador já existe!',

	    	'photoInput.image'		=> 'Por Favor selecione uma Foto válida',
	    	'photoInput.mimes'		=> ' A Foto não tem uma extenção válida',
	    	'photoInput.size'		=> 'A Foto é grande de mais',
	    	'photoInput.dimensions'	=> 'A Foto tem de ser no máximo :max_width x :max_height',

	   ];
	    
	   	$validator = Validator::make( $request->all(), $rules, $customMessages );
	    if ($validator->fails()) {   
            return response()->json(['status'=>'validation','error'=>$validator->errors()]);
        }
	    
	    try{
			DB::beginTransaction();

            DB::table('users')
            	->where('pkuserid', $userid)
            	->update([
            		'name' => $_POST['nameInput'],
					'surname' => $_POST['surnameInput'],
					'email' => $_POST['emailInput'],
					'username' => $_POST['usernameInput'],
					'fkusertypeid' => $_POST['UserType'],
					'fkenterpriseid' => $_POST['clientname'],
            	]);

            if ($request->hasFile('photoInput')) {
			    $img = Image::make($request->file('photoInput'))->encode('jpg', 80);
			    $imageName = $userid. '.jpg';
			   	$img->save(
			        base_path() . '/public/img/user/'. $imageName
			    );
			}            
            
			DB::commit();

			return response()->json([
			    'status' => 'sucesso',
			    'userid' => $userid
			]);
			//return redirect('/collar/view/'.$_POST['serialnumber']);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			//return redirect('/collar/view/'.$collarSerial)->withErrors(['Error'=>"Occoreu um erro! Tente novamente"])->withInput();
			return response()->json(['status'=>'error','error'=>'Occoreu um erro! Tente novamente']);
		}	
    }

}
