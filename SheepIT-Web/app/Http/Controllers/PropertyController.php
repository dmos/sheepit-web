<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use View;
use DB;
use Validator;

class PropertyController extends Controller
{
    public function index($propertySearch = null){
        return View::make('pages.property.propertyList')->with(['propertySearch' => $propertySearch]);
    }

    public function propertyList(){

    	$request=$_REQUEST;
    		    
	    $col =array(
	        0   =>  'pkpropertyid',
	        1   =>  'description',
	        2   =>  'enterprisename',
	        3   =>  'type',
	        4	=> 	'locality',
		);  //create column like table in database //create filter animal get animal

		$result = DB::table('property')
			->join('enterprise', 'property.fkenterpriseid', '=', 'enterprise.pkenterpriseid')
			->leftJoin('address','property.fkaddressid','=','address.pkaddressid')
		->select('pkpropertyid','type','description','locality','enterprisename')
		->distinct('pkpropertyid');
		
    	


    	//search
    	
		if(!empty($request['search']['value'])){
            $result = $result->whereRaw("(
            	pkpropertyid::varchar ilike '".$request['search']['value']."%' OR 
            	description::varchar ilike '".$request['search']['value']."%' OR
            	enterprisename::varchar ilike '".$request['search']['value']."%' OR
            	type::varchar ilike '".$request['search']['value']."%' OR
            	locality::varchar ilike '".$request['search']['value']."%')");

            // var_dump($result->toSql());
            // exit;
		}

		$totalData = $result->count("pkpropertyid");
    	$totalFilter =$totalData;

		//order
		$result = $result
		         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
		         	->offset($request['start'])
		         	->limit($request['length']);
		$result = $result->get();



		$data = array();

		foreach($result as $r){		
			$temp = array();
		    $temp[] = $r->pkpropertyid;
	        $temp[] = $r->description;
	        $temp[] = $r->enterprisename;
	        $temp[] = $r->type;
	        $temp[] = $r->locality;
	        $temp[] = '<a href="'.url('/property/view/').'/'.$r->pkpropertyid.'" class="btn btn-primary btn-xs">Ver <i class="fa fa-eye">&nbsp;</i></a>';
	        $data[] = $temp;
		}
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }


 	/* Create PROPERTY */
    public function newProperty(){
		return View::make('pages.property.newProperty');
	}

	public function registerProperty(Request $request){
		$coord = json_decode($_POST['coordentsInput']);
		$request['coordentsInput'] = empty($coord)?"": $request['coordentsInput'];

		$rules = [
			'descriptionInput'	=> 	'required|max:250',
			'typeInput'			=> 	'required',

			'coordentsInput'	=>	'required',

			'countryInput'		=>	'required',
			'districtInput'		=>	'required',
			'localityInput'		=>	'required',
			//'zipcodeInput'		=>	'unique:address,zipcode',
	    ];

	    $customMessages = [
	    	'descriptionInput.required' =>	'A descrição é obrigatoria!',
	    	'descriptionInput.max' 		=>	'A descrição não pode exeder :max caracteres!',

	    	'typeInput.required'		=>	'O tipo de propriedade é obrigatorio!',

	    	'coordentsInput.required' 	=>	'A área da propriedade É Obrigatoria!',

	    	'countryInput.required' 	=>	'O país é obrigatorio!',
	    	'districtInput.required' 	=> 	'O distrito é obrigatorio!',
	    	'localityInput.required'	=>	'A localidade é obrigatoria!',
	    	'zipcodeInput.unique'		=>	'O código postal já esta registado!'
	   ];

	    $this->validate($request, $rules, $customMessages);
	    
	    try{
			DB::beginTransaction();
			$addressID = DB::table('address')->insertGetId([
				'country' => $_POST['countryInput'],
				'district' => $_POST['districtInput'],
				'locality'=> $_POST['localityInput'],
				'street'=> $_POST['streetInput'],
				'zipcode'=> $_POST['zipcodeInput'],
			],'pkaddressid');

			$propertyID = DB::table('property')->insertGetId([
				'type' => $_POST['typeInput'],
				'description' => $_POST['descriptionInput'],
				'fkenterpriseid'=> $_POST['ClientSelect'],
				'fkaddressid'=> $addressID,
			],'pkpropertyid');

			
			foreach($coord as $c){
	    		DB::table('polygonpoint')->insert([
	    			'gps' => '('.$c[0].','.$c[1].')',
	    			'fkpropertyid' => $propertyID
	    		]);
	    	}

			DB::commit();
			return redirect('/property/view/'.$propertyID);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return redirect('/property/new')->withErrors(['Error'=>"Occoreu um erro! Tente novamente"])->withInput();
		}	
	}

    public function clienteList(){
		$result = DB::table("enterprise")
			->select('pkenterpriseid','enterprisename')
			->get();
		echo json_encode($result);
	}
   


	/* VIEW PROPERTY */
    public function propertyVeiwInfo($propertyID){
    	$result = DB::table('property')
			->join('enterprise', 'property.fkenterpriseid', '=', 'enterprise.pkenterpriseid')
			->leftJoin('address','property.fkaddressid','=','address.pkaddressid')
			->where('property.pkpropertyid', '=', $propertyID)
			->get()
			->toArray();
		// 	->toSql();
		//dd($result); exit;
        return View::make('pages.property.VeiwInfo')->with(['result' => $result]);
    }

    	//MAP on VIEW PROPERTY PAGE
    public function propertyViewMap($propertyID){

    	/**
    	 * TODO : 
    	 * if no property polygon exists.. gives error on: $result[0]->description
    	 */

    	$geojson = array();

		$area = array();

		$result = DB::table('polygonpoint')
			->join('property', 'polygonpoint.fkpropertyid', '=', 'property.pkpropertyid')
			->select(DB::raw('pkpolygonpointid,gps[0] as lng , gps[1] as lat,property.description'))
			->where('polygonpoint.fkpropertyid', '=', $propertyID)
			->orderBy('pkpolygonpointid','asc')
			->get()
			->toArray();

		foreach($result as $r){		
	        array_push($area, array($r->lng,$r->lat));
		}

		$propaty_area= array(
		    'type' => 'Feature',
		    'properties' => array(
		        'popupContent'=> $result[0]->description
		    ),
		    'geometry' => array(
		        'type' => 'Polygon',
		        'coordinates' => array($area)
		    )
		);

		array_push($geojson, $propaty_area);


		//beacons
		$result = DB::table('beacon')
			->select(DB::raw('pkbeaconid,gps[0] as lng , gps[1] as lat,serialnumber'))
			->join('device', 'beacon.fkdeviceid', '=', 'device.pkdeviceid')
			->where('device.fkpropertyid', '=', $propertyID)
			->orderBy('pkbeaconid','asc')
			->get()
			->toArray();

		foreach($result as $r){		
			if($r->lng==200 && $r->lat==200)
				continue;

			$beaconNAnimal = DB::table('animalrecordcache')
				->where('nearestbeacon','=',$r->pkbeaconid)
				->count('fkanimalid');

	        $feature = array(
			        'type' => 'Feature',
			        'properties' => array(
						'popupContent'=> "<b>N. Beacon: ".$r->pkbeaconid."</b><br> N. Serie: ".$r->serialnumber."<br>"."Long: ".$r->lng." Lat: ". $r->lat."<br>"."N. Ovelhas:".$beaconNAnimal,
						'marker-color'=> '#d23c3c',
						'marker-size'=> 'large',
						'marker-symbol'=> 'star',
						'beaconID' =>$r->pkbeaconid
			        ), 
			        'geometry' => array(
			            'type' => 'Point',
			            'coordinates' => array($r->lng,$r->lat)
			        )
			        
			        );
	    	array_push($geojson, $feature);
		}


		//final
		$total = array(
		    'type' => 'FeatureCollection',
		    'features' => ($geojson)
		);

    	echo json_encode($total, JSON_NUMERIC_CHECK);
    }

    public function mapBeaconInfoUpdate($propertyID){
    	$json=[];

    	$result = DB::table('beacon')
			->select(DB::raw('pkbeaconid,gps[0] as lng , gps[1] as lat,serialnumber'))
			->join('device', 'beacon.fkdeviceid', '=', 'device.pkdeviceid')
			->where('device.fkpropertyid', '=', $propertyID)
			->orderBy('pkbeaconid','asc')
			->get()
			->toArray();

		foreach($result as $r){		
			if($r->lng==200 && $r->lat==200)
				continue;

			$beaconNAnimal = DB::table('animalrecordcache')
				->where('nearestbeacon','=',$r->pkbeaconid)
				->count('fkanimalid');

			$temp = array();
			$temp['id'] = $r->pkbeaconid;
			$temp['lng'] = $r->lng;
			$temp['lat'] = $r->lat;
			//$temp['popup'] = $beaconNAnimal;
			$temp['popup'] ="<b>N. Beacon: ".$r->pkbeaconid."</b><br> N. Serie: ".$r->serialnumber."<br>"."Long: ".$r->lng." Lat: ". $r->lat."<br>"."N. Ovelhas:".$beaconNAnimal;
			$json[] = $temp;
		}


		echo json_encode($json);
    }

    /* update functions */
    public function propertyVeiwInfoUpdate($propertyID,Request $request){
    	$rules = [
			'descriptionInput'	=> 	'required|max:250',
			'typeInput'			=> 	'required',
	    ];

	    $customMessages = [
	    	'descriptionInput.required' =>	'A descrição é obrigatoria!',
	    	'descriptionInput.max' 		=>	'A descrição não pode exeder :max caracteres!',

	    	'typeInput.required'		=>	'O tipo de propriedade é obrigatorio!',
	  	];
	    
	   	$validator = Validator::make( $request->all(), $rules, $customMessages );

	    if ($validator->fails()) {
            return response()->json(['status'=>'validation','error'=>$validator->errors()]);
        }    
	    
	    try{
			DB::beginTransaction();

			DB::table('property')
            	->where('pkpropertyid', $propertyID)
            	->update([
            		'type' => $_POST['typeInput'],
					'description' => $_POST['descriptionInput'],
					'fkenterpriseid'=> $_POST['clientname'],
            	]);       
            
			DB::commit();

			return response()->json([
			    'status' => 'sucesso',
			    'pkpropertyid' => $propertyID
			]);
		} 
		catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return response()->json(['status'=>'error','error'=>'Occoreu um erro! Tente novamente']);
		}	
    }

    public function propertyVeiwInfoUpdateAddress($propertyID,Request $request){
    	$FKAddressID = $this->getFKAddressID($propertyID);

    	$rules = [
			'countryInput'		=>	'required',
			'districtInput'		=>	'required',
			'localityInput'		=>	'required',

			'zipcodeInput'		=>	'unique:address,zipcode,'.$FKAddressID.',pkaddressid',
	    ];

	    $customMessages = [
	    	'countryInput.required' 	=>	'O país é obrigatorio!',
	    	'districtInput.required' 	=> 	'O distrito é obrigatorio!',
	    	'localityInput.required'	=>	'A localidade é obrigatoria!',

	    	'zipcodeInput.unique'		=>	'O código postal já esta registado!'
	  	];
	    
	   	$validator = Validator::make( $request->all(), $rules, $customMessages );

	    if ($validator->fails()) {
            return response()->json(['status'=>'validation','error'=>$validator->errors()]);
        }    
	    
	    try{
			DB::beginTransaction();

			DB::table('address')
            	->where('pkaddressid', $FKAddressID)
            	->update([
            		'country' => $_POST['countryInput'],
					'district' => $_POST['districtInput'],
					'locality'=> $_POST['localityInput'],
					'street'=> $_POST['streetInput'],
					'zipcode'=> $_POST['zipcodeInput'],
            	]);       

			DB::commit();

			return response()->json([
			    'status' => 'sucesso',
			    'pkpropertyid' => $propertyID
			]);
		} 
		catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return response()->json(['status'=>'error','error'=>'Occoreu um erro! Tente novamente']);
		}	
    }

    public function updateCoord($propertyID){
    	$coord = $_REQUEST['coord'];
    	
    	try{
			DB::beginTransaction();

			DB::table('polygonpoint')->where('fkpropertyid', '=', $propertyID)->delete();

			foreach($coord as $c){
	    		DB::table('polygonpoint')->insert([
	    			'gps' => '('.$c[0].','.$c[1].')',
	    			'fkpropertyid' => $propertyID
	    		]);
	    	}

			DB::commit();
		}
		catch(\Exception $e){
			DB::rollback();
			echo  "ERROR: <br>".$e->getMessage();
		}	
    }




    /* PRIVATE FUNCTIONS */
    private function getFKAddressID($propertyID){
    	$temp = DB::table('property')
			->select('fkaddressid')
			->where('pkpropertyid', '=', $propertyID)
			->get();
    	return $temp[0]->fkaddressid;
    }
}
