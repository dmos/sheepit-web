<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use DB;
use Validator;

class HerdController extends Controller
{
    public function index($herdSearch = null){
    	return View::make('pages.herd.herdList')->with(['herdSearch' => $herdSearch]);
    }

    public function herdList(){
    	$request=$_REQUEST;
    		    
	    $col =array(
	        0   =>  'name',
	        1   =>  'enterprisename',
	        2   =>  'nanimals',
		);

		$result = DB::table('herd')
			->leftJoin('property', 'herd.fkpropertyid', '=', 'property.pkpropertyid')
			->leftJoin('enterprise','property.fkenterpriseid','=','enterprise.pkenterpriseid')
			->leftJoin('animal','herd.pkherdid','=','animal.fkherdid')
		->select('herd.name','herd.pkherdid','enterprisename', DB::raw('count(animal.pkanimalid) as nanimals'))
		->distinct('herd.name')
		->groupBy('herd.name','herd.pkherdid','enterprisename');
		
    	$totalData = $result->count();
    	$totalFilter =$totalData;


    	//search
    	
		if(!empty($request['search']['value'])){
            $result = $result->whereRaw("(herd.name::varchar ilike '".$request['search']['value']."%' OR enterprisename::varchar ilike '".$request['search']['value']."%')");

            // var_dump($result->toSql());
            // exit;

	        $totalData=$result->count("herd.name");
		}

		//order
		$result = $result
		         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
		         	->offset($request['start'])
		         	->limit($request['length']);
		$result = $result->get();



		$data = array();
		foreach($result as $r){		
			$temp = array();
		    $temp[] = $r->name;
	        $temp[] = $r->enterprisename;
	        $temp[] = $r->nanimals;
	        $temp[] = '<a href="'.url('/herd/view//').'/'.$r->pkherdid.'" class="btn btn-primary btn-xs">Ver <i class="fa fa-eye">&nbsp;</i></a>';
	        $data[] = $temp;
		}
		// echo "<br><br>";
		// var_dump($sql);
		// exit;
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }


    /* Create Herd */
    public function newHerd(){
		return View::make('pages.herd.newHerd');
	}

	public function registerHerd(Request $request){
		//dd($request);exit;
		$rules = [
			'herdNameInput' => 'required|unique:herd,name',
	    ];

	    $customMessages = [
	    	'herdNameInput.required' => 'O nome do rebanho é obrigatorio!',
	    	'herdNameInput.unique' => 'O nome do rebanho já esta a ser utilizado!',
	   ];

	    $this->validate($request, $rules, $customMessages);
	    
	    $PropertySelect = (isset($_POST['PropertySelect']) && $_POST['PropertySelect'] != 0  ) ? $_POST['PropertySelect'] : null;
	    
	    
	    try{
			DB::beginTransaction();
			$id = DB::table('herd')->insertGetId(
			    ['name'=>$_POST['herdNameInput'],'fkpropertyid' => $PropertySelect] , 
			    'pkherdid'
			);


			DB::commit();
			return redirect('/herd/view/'.$id);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return redirect('/herd/new')->withErrors(['Error'=>"Occoreu um erro! Tente novamente"])->withInput();
		}	
	}

    public function clienteList(){
		$result = DB::table("enterprise")
			->select('pkenterpriseid','enterprisename')
			->get();
		echo json_encode($result);
	}

	public function propertyList($clientID){
		$result = DB::table("property")
			->select('pkpropertyid', 'description')
			->where("property.fkenterpriseid", "=",$clientID)
			->get();
		echo json_encode($result);
	}


	/* HERD INFO PAGE */
	public function herdVeiwInfo($herdID){
    	//dd($collarSerial);exit;
    	$result = DB::table('herd')
			->leftJoin('property', 'property.pkpropertyid', '=', 'herd.fkpropertyid')
			->leftJoin('enterprise', 'enterprise.pkenterpriseid', '=', 'property.fkenterpriseid')
			->leftJoin('animal','herd.pkherdid','=','animal.fkherdid')
			->select('herd.pkherdid','herd.name','enterprise.pkenterpriseid','enterprise.enterprisename as clientname','property.description as propertydescription', 'property.pkpropertyid',DB::raw('count(animal.pkanimalid) as nanimals'))
			->where('herd.pkherdid', '=', $herdID)
			->groupBy('herd.name','herd.pkherdid','pkenterpriseid','clientname','propertydescription','pkpropertyid')
			->limit(1)
			->get()
			->toArray();

    	return View::make('pages.herd.VeiwInfo')->with(['result' => $result]);
    }

    public function animalList($herdID){
    	$request=$_REQUEST;
    
	    $col =array(
	        0   =>  'animalidtag',
	        1   =>  'birthdata',
	        2   =>  'weight',
	        3   =>  'gender',
	        4	=> 	'breed',
	        5	=>	'Asso_Collar',
		);  //create column like table in database //create filter animal get animal

		$result = DB::table('animal')
			->leftJoin('animalcollar','animal.pkanimalid','=','animalcollar.fkanimalid')
			->leftJoin('collar','animalcollar.fkcollarid','=','collar.pkcollarid')
			->leftJoin('device','collar.fkdeviceid','=','device.pkdeviceid')
		->select('animalidtag', 'birthdata','weight','gender','breed', 'device.serialnumber as Asso_Collar','fkherdid')
		->where('animal.fkherdid','=',$herdID)
		->distinct('animalidtag');
		

    	//search
    	
		if(!empty($request['search']['value'])){
            $result = $result->whereRaw("(animalidtag::varchar ilike '".$request['search']['value']."%' 
            							OR birthdata::varchar ilike '".$request['search']['value']."%'
            							OR weight::varchar ilike '".$request['search']['value']."%' 
            							OR breed::varchar ilike '".$request['search']['value']."%'   
            							OR fkcollarid::varchar ilike '".$request['search']['value']."%')");

            // var_dump($result->toSql());
            // exit;
		}

		$totalData = $result->count("animalidtag");
    	$totalFilter =$totalData;

		//order
		$result = $result
		         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
		         	->offset($request['start'])
		         	->limit($request['length']);
		         	
		$result = $result->get();

		$data = array();

		foreach($result as $r){		
			$temp = array();
		    $temp[] = $r->animalidtag;
	        $temp[] = $r->birthdata;
	        $temp[] = $r->weight;
	        $temp[] = $r->gender;
	        $temp[] = $r->breed;
	        $temp[] = $r->Asso_Collar;
	        $temp[] = '<a href="'.url('/animal/view').'/'.$r->animalidtag.'" class="btn btn-primary btn-xs">Ver <i class="fa fa-eye">&nbsp;</i></a>';
	        $data[] = $temp;
		}
		// echo "<br><br>";
		// var_dump($sql);
		// exit;
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }

    public function animalListAdd($herdID){
    	$fkpropertyid = $this->getFkPropertyID($herdID);

    	//TODO : pode ser util para a pagina das coleiras- animal associado
		$result = DB::table('animal')
			->leftJoin('animalcollar','animal.pkanimalid','=','animalcollar.fkanimalid')
			->leftJoin('collar','animalcollar.fkcollarid','=','collar.pkcollarid')
			->leftJoin('device','collar.fkdeviceid','=','device.pkdeviceid')
		->select('animalidtag','pkanimalid','device.serialnumber as Asso_Collar')
		->whereNull('animal.fkherdid')
		->whereRaw('CASE WHEN  animalcollar.fkcollarid is not null THEN 
						device.fkpropertyid = '.$fkpropertyid.' and fkherdid is null
					ELSE
						fkherdid is null
					END'
		)
		->distinct('animalidtag')
		->orderBy('animalidtag')
		->get();
		echo json_encode($result);
    }

    public function addAnimal($herdID){
    	try{
			DB::beginTransaction();

            DB::table('animal')
            	->where('pkanimalid', $_POST['Animal'])
            	->update([
            		'fkherdid' => $herdID
            	]);            
            
			DB::commit();

			return response()->json([
			    'status' => 'sucesso',
			]);

		} 
		catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return response()->json(['status'=>'error','error'=>'Occoreu um erro! Tente novamente']);
		}	

    }


    /* UPDATE INFO OF HERD FUNTCION */
    public function herdVeiwInfoUpdate($herdID,Request $request){
    	// dd($_POST);exit;

    	$rules = [
			'herdName' => 'required|unique:herd,name,'.$herdID.',pkherdid',
	    ];

	    $customMessages = [
	    	'herdName.required' => 'O nome do rebanho é obrigatorio!',
	    	'herdName.unique' => 'O nome do rebanho já esta a ser utilizado!',
	   ];
	    
	   	$validator = Validator::make( $request->all(), $rules, $customMessages );
	    if ($validator->fails()) {   
            return response()->json(['status'=>'validation','error'=>$validator->errors()]);
        }

	    $_POST['propertydescription'] = (isset($_POST['propertydescription']) && $_POST['propertydescription'] != 0  ) ? $_POST['propertydescription'] : null;   
	    
	    try{
			DB::beginTransaction();

            DB::table('herd')
            	->where('pkherdid', $herdID)
            	->update([
            		'name' => $_POST['herdName'] ,
            		'fkpropertyid' => $_POST['propertydescription']
            	]);            
            
			DB::commit();

			return response()->json([
			    'status' => 'sucesso',
			    'pkherdid' => $herdID
			]);
			//return redirect('/collar/view/'.$_POST['serialnumber']);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			//return redirect('/collar/view/'.$collarSerial)->withErrors(['Error'=>"Occoreu um erro! Tente novamente"])->withInput();
			return response()->json(['status'=>'error','error'=>'Occoreu um erro! Tente novamente']);
		}	
    }




    /* PRIVATE FUNCTIONS */
    private function getFkPropertyID($herdID){
    	$temp = DB::table('herd')
			->select('fkpropertyid')
			->where('herd.pkherdid', '=', $herdID)
			->get();
    	return $temp[0]->fkpropertyid;
    }
}
