<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use DB;
use Validator;
use Image;

class ClientController extends Controller
{
    public function index($clientSearch = null){	
    	return View::make('pages.client.clientList')->with(['clientSearch' => $clientSearch]);
    }

    public function clientList(){
    	$request=$_REQUEST;
    		    
	    $col =array(
	        0   =>  'enterprisename',
	        1   =>  'nameresponsible',
	        2   =>  'district',
	        3   =>  'type',
		);

		$result = DB::table('enterprise')
		->leftJoin('address','enterprise.fkaddressid','=','address.pkaddressid')
		->leftJoin('enterprisetype','enterprise.fkenterprisetypeid','=','enterprisetype.pkenterprisetypeid')
		->select('pkenterprisetypeid', 'enterprisename', 'nameresponsible', 'surnameresponsible', 'address.district', 'enterprisetype.type')
		->distinct('pkenterprisetypeid');

    	//search
    	
		if(!empty($request['search']['value'])){
            $result = $result->whereRaw("
            		(enterprisename::varchar ilike '".$request['search']['value']."%' 
					OR nameresponsible::varchar ilike '".$request['search']['value']."%'
					OR district::varchar ilike '".$request['search']['value']."%'
					OR type::varchar ilike '".$request['search']['value']."%')");	     
		}
		$totalData = $result->count();
    	$totalFilter =$totalData;


		//order
		$result = $result
	         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
	         	->offset($request['start'])
	         	->limit($request['length']);
		$result = $result->get();



		$data = array();
		foreach($result as $r){		
			$temp = array();
		    $temp[] = $r->enterprisename;
	        $temp[] = $r->nameresponsible ." ". $r->surnameresponsible;
	        $temp[] = $r->district;
	        $temp[] = $r->type;
	        $temp[] = '<a href="'.url('/client/view//').'/'.$r->pkenterprisetypeid.'" class="btn btn-primary btn-xs">Ver <i class="fa fa-eye">&nbsp;</i></a>';
	        $data[] = $temp;
		}
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }

    /* new CLIENT */
    public function newClient(){
		return View::make('pages.client.newClient');
	}

	public function registerClient(Request $request){
		//dd($request);exit;
		$rules = [
			'enterprisenameInput' 	=> 	'required',
			'emailInput' 			=> 	'required|email||unique:enterprise,email',
			'taxnumberInput'		=>	'required|max:9|min:9',
			'cellphoneInput'		=>	'required|digits_between:9,12',
			'photoInput'			=>	'image|mimes:jpeg,bmp,png|max:5120|dimensions:max_width=500,max_height=500',

			'countryInput'		=>	'required',
			'districtInput'		=>	'required',
			'localityInput'		=>	'required',

			//'zipcodeInput'		=>	'unique:address,zipcode',
	    ];

	    $customMessages = [
	    	'enterprisenameInput.required' 	=> 'O Nome do Cliente é obrigatorio!',

	    	'emailInput.required' 		=> 'O Email é obrigatorio!',
	    	'emailInput.email'			=> 'O Email não esta no formato correto!',
	    	'emailInput.unique'			=> 'O Email já existe!',

	    	'taxnumberInput.required'	=> 'O Nºfiscal é obrigatorio!',
	    	'taxnumberInput.max'		=> 'O Nºfiscal não pode ser maior que :max!',
	    	'taxnumberInput.min'		=> 'O Nºfiscal não pode ser menor que :min!',

	    	'cellphoneInput.required'	=> 'O Nºtelefone é obrigatorio!',
	    	'cellphoneInput.digits_between'		=> 'O Nºtelefone tem de ser entre :min and :max digits',

	    	'photoInput.image'			=> 'Por Favor selecione uma Foto válida',
	    	'photoInput.mimes'			=> ' A Foto não tem uma extenção válida',
	    	'photoInput.size'			=> 'A Foto é grande de mais',
	    	'photoInput.dimensions'		=> 'A Foto tem de ser no máximo :max_width x :max_height',

	    	'countryInput.required' 	=>	'O país é obrigatorio!',
	    	'districtInput.required' 	=> 	'O distrito é obrigatorio!',
	    	'localityInput.required'	=>	'A localidade é obrigatoria!',

	    	'zipcodeInput.unique'		=>	'O código postal já esta registado!'
	   ];

	    $this->validate($request, $rules, $customMessages);

	    try{
			DB::beginTransaction();

			$addressID = DB::table('address')->insertGetId([
				'country' => $_POST['countryInput'],
				'district' => $_POST['districtInput'],
				'locality'=> $_POST['localityInput'],
				'street'=> $_POST['streetInput'],
				'zipcode'=> $_POST['zipcodeInput'],
			],'pkaddressid');

			// seguir
			$id = DB::table('enterprise')->insertGetId(
				[
					'enterprisename' => $_POST['enterprisenameInput'],
					'nameresponsible' => $_POST['nameresponsibleInput'],
					'surnameresponsible' => $_POST['surnameresponsibleInput'],
					'cellphone' => $_POST['cellphoneInput'],
					'email' => $_POST['emailInput'],
					'taxnumber' => $_POST['taxnumberInput'],
					'fkaddressid'=> $addressID,
					'fkenterprisetypeid' => $_POST['ClientTypeSelect']
				] ,'pkenterpriseid'
			);

			DB::commit();

		    if ($request->hasFile('photoInput')) {
			    $img = Image::make($request->file('photoInput'))->encode('jpg', 80);
			    $imageName = $id. '.jpg';
			   	$img->save(
			        base_path() . '/public/img/enterprise/'. $imageName
			    );
			}

			return redirect('/client/view/'.$id);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return redirect('/client/new')->withErrors(['Error'=>'Occoreu um erro! Tente novamente'])->withInput();
		}	
	}

    public function clientTypeList(){
    	$result = DB::table("enterprisetype")
			->select('pkenterprisetypeid','type')
			->get();
		echo json_encode($result);
    }


    /* VIEW CLIENT */
	public function clientVeiwInfo($clientID){
    	$result = DB::table('enterprise')
			->leftJoin('address','enterprise.fkaddressid','=','address.pkaddressid')
			->leftJoin('enterprisetype','enterprise.fkenterprisetypeid','=','enterprisetype.pkenterprisetypeid')
			->select('pkenterpriseid', 'enterprisename', 'nameresponsible', 'surnameresponsible', 'cellphone', 'email', 'taxnumber', 'fkenterprisetypeid', 'address.country', 'address.district', 'address.locality', 'address.street', 'address.zipcode', 'enterprisetype.type')
			->distinct('pkenterpriseid')
			->where('pkenterpriseid', '=', $clientID)
			->limit(1)
			->get()
			->toArray();

    	return View::make('pages.client.VeiwInfo')->with(['result' => $result]);
    }

    public function userList($clientID){
    	$request=$_REQUEST;
    		    
	    $col =array(
	        0   =>  'username',
	        1   =>  'name',
	        2   =>  'email',
		);

		$result = DB::table('users')
		->select('pkuserid', 'name','surname','username','email')
		->where('users.fkenterpriseid','=',$clientID)
		->distinct('pkuserid');
		
    	


    	//search
    	
		if(!empty($request['search']['value'])){
            $result = $result->whereRaw("(
            				username::varchar ilike '".$request['search']['value']."%' 
            				OR name::varchar ilike '".$request['search']['value']."%' 
            				OR email::varchar ilike '".$request['search']['value']."%')");	     
		}
		$totalData = $result->count();
    	$totalFilter =$totalData;


		//order
		$result = $result
		         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
		         	->offset($request['start'])
		         	->limit($request['length']);
		$result = $result->get();



		$data = array();

		foreach($result as $r){		
			$temp = array();
		    $temp[] = $r->username;
	        $temp[] = $r->name ." ". $r->surname;
	        $temp[] = $r->email;
	        $temp[] = '<a href="'.url('/user/view//').'/'.$r->pkuserid.'" class="btn btn-primary btn-xs">Ver <i class="fa fa-eye">&nbsp;</i></a>';
	        $data[] = $temp;
		}
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }

    public function propertyList($clientID){
    	$request=$_REQUEST;
    		    
	    $col =array(
	        0   =>  'description',
	        1   =>  'type',
	        2	=> 	'locality',
		); 

		$result = DB::table('property')
			->leftJoin('address','property.fkaddressid','=','address.pkaddressid')
		->select('pkpropertyid','type','description','locality')
		->where('fkenterpriseid','=',$clientID)
		->distinct('pkpropertyid');

    	//search
    	
		if(!empty($request['search']['value'])){
            $result = $result->whereRaw("(
            	description::varchar ilike '".$request['search']['value']."%' OR 
            	type::varchar ilike '".$request['search']['value']."%' OR
            	locality::varchar ilike '".$request['search']['value']."%')");
		}

		$totalData = $result->count();
    	$totalFilter =$totalData;

		//order
		$result = $result
		         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
		         	->offset($request['start'])
		         	->limit($request['length']);
		$result = $result->get();



		$data = array();

		foreach($result as $r){		
			$temp = array();
	        $temp[] = $r->description;
	        $temp[] = $r->type;
	        $temp[] = $r->locality;
	        $temp[] = '<a href="'.url('/property/view/').'/'.$r->pkpropertyid.'" class="btn btn-primary btn-xs">Ver <i class="fa fa-eye">&nbsp;</i></a>';
	        $data[] = $temp;
		}

    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }



    /* UPDATE CLIENT */
    public function clientVeiwInfoUpdate($clientID,Request $request){
    	$rules = [
			'enterprisenameInput' 	=> 	'required',
			'emailInput' 			=> 	'required|email||unique:enterprise,email,'.$clientID.',pkuserid',
			'taxnumberInput'		=>	'required|max:9|min:9',
			'cellphoneInput'		=>	'required|digits_between:9,12',
			'photoInput'			=>	'image|mimes:jpeg,bmp,png|max:5120|dimensions:max_width=500,max_height=500',
	    ];

	    $customMessages = [
	    	'enterprisenameInput.required' 	=> 'O Nome do Cliente é obrigatorio!',

	    	'emailInput.required' 		=> 'O Email é obrigatorio!',
	    	'emailInput.email'			=> 'O Email não esta no formato correto!',
	    	'emailInput.unique'			=> 'O Email já existe!',

	    	'taxnumberInput.required'	=> 'O Nºfiscal é obrigatorio!',
	    	'taxnumberInput.max'		=> 'O Nºfiscal não pode ser maior que :max!',
	    	'taxnumberInput.min'		=> 'O Nºfiscal não pode ser menor que :min!',

	    	'cellphoneInput.required'	=> 'O Nºtelefone é obrigatorio!',
	    	'cellphoneInput.digits_between'		=> 'O Nºtelefone tem de ser entre :min and :max digits',

	    	'photoInput.image'			=> 'Por Favor selecione uma Foto válida',
	    	'photoInput.mimes'			=> ' A Foto não tem uma extenção válida',
	    	'photoInput.size'			=> 'A Foto é grande de mais',
	    	'photoInput.dimensions'		=> 'A Foto tem de ser no máximo :max_width x :max_height',
	   ];
	    
	   	$validator = Validator::make( $request->all(), $rules, $customMessages );
	    if ($validator->fails()) {   
            return response()->json(['status'=>'validation','error'=>$validator->errors()]);
        }
	    
	    try{
			DB::beginTransaction();

            DB::table('enterprise')
            	->where('pkenterpriseid', $clientID)
            	->update([
            		'enterprisename' => $_POST['enterprisenameInput'],
					'nameresponsible' => $_POST['nameresponsibleInput'],
					'surnameresponsible' => $_POST['surnameresponsibleInput'],
					'cellphone' => $_POST['cellphoneInput'],
					'email' => $_POST['emailInput'],
					'taxnumber' => $_POST['taxnumberInput'],
					'fkenterprisetypeid' => $_POST['clientType'],
            	]);

            if ($request->hasFile('photoInput')) {
			    $img = Image::make($request->file('photoInput'))->encode('jpg', 80);
			    $imageName = $clientID. '.jpg';
			   	$img->save(
			        base_path() . '/public/img/enterprise/'. $imageName
			    );
			}            
            
			DB::commit();

			return response()->json([
			    'status' => 'sucesso',
			    'clientID' => $clientID
			]);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return response()->json(['status'=>'error','error'=>'Occoreu um erro! Tente novamente']);
		}	
    }

    public function clientVeiwInfoUpdateAddress($clientID,Request $request){
    	$FKAddressID = $this->getFKAddressID($clientID);

    	$rules = [
			'countryInput'		=>	'required',
			'districtInput'		=>	'required',
			'localityInput'		=>	'required',

			'zipcodeInput'		=>	'unique:address,zipcode,'.$FKAddressID.',pkaddressid',
	    ];

	    $customMessages = [
	    	'countryInput.required' 	=>	'O país é obrigatorio!',
	    	'districtInput.required' 	=> 	'O distrito é obrigatorio!',
	    	'localityInput.required'	=>	'A localidade é obrigatoria!',

	    	'zipcodeInput.unique'		=>	'O código postal já esta registado!'
	  	];
	    
	   	$validator = Validator::make( $request->all(), $rules, $customMessages );

	    if ($validator->fails()) {
            return response()->json(['status'=>'validation','error'=>$validator->errors()]);
        }    
	    
	    try{
			DB::beginTransaction();

			DB::table('address')
            	->where('pkaddressid', $FKAddressID)
            	->update([
            		'country' => $_POST['countryInput'],
					'district' => $_POST['districtInput'],
					'locality'=> $_POST['localityInput'],
					'street'=> $_POST['streetInput'],
					'zipcode'=> $_POST['zipcodeInput'],
            	]);       

			DB::commit();

			return response()->json([
			    'status' => 'sucesso',
			    'clientID' => $clientID
			]);
		} 
		catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return response()->json(['status'=>'error','error'=>'Occoreu um erro! Tente novamente']);
		}	
    }



    /* PRIVATE FUNCTIONS */
    private function getFKAddressID($clientID){
    	$temp = DB::table('enterprise')
			->select('fkaddressid')
			->where('pkenterpriseid', '=', $clientID)
			->get();
    	return $temp[0]->fkaddressid;
    }
}
