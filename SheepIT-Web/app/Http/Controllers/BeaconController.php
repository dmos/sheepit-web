<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use DB;
use Validator;

class BeaconController extends Controller
{
    public function index($beaconSearch = null){
    	
    	return View::make('pages.beacon.beaconList')->with(['beaconSearch' => $beaconSearch]);
    }

    public function beaconList(){
    	$request=$_REQUEST;
    		    
	    $col =array(
	        0   =>  'serialnumber',
	        1   =>  'version',
	        2   =>  'battery',
	        3   =>  'uptime',
		);  //create column like table in database //create filter animal get animal

		$result = DB::table('device')
			->join('beacon', 'device.pkdeviceid', '=', 'beacon.fkdeviceid')
			->leftJoin('devicerecordcache','device.pkdeviceid','=','devicerecordcache.fkdeviceid')
		->select('serialnumber', 'version','pkdeviceid','uptime', 'battery')
		->distinct('serialnumber');
		
    	


    	//search
    	
		if(!empty($request['search']['value'])){
            $result = $result->whereRaw("(serialnumber::varchar ilike '".$request['search']['value']."%' OR version::varchar ilike '".$request['search']['value']."%')");

            // var_dump($result->toSql());
            // exit;

		}

		$totalData = $result->count("serialnumber");
    	$totalFilter =$totalData;
		//order
		$result = $result
		         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
		         	->offset($request['start'])
		         	->limit($request['length']);
		$result = $result->get();



		$data = array();

		foreach($result as $r){		
			$temp = array();
		    $temp[] = $r->serialnumber;
	        $temp[] = $r->version;
	        $temp[] = $r->battery;
	        $temp[] = $r->uptime;

	        $temp[] = '<a href="'.url('/beacon/view//').'/'.$r->serialnumber.'" class="btn btn-primary btn-xs">Ver <i class="fa fa-eye">&nbsp;</i></a>';
	        $data[] = $temp;
		}
		// echo "<br><br>";
		// var_dump($sql);
		// exit;
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }



    /* Create Beacon */
    public function newBeacon(){
		return View::make('pages.beacon.newBeacon');
	}

	public function registerBeacon(Request $request){
		//dd($request);exit;


		$rules = [
			'serialNumberInput' => 'required|unique:device,serialnumber',
			'VersionInput' => 'required',
	    ];

	    $customMessages = [
	    	'serialNumberInput.required' => 'Numero de Série é obrigatorio!',
	    	'serialNumberInput.unique' => 'Numero de Série já existe!',
	    	'VersionInput.required'=> 'A versão é obrigatoria'
	   ];

	    $this->validate($request, $rules, $customMessages);
	    
	    $PropertySelect = (isset($_POST['PropertySelect']) && $_POST['PropertySelect'] != 0  ) ? $_POST['PropertySelect'] : null;
	    
	    
	    try{
			DB::beginTransaction();
			$id = DB::table('device')->insertGetId(
			    ['version' => $_POST['VersionInput'] , 'serialnumber' => $_POST['serialNumberInput'],'fkpropertyid'=> $PropertySelect] , 
			    'pkdeviceid'
			);

			DB::table('beacon')->insert([
				['fkdeviceid' => $id]
			]);

			DB::commit();
			return redirect('/beacon/view/'.$_POST['serialNumberInput']);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			return redirect('/beacon/new')->withErrors(['Error'=>"Occoreu um erro! Tente novamente"])->withInput();
		}	
	}

    public function clienteList(){
		$result = DB::table("enterprise")
			->select('pkenterpriseid','enterprisename')
			->get();
		echo json_encode($result);
	}

	public function propertyList($clientID){
		$result = DB::table("property")
			->select('pkpropertyid', 'description')
			->where("property.fkenterpriseid", "=",$clientID)
			->get();
		echo json_encode($result);
	}


	/* view Beacon */
    /**
     * [Creates the view of the collar]
     * @param [String] $collarSerial [contains the serial number of the collar]
     * @return [view] [returns the view of the collar info page]
     */
    public function beaconVeiwInfo($serialnumber){
    	//dd($collarSerial);exit;
    	$result = DB::table('device')
			->join('beacon', 'device.pkdeviceid', '=', 'beacon.fkdeviceid')
			->leftJoin('devicerecordcache', 'device.pkdeviceid', '=', 'devicerecordcache.fkdeviceid')
			->leftJoin('property', 'property.pkpropertyid', '=', 'device.fkpropertyid')
			->leftJoin('enterprise', 'enterprise.pkenterpriseid', '=', 'property.fkenterpriseid')
			->select('device.serialnumber','device.version','devicerecordcache.battery','devicerecordcache.uptime','devicerecordcache.timestamp','enterprise.pkenterpriseid','enterprise.enterprisename as clientname','property.description as propertydescription', 'property.pkpropertyid')
			->distinct('device.serialnumber')
			->where('device.serialnumber', '=', $serialnumber)
			->limit(1)
			->get()
			->toArray();
		 	// ->toSql();
		 //dd($result); exit;
		 
		$deviceID = $this->getPkDeviceID($serialnumber);
		$lastDay = DB::table('devicerecordcache')
		 	->select("timestamp")
		 	->where('fkdeviceid', '=', $deviceID)
		 	->get();

		$lastDay = $lastDay->isEmpty() ? date("Y-m-d"): $lastDay[0]->timestamp;

    	return View::make('pages.beacon.VeiwInfo')->with(['result' => $result,'lastDay'=>$lastDay]);
    }


    public function beaconVeiwInfoLineChart($serialnumber,$dayStart,$dayEnd){
    	$deviceID = $this->getPkDeviceID($serialnumber);
    	/* 1 day
    	$result = DB::table('devicerecordhistory')
    		->select("timestamp","battery")
			->where('fkdeviceid', '=', $deviceID)
			->whereRaw("devicerecordhistory.timestamp::text LIKE '".$day."%' ")
			->whereNotNull('timestamp')
			->orderBy('timestamp', 'desc')
			->limit(100)
			->get()
			->toArray();
		
		$result = array_reverse($result); //para aparecer os dados do grafico de forma correta
		*/

		$dayEnd = date('Y-m-d',strtotime($dayEnd."+1 days")) ;//offset
		$result = DB::table('devicerecordhistory')//timestamp_hour
			->select(DB::raw("to_timestamp(floor((extract('epoch' from timestamp) / 1800 )) * 1800) AT TIME ZONE 'UTC' as timestamp_hour, AVG(devicerecordhistory.battery) as battery"))
			->where('devicerecordhistory.fkdeviceid', '=', $deviceID)
			->whereRaw("devicerecordhistory.timestamp >= '".$dayStart."'  And devicerecordhistory.timestamp < '".$dayEnd."'")
			->whereNotNull('devicerecordhistory.timestamp')
			->groupBy('timestamp_hour')
			->orderBy('timestamp_hour', 'asc')
			->get()
			->toArray();

		$data = array();
		
		foreach($result as $r){
			//echo substr($r->timestamp, 0, 10) . '<br>';		
			$temp = array();
		    $temp[] = $r->timestamp;
	        $temp[] = $r->battery;
	        $data[] = $temp;
		}
		//var_dump(json_encode($data));
		
		$graph = array(
			"label"=> "Percentagem da bateria",
		    "color"=> "#00BCD4",
		    "data"=> $data
		);
		//var_dump($graph);
		
		echo json_encode($graph);
    }

    /* UPDATE INFO OF BEACON FUNTCION */
    public function beaconVeiwInfoUpdate($serialnumber,Request $request){
    	$pkdeviceid = $this->getPkDeviceID($serialnumber);
    	$pkbeaconid = $this->getPkBeaconID($serialnumber);
    	// dd($_POST);exit;

    	// TODO : versão como double - fazer validação
    	$rules = [
			'serialnumber' => 'required|unique:device,serialnumber,'.$pkdeviceid.',pkdeviceid',
			'version' => 'required',
	    ];

	    

	    $customMessages = [
	    	'serialnumber.required' => 'Numero de Série é obrigatorio!',
	    	'serialnumber.unique' => 'Numero de Série já existe!',

	    	'version.required' => 'A versão é obrigatoria',
	   ];
	    
	   	$validator = Validator::make( $request->all(), $rules, $customMessages );
	    if ($validator->fails()) {   
            return response()->json(['status'=>'validation','error'=>$validator->errors()]);
        }

	    $_POST['propertydescription'] = (isset($_POST['propertydescription']) && $_POST['propertydescription'] != 0  ) ? $_POST['propertydescription'] : null;   
	    
	    try{
			DB::beginTransaction();

            DB::table('device')
            	->where('pkdeviceid', $pkdeviceid)
            	->update(['serialnumber' => $_POST['serialnumber'] , 'fkpropertyid' => $_POST['propertydescription'], 'version' => $_POST['version']]);            
            
			DB::commit();

			return response()->json([
			    'status' => 'sucesso',
			    'serialnumber' => $_POST['serialnumber']
			]);
			//return redirect('/collar/view/'.$_POST['serialnumber']);

		} catch(\Exception $e){
			DB::rollback();
			//echo  "ERROR: <br>".$e->getMessage();
			//return redirect('/collar/view/'.$collarSerial)->withErrors(['Error'=>"Occoreu um erro! Tente novamente"])->withInput();
			return response()->json(['status'=>'error','error'=>'Occoreu um erro! Tente novamente']);
		}	
    }


    /* DEBUG PAGE COLLAR */
    /**
     * [Creates the view of the collar in debug mode]
     * @param  [String] $collarSerial [Serial number of the collar]
     * @return [View] [returns the view of the collar page debug mode]
     */
    public function beaconVeiwInfoDebug($serialnumber){
    	
    	return View::make('pages.beacon.VeiwInfoDebug')->with(['serialnumber'=>$serialnumber]);
    }

    /**
     * [Functions loaded by ajax to get information for the debug datatable]
     * @param  [String] $collarSerial [Serial number of the collar]
     * @return [JSON] [returns a json with the information for the datatable]
     */
    public function beaconVeiwInfoDebugTable($serialnumber){
    	$baeconID = $this->getPkBeaconID($serialnumber);
    	$col =array(
	        0   =>  'id',
		);

    	$result = DB::table('rawdatabeacon')
			->where('rawdatabeacon.b_beaconid', '=',$baeconID)
			->orderBy('id','desc');
			//->limit(100);
			//->get();

		// datatable
		$request=$_REQUEST;

		$totalData = $result->count("id");
		$totalFilter= $totalData;

		$result = $result
	         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
	         	->offset($request['start'])
	         	->limit($request['length']);

		$result = $result->get();

		$data = array();

		foreach($result as $r){
			$temp = array();
		    $temp[] = $r->id;
	        $temp[] = $r->b_beaconid;
	        $temp[] = $r->b_b2bseqnumb;
	        $temp[] = $r->b_bsseqnumb;
	        $temp[] = $r->b_gateway;
	        $temp[] = $r->b_latitude;
	        $temp[] = $r->b_longitude;
	        $temp[] = $r->b_neighborid_1.", ".$r->b_neighborid_2.", ".$r->b_neighborid_3.", ".$r->b_neighborid_4;
	        $temp[] = $r->b_neighborrssi_1.", ".$r->b_neighborrssi_2.", ".$r->b_neighborrssi_3.", ".$r->b_neighborrssi_4;
	        $temp[] = $r->b_propertyid;
	        $temp[] = $r->b_timestamp;
	        

	        $data[] = $temp;
		}
		// echo "<br><br>";
		// var_dump($sql);
		// exit;
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }



    /* PRIVATE FUNCTIONS */

	private function getPkDeviceID($deviceSerialNumber){
    	$temp = DB::table('device')
			->select('pkdeviceid')
			->where('serialnumber', '=', $deviceSerialNumber)
			->get();
    	return $temp[0]->pkdeviceid;
    }

    private function getPkBeaconID($deviceSerialNumber){
    	$temp = DB::table('beacon')
			->select('pkbeaconid')
			->join('device','beacon.fkdeviceid','=','device.pkdeviceid')
			->where('device.serialnumber', '=', $deviceSerialNumber)
			->get();
    	return $temp[0]->pkbeaconid;
    }
}
