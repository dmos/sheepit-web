<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use DB;

class AlertsController extends Controller
{
	/**
	 * Creates the view of the Alert page.
	 * @param  String $alertType Containes the search value to filter the alerts. if none is given, then the default value is null
	 * @return View returns the view of the alert page
	 */
    public function index($alertType = null){
    	
    	return View::make('pages.alertas')->with(['alertType' => $alertType]);
    }

    /**
     * function that gets the list of alerts for the datatable. this function also does the search option of the datatable
     * @return JSON  returns a json specifically formated for the datatable that contains the data of the table
     */
    public function alertsData(){
    	/**
    	 * TODO :
    	 * Rescrever codigo com querybuilder
    	 * feito mas tem de se rever o que há aqui para fazer! como comentar
    	 * 
    	 */

    	$request=$_REQUEST;
    		    
	    $col =array(
	        0   =>  'id',
	        1   =>  'alertvalue',
	        2   =>  'checked',
	        3   =>  'type'
		);  //create column like table in database

		$result = DB::table('alert')->select('id', 'alertvalue','checked','type');

    	//search
    	
		if(!empty($request['search']['value'])){
		        $result = $result
				->whereRaw("id::varchar Like '".$request['search']['value']."%'")
                ->orWhereRaw("alertvalue::varchar Like '".$request['search']['value']."%'")
                ->orWhereRaw("checked::varchar ilike '".$request['search']['value']."%'")
                ->orWhereRaw("type ilike '".$request['search']['value']."%'");

		}
		$totalData = $result->count("id");
    	$totalFilter =$totalData;

		//order
		$result = $result
		         	->orderBy($col[$request['order'][0]['column']], $request['order'][0]['dir'])
		         	->offset($request['start'])
		         	->limit($request['length']);
		$result = $result->get();


		$data = array();

		foreach($result as $r){
			$temp = array();
		    $temp[] = $r->id;
	        $temp[] = $r->alertvalue;
	        $temp[] = $r->checked;
	        $temp[] = $r->type;
	        $temp[] = '<button type="button" id="getEdit" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal" data-id="'.$r->id.'">Ver <i class="fa fa-eye">&nbsp;</i></button>';
	        $data[] = $temp;
		}
		// echo "<br><br>";
		// var_dump($data);
		// exit;
		
    	$json_data=array(
		    "draw"              =>  intval($request['draw']),
		    "recordsTotal"      =>  intval($totalData),
		    "recordsFiltered"   =>  intval($totalFilter),
		    "data"              =>  $data
		    );

		echo json_encode($json_data);
    }


    /**
     * function that gets the information of a specific alert
     * @param  Integer $id id of the alert
     * @return JSON     returns a json with the information of the alert
     */
    public function getAlertInfo($id){
    	$info['info'] = DB::table('alert')->where('id',$id)->get();
    	return $info;
    }
}
