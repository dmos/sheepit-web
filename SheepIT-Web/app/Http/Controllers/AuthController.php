<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Authenticable;

use View;
use Hash;

class AuthController extends Controller
{
  public function index(){
    return View::make('pages.login');
  }

  public function doLogin(Request $request){

    $rules = [
      'pass' => 'required',
      'username'=> 'required|exists:users,username',
    ];

    $customMessages = [
       'pass.required' => 'A Password é obrgatorio!',
       'username.required' => 'O Nome de Utilizador é obrgatorio!',
       'username.exists' => 'O Nome de Utilizador não esta atribuido',

   ];

    $this->validate($request, $rules, $customMessages);

    if(Auth::attempt(['username' => $request->input('username'), 'password' =>$request->input('pass')])){
      //echo "SUCESSO";
      return redirect('/');
    }
    else{
      //echo "failed";
      //return redirect('/')->with('Error',"Credencias Erradas")->withInput();//não mostra erro
      return redirect('/')->withErrors(['Error'=>"Credencias Erradas"])->withInput();
    }

  }

  public function Logout(){
    Auth::logout();
    return redirect('/');
  }
}
