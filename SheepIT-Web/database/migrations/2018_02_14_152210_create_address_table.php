<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('address', function(Blueprint $table)
		{
			$table->integer('pkaddressid', true);
			$table->string('district', 45)->nullable();
			$table->string('county', 45)->nullable();
			$table->string('locality', 45)->nullable();
			$table->string('street', 45)->nullable();
			$table->string('zipcode', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('address');
	}

}
