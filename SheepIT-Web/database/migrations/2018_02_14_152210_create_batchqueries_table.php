<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBatchqueriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('batchqueries', function(Blueprint $table)
		{
			$table->integer('pkbatchqueriesid', true);
			$table->text('type')->nullable();
			$table->text('inicialtablename')->nullable();
			$table->text('finaltablename')->nullable();
			$table->text('query')->nullable();
			$table->text('description')->nullable();
			$table->text('schedule')->nullable();
			$table->boolean('executable')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('batchqueries');
	}

}
