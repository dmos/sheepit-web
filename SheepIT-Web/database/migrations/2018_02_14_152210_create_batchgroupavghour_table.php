<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBatchgroupavghourTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('batchgroupavghour', function(Blueprint $table)
		{
			$table->dateTime('ts')->nullable();
			$table->decimal('groupavgdistance', 38, 18)->nullable();
			$table->decimal('groupavgsteps', 38, 18)->nullable();
			$table->decimal('groupavgfence', 38, 18)->nullable();
			$table->decimal('groupavgposture', 38, 18)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('batchgroupavghour');
	}

}
