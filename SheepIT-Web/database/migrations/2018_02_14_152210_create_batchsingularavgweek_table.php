<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBatchsingularavgweekTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('batchsingularavgweek', function(Blueprint $table)
		{
			$table->dateTime('ts')->nullable();
			$table->integer('fkanimalid');
			$table->decimal('singleavgdistance', 38, 18)->nullable();
			$table->decimal('singleavgsteps', 38, 18)->nullable();
			$table->decimal('singleavgfence', 38, 18)->nullable();
			$table->decimal('singleavgposture', 38, 18)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('batchsingularavgweek');
	}

}
