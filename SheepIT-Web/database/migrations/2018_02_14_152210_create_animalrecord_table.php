<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnimalrecordTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('animalrecord', function(Blueprint $table)
		{
			$table->integer('pkanimalrecordid', true);
			$table->string('nearestbeacon', 45)->nullable();
			$table->string('rssi', 45)->nullable();
			$table->dateTime('timestamp')->nullable();
			$table->integer('numbersteps')->nullable();
			$table->integer('postureinfraction')->nullable();
			$table->integer('fenceinfraction')->nullable();
			$table->integer('fkanimalid')->index('fk_animalrecord_animal1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('animalrecord');
	}

}
