<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDevicerecordTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('devicerecord', function(Blueprint $table)
		{
			$table->integer('pkdevicerecordid', true);
			$table->dateTime('timestamp')->nullable();
			$table->integer('battery')->nullable();
			$table->dateTime('uptime')->nullable();
			$table->integer('fkdeviceid')->index('fk_devicerecord_device1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('devicerecord');
	}

}
