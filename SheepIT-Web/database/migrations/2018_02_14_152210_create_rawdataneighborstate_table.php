<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRawdataneighborstateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rawdataneighborstate', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->integer('b_battery');
			$table->integer('b_beaconid');
			$table->string('b_gpslat')->nullable();
			$table->string('b_gpslon')->nullable();
			$table->dateTime('b_timestamp')->nullable();
			$table->integer('g_battery');
			$table->integer('g_gatewayid');
			$table->string('g_gpslat')->nullable();
			$table->string('g_gpslon')->nullable();
			$table->string('g_propertyid')->nullable();
			$table->dateTime('g_timestamp')->nullable();
			$table->integer('n_battery');
			$table->integer('n_beaconid');
			$table->string('n_gpslat')->nullable();
			$table->string('n_gpslon')->nullable();
			$table->integer('n_propertyid');
			$table->dateTime('n_timestamp')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rawdataneighborstate');
	}

}
