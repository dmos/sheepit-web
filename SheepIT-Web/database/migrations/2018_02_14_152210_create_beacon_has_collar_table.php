<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBeaconHasCollarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('beacon_has_collar', function(Blueprint $table)
		{
			$table->integer('fkbeaconid')->index('fk_beacon_has_collar_beacon1_idx');
			$table->integer('fkcollarid')->index('fk_beacon_has_collar_collar1_idx');
			$table->primary(['fkbeaconid','fkcollarid'], 'beacon_has_collar_pkey');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('beacon_has_collar');
	}

}
