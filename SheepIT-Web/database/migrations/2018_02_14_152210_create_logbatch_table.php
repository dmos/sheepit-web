<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogbatchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logbatch', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->dateTime('finishtime')->nullable();
			$table->integer('queryid');
			$table->string('schedule')->nullable();
			$table->dateTime('starttime')->nullable();
			$table->string('status')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('logbatch');
	}

}
