<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoginTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('login', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('firstName')->nullable();
			$table->text('lastName')->nullable();
			$table->string('email', 45)->nullable();
			$table->string('username', 45)->nullable();
			$table->string('password', 45)->nullable();
			$table->string('type', 45)->nullable();
			$table->text('authKey')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('login');
	}

}
