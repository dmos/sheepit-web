<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAlertTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alert', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->float('alertvalue', 10, 0);
			$table->boolean('checked');
			$table->dateTime('logtime')->nullable();
			$table->string('object')->nullable();
			$table->float('percentage', 10, 0);
			$table->float('percentual', 10, 0);
			$table->float('steps', 10, 0);
			$table->string('topic')->nullable();
			$table->string('type')->nullable();
			$table->integer('fkobjectid');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alert');
	}

}
