<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRawdatacollarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rawdatacollar', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->integer('b_battery');
			$table->integer('b_beaconid');
			$table->string('b_gpslat')->nullable();
			$table->string('b_gpslon')->nullable();
			$table->dateTime('b_timestamp')->nullable();
			$table->string('c_accelerometerx')->nullable();
			$table->string('c_accelerometery')->nullable();
			$table->string('c_accelerometerz')->nullable();
			$table->integer('c_battery');
			$table->integer('c_collarid');
			$table->string('c_d_accelerometerx')->nullable();
			$table->string('c_d_accelerometery')->nullable();
			$table->string('c_d_accelerometerz')->nullable();
			$table->integer('c_fenceinfraction');
			$table->string('c_magnetometerx')->nullable();
			$table->string('c_magnetometery')->nullable();
			$table->string('c_magnetometerz')->nullable();
			$table->integer('c_postureinfraction');
			$table->integer('c_propertyid');
			$table->integer('c_rcv_rssi_1');
			$table->integer('c_rcv_rssi_2');
			$table->integer('c_rcv_rssi_3');
			$table->integer('c_rcv_rssi_4');
			$table->integer('c_rcv_rssi_id1');
			$table->integer('c_rcv_rssi_id2');
			$table->integer('c_rcv_rssi_id3');
			$table->integer('c_rcv_rssi_id4');
			$table->integer('c_reportingbeacon');
			$table->integer('c_steps');
			$table->integer('c_ultrasoundsdistance');
			$table->integer('g_battery');
			$table->integer('g_gatewayid');
			$table->string('g_gpslat')->nullable();
			$table->string('g_gpslon')->nullable();
			$table->string('g_propertyid')->nullable();
			$table->dateTime('g_timestamp')->nullable();
			$table->integer('fkanimal');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rawdatacollar');
	}

}
