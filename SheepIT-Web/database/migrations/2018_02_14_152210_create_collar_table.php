<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCollarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('collar', function(Blueprint $table)
		{
			$table->integer('pkcollarid', true);
			$table->integer('fkdeviceid')->index('fk_collar_device1_idx');
			$table->integer('collarid')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('collar');
	}

}
