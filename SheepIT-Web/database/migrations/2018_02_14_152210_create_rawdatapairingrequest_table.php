<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRawdatapairingrequestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rawdatapairingrequest', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('animalid')->nullable();
			$table->string('collarserial')->nullable();
			$table->string('prid')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rawdatapairingrequest');
	}

}
