<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client', function(Blueprint $table)
		{
			$table->integer('pkclientid', true);
			$table->string('name', 45)->nullable();
			$table->string('surname', 45)->nullable();
			$table->string('cellphone', 45)->nullable();
			$table->string('email', 45)->nullable();
			$table->string('citizencard', 45)->nullable();
			$table->string('taxnumber', 45)->nullable();
			$table->integer('fkaddressid')->index('fk_owner_address1_idx');
			$table->integer('fkclienttypeid')->index('fk_client_clienttype1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('client');
	}

}
