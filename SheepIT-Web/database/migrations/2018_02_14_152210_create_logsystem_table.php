<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogsystemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logsystem', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('freehdd');
			$table->bigInteger('freeram');
			$table->dateTime('timestamp')->nullable();
			$table->string('type')->nullable();
			$table->bigInteger('usedhdd');
			$table->float('usedhddperc', 10, 0);
			$table->bigInteger('usedram');
			$table->float('usedramperc', 10, 0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('logsystem');
	}

}
