<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBatchdevicebatteryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('batchdevicebattery', function(Blueprint $table)
		{
			$table->dateTime('ts')->nullable();
			$table->integer('fkdeviceid');
			$table->decimal('singleavgbattery', 38, 18)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('batchdevicebattery');
	}

}
