<?php

	
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use View;

class AnimalController extends Controller
{
    public function index(){
    	$dados = DB::table('animal')
    		->limit(10)
    		->join('animalcollar', 'animalcollar.fkanimalid', '=', 'animal.pkanimalid')
    		->join('collar', 'animalcollar.fkcollarid', '=', 'collar.pkcollarid')
    		->join('device', 'collar.fkdeviceid', '=', 'device.pkdeviceid')
    		->select('animal.*', 'animalcollar.*','collar.*','device.*')
    		->get()
    		->toArray();

    	// $dados = DB::table("animal")->select("animal.*", "animalcollar.*")
			  //   ->leftjoin("animalcollar", function ($join) {
			  //       $join->on("animal.pkanimalid", "=", "animalcollar.fkanimalid");
			  //   })
			  //   ->get();
    	return View::make('pages.animal',['dados' => $dados]);
    }


    public function dataTable(){
    	$dados['dados'] = DB::table('animal')
    		->limit(10)
    		->join('animalcollar', 'animalcollar.fkanimalid', '=', 'animal.pkanimalid')
    		->join('collar', 'animalcollar.fkcollarid', '=', 'collar.pkcollarid')
    		->join('device', 'collar.fkdeviceid', '=', 'device.pkdeviceid')
    		->select('animal.*', 'animalcollar.*','collar.*','device.*')
    		->get()
    		->toJson();
    	return $dados;
    }


    public function index(){
    	$address = DB::table('address')->where("street","like","%da%")->get()->toArray();
    	return View::make('pages.address',['address' => $address]);
    }
}

$result = DB::table('device')
            ->join('collar', 'device.pkdeviceid', '=', 'collar.fkdeviceid')
            ->join('devicerecord', 'device.pkdeviceid', '=', 'devicerecord.fkdeviceid')
        ->select('serial', 'version','battery','uptime')
        ->toSql();

        dd($result);exit;


// blade debugger:
 //{{ dd(get_defined_vars()) }}
 



 /* X-editable
    $.fn.editable.defaults.mode = 'inline'; 
        
        $('#infos .editable').editable('disabled');
        $('#enable').click(function() {
            $('#infos .editable').editable('toggleDisabled');
            $(this).html() == "Editar"?$(this).html("Concluir"):$(this).html("Editar");
        });    

        function editableOption(inputType,BDPrimaryKey,Title){
            //url = '/post' // era
            return {
                type: inputType,
                pk: BDPrimaryKey,
                url: '',
                title: Title,
                disabled : true
            };
        }
        
        $('#serial').editable(editableOption('text',16,'Numero de Série'));
        $('#version').editable(editableOption('text',16,'Versão'));
        $('#collaridnetwork').editable(editableOption('number',16,'ID de Rede'));
        
        $('#animalpublicid').editable({
            type: 'select',
            pk: 16,
            url: '',
            title: 'Animal associado',
            disabled : true,
            source : [{'value': 1, 'text': '1'}, {'value': 2, 'text': '2'},{'value': 3, 'text': '3'}]
        });
        $('#clientname').editable({
            type: 'select',
            pk: 16,
            url: '',
            title: 'Cliente Associado',
            disabled : true,
            source : [{'value': 1, 'text': 'Manuel'}, {'value': 2, 'text': 'Rui'},{'value': 3, 'text': 'Daniel'}]
        });
        $('#propertydescription').editable({
            type: 'select',
            pk: 16,
            url: '',
            title: 'Propriedade',
            disabled : true,
            source : [{'value': 1, 'text': 'Ramos Pinto Portugal'}, {'value': 2, 'text': 'Prop2'},{'value': 3, 'text': 'Prop3'}]
        }); 
 */
?>


