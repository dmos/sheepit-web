function createDatatable(tableID,URL,search){
	$(tableID).DataTable( {
	        "processing": true,
	        "serverSide": true,
	        server : true,
	        "ajax": URL,
	        searchDelay: 350,
	        pageLength : 25,
	        "lengthMenu": [ 10, 25, 50, 75, 100 ],

	        "bDeferRender": true,
	        
	        "searching": true,
	        "search": {
	                    "search": search
	                  },
	        language: {
			    "sProcessing":   "A processar...",
			    "sLengthMenu":   "Mostrar _MENU_ registos",
			    "sZeroRecords":  "Não foram encontrados resultados",
			    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
			    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
			    "sInfoFiltered": "<br>(filtrado de _MAX_ registos no total)",
			    "sInfoPostFix":  "",
			    "sSearch":       "Procurar:",
			    "sUrl":          "",
			    "oPaginate": {
			        "sFirst":    "Primeiro",
			        "sPrevious": "Anterior",
			        "sNext":     "Seguinte",
			        "sLast":     "Último"
			    }
			} ,

	    } );

    //os do meio podem fazer a datatable carregar mais rapido
    //scrollY:        200,
    //deferRender:    true,
    //scroller:       true,
    
    /**
     * TODO :
     * filter by type ja esta.
     * bug na paginação
     */
}

function createDatatableScroll(tableID,URL,searchEnable = false,search =null,scrollYSize =200, scrollXEnable = false,columnOrderDisable = [], columndefaultorder = [0, "asc"]){
	$(tableID).DataTable( {
	        "processing": true,
	        "serverSide": true,
	        server : true,
	        "ajax": URL,
	        
	        scrollY: scrollYSize,

	        "scrollX": scrollXEnable,
    		
    		scroller: true,

	        "bDeferRender": true,
	       	"deferRender": true,

	       	"columnDefs": [ {
				"targets": columnOrderDisable,
				"orderable": false
				} ],
			"order": [columndefaultorder],
			"aaSorting" : [columndefaultorder],

	        "searching": searchEnable,
	        searchDelay: 350,
	        "search": {
	                    "search": search
	                  },
	        language: {
			    "sProcessing":   "A processar...",
			    "sLengthMenu":   "Mostrar _MENU_ registos",
			    "sZeroRecords":  "Não foram encontrados resultados",
			    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
			    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
			    "sInfoFiltered": "<br>(filtrado de _MAX_ registos no total)",
			    "sInfoPostFix":  "",
			    "sSearch":       "Procurar:",
			    "sUrl":          "",
			    "oPaginate": {
			        "sFirst":    "Primeiro",
			        "sPrevious": "Anterior",
			        "sNext":     "Seguinte",
			        "sLast":     "Último"
			    }
			} 

	    } );
}
