
  var cores = ['rgba(255, 99, 132, 0.8)',
  'rgba(54, 162, 235, 0.8)',
  'rgba(255, 206, 86, 0.8)',
  'rgba(75, 192, 192, 0.8)',
  'rgba(153, 102, 255, 0.8)',
  'rgba(255, 159, 64, 0.8)',
  'rgba(100, 100, 12, 0.8)',
  'rgba(255, 5, 64, 0.8)',
  'rgba(20, 159, 64, 0.8)',
  'rgba(5, 5, 5, 0.8)'];

/*
  charts zoom
  https://github.com/chartjs/chartjs-plugin-zoom
  https://stackoverflow.com/questions/44551904/add-zoom-event-handler-to-charts-for-chartjs-with-chartjs-plugin-zoom
*/

  //need to be global to bea able to be destroyed
  var pie;
  var mypie;
function piechart(route,elemente){
  $.getJSON( route, function( data ) {
    //console.log(data);
    //console.log("entrou");
    var fetched = data.dados;
    

    var labels=[];
    var dados=[];
    var cor= [];

    for(var i = 0; i<fetched.length; i++){
      labels.push(fetched[i]['label']);
      dados.push(fetched[i]['total']);
      cor.push(cores[i]);
    }

    //use to clear all variables and clear graphs.
    //if not done, can ocurre in visual bug on graph
    if(mypie != undefined || mypie != null){
      mypie.clear();
      mypie.destroy();
      pie = null;
      mypie = null;

    }

    pie = document.getElementById(elemente).getContext('2d');

    mypie = new Chart(pie, {
        type: 'pie',
        data: {
            labels: labels,
            datasets:[{
              label: labels[i],
                fill:false,
                backgroundColor: cor,
                borderColor: cor,
                data: dados
            }]
          },
          options: {
            responsive: true,
            legend: {
              display: true,
              position :'bottom',
              }
          }
      });



      mypie.update();


    });
}

  //need to be global to bea able to be destroyed
  var line;
  var myLineChart;
function lineChart(route,elemente){

  $.getJSON(route, function( data ) {
    //console.log(data);
    var tempData = data.data;
    console.log(tempData);
    var labels=[];
    var dados=[];


    for(var i = 0; i<tempData.length; i++){
      labels.push(tempData[i][0]);
      dados.push(tempData[i][1]);
    }

    //console.log(labels);

    //use to clear all variables and clear graphs.
    //if not done, can ocurre in visual bug on graph
    if(myLineChart != undefined || myLineChart != null){
      myLineChart.clear();
      myLineChart.destroy();
      line = null;
      myLineChart = null;

    }
    
    line = document.getElementById(elemente).getContext('2d');

    myLineChart = new Chart(line, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{ 
                  data: dados,
                  label: data.label,
                  borderColor: data.color,
                  backgroundColor: data.color,
                  fill: false
                }]
          },
          options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true,
                      stepSize: 10,
                      callback: function(tickValue, index, ticks) {
                        if(!(index % parseInt(ticks.length / 5))) {
                          return tickValue
                        }
                      }
                  }
              }],
          },
          // Container for pan options
          pan: {
              // Boolean to enable panning
              enabled: true,

              // Panning directions. Remove the appropriate direction to disable
              // Eg. 'y' would only allow panning in the y direction
              mode: 'xy'
          },

          // Container for zoom options
          zoom: {
              // Boolean to enable zooming
              enabled: true,

              // Zooming directions. Remove the appropriate direction to disable
              // Eg. 'y' would only allow zooming in the y direction
              mode: 'xy',
          }
      }
      });

      myLineChart.update();


  });
}


function lineChartTime(route,elemente){

  $.getJSON(route, function( data ) {
    //console.log(data);
    var tempData = data.data;
    //console.log(tempData);
    var labels=[];
    var dados=[];


    for(var i = 0; i<tempData.length; i++){
      var timestamp = new Date(tempData[i][0])
      var min = timestamp.getMinutes() == '0'? '00': timestamp.getMinutes() ;
      var time = timestamp.getHours()+":"+ min;
      var day = timestamp.getDate() +"/"+ (timestamp.getMonth() +1)
      labels.push([time,day]);
      //labels.push(tempData[i][0]);
      dados.push(tempData[i][1]);
    }

    //console.log(labels);

    //use to clear all variables and clear graphs.
    //if not done, can ocurre in visual bug on graph
    if(myLineChart != undefined || myLineChart != null){
      myLineChart.clear();
      myLineChart.destroy();
      line = null;
      myLineChart = null;

    }
    
    line = document.getElementById(elemente).getContext('2d');

    myLineChart = new Chart(line, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{ 
                  data: dados,
                  label: data.label,
                  borderColor: data.color,
                  backgroundColor: data.color,
                  fill: false
                }]
          },
          options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true,
                      stepSize: 10,
                      callback: function(tickValue, index, ticks) {
                        if(!(index % parseInt(ticks.length / 5))) {
                          return tickValue
                        }
                      }
                  }
              }],
              xAxes: [{
                  ticks: {                    
                    fontSize: 12,
                    autoSkip: false,
                    maxRotation: 0,
                    minRotation: 0,
                    callback: function(tickValue, index, ticks) {
                      if(!(index % parseInt(ticks.length / 15))) {
                        return tickValue
                      }
                    }
                  }
              }]
          },
          // Container for pan options
          pan: {
              // Boolean to enable panning
              enabled: true,

              // Panning directions. Remove the appropriate direction to disable
              // Eg. 'y' would only allow panning in the y direction
              mode: 'xy'
          },

          // Container for zoom options
          zoom: {
              // Boolean to enable zooming
              enabled: true,

              // Zooming directions. Remove the appropriate direction to disable
              // Eg. 'y' would only allow zooming in the y direction
              mode: 'xy',
          }
      }
      });

      myLineChart.update();


  });
}