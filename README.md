# SheepIT Web Platform

The SheepIT Web Platform is a **responsive web application** that aims to allow the monitoring Sheeps, by using data from various IoT sensores.

This platform must be able to create, edit and view information related to sheep's, collar's, beacon's, properties, among others, using tables and graphs. Its also possible to delimit the areas of a registered property and visualize in real time sheep’s movement inside that
area using a map

This project was developed in an **internship** at **Instituto de Telecomunicações** to finish the curse of **Information System Programming**  from the **University of Aveiro**

![Dashboard](./images/sheepit_dashboard.png)



## Core Tecnologies

* HTML
* CSS
* Bootstrap
* JavaScript
* JQuery
* PHP
* Laravel
* JSON
* AJAX
* PostgreSQL
* Leaflet
* Datatables
* ChartJS



## Main Functionality

* [x] CRUD Sheep's
* [x] CRUD flock's
* [x] CRUD Collar's
* [x] CRUD Beacon's
* [x] CRUD Properties
* [x] CRUD User's
* [x] CRUD Clients
* [x] Alert's
* [x] System configurations
* [x] Dashboard
* [x] Graph's
* [x] Maps (Leaflet)
* [x] etc..


## Getting Started
1. `git clone ...`
2. `cd` into project folder
3. Run `composer install`
4. Run `php artisan key:generate`
5. Check **.env** file
6. Copy project folder to WebServer
7. Enjoy! 

Note: this project used an already existing database, so there was no migrations files.

___

## Other fotos

![Sheep List](./images/sheepit_list.png)

![Sheep Info](./images/sheepit_info.png)

![Sheep Info](./images/sheepit_map.png)


